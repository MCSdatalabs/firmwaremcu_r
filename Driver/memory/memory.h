/**
 * @file			memory.h
 * @date			21.03.2017
 *	
 * @details
 */

#ifndef MEMORY_H_
#define MEMORY_H_

#include <stdint.h>
#include <stdbool.h>

#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"

#include "spidrv.h"

#define SPI_CLK_PIN                     (2)
#define SPI_CLK_PORT                    (gpioPortD)

#define SPI_CS_PIN                      (3)
#define SPI_CS_PORT                     (gpioPortD)

#define SPI_MISO_PIN                    (1)
#define SPI_MISO_PORT                   (gpioPortD)

#define SPI_MOSI_PIN                    (0)
#define SPI_MOSI_PORT                   (gpioPortD)


/**
 * @brief Return codes for the memory.
 */
typedef enum {
	MEMORY_SUCCESS = 0,			//!< Transfer completed successfully.
	MEMORY_FAIL = -1,          	//!< Memory error.
	MEMORY_SPI_ERR = -3,       	//!< SPI error.
	MEMORY_NO_INIT = -99       	//!< Memory not initialized.
}MEMORY_RETURN;

/**
 * @brief Initialize the memory.
 *
 * Initialize SPI bus including the corresponding GPIOs (MISO, MOSI, CLK and CS)
 * for the communication.
 *
 * @return MEMORY_RETURN
 * @retval MEMORY_SUCCESS			Transfer completed successfully
 * @retval MEMORY_NO_INIT			MEMORY not initialized
 */
MEMORY_RETURN memory_init(void);

MEMORY_RETURN memory_chipErase(void);
MEMORY_RETURN memory_readMemory(uint32_t address, uint8_t *result, uint32_t numOfBytes);
MEMORY_RETURN memory_writeMemory(uint32_t address, uint8_t *data_buffer, uint32_t numOfBytes);

#endif /* MEMORY_H_ */
