/**
 * @file			memory.c
 * @date			21.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "memory.h"

/******************** defines *****************************/
#define DEBUG_BREAK           __asm__("BKPT #0");

#define GET_DEVICE_ID_DATA	0xAF
#define MANUFACTURER_ID		0x20
#define MEMORY_TYPE			0xBA
#define MEMORY_CAPACITY		0x19

#define STATUS                0x05
#define WR_ENABLE             0x06
#define PAGE_PROGRAM          0x02
#define CHIP_ERASE            0xC7
#define READ_DATA             0x03

// Bit positions for status register
#define WIP_BIT               1 << 0
#define WEL_BIT               1 << 1

#define PAGE_SIZE             256
#define SPI_TRANSFER_SIZE     PAGE_SIZE + 4  // Account for SPI header

/******************** prototypes **************************/
void config_write(uint8_t command);
uint8_t read_status(void);

/******************** struct data type ********************/

/******************** private variables********************/
static bool initiliazed = false;
static bool transferComplete = false;
SPIDRV_HandleData_t handleData;
SPIDRV_Handle_t handle = &handleData;

/******************** callback function********************/
void TransferComplete(SPIDRV_Handle_t handle, Ecode_t transferStatus, int itemsTransferred)
{
  if ( transferStatus == ECODE_EMDRV_SPIDRV_OK )
  {
    // Success !
  }
  transferComplete = true;
}

/******************** private function ********************/
void config_write(uint8_t command)
{
      const int size = 1;
      uint8_t result[size];
      uint8_t tx_data[size];

      tx_data[0] = command;

      transferComplete = false;
      SPIDRV_MTransfer( handle, &tx_data, &result, size, TransferComplete);

      while(!transferComplete)
    	  ;
}

uint8_t read_status(void)
{
      const int size = 2;
      uint8_t result[size];
      uint8_t tx_data[size];

      tx_data[0] = STATUS;

      transferComplete = false;
	  SPIDRV_MTransfer( handle, &tx_data, &result, size, TransferComplete);
	  while(!transferComplete)
		  ;

      return result[1];
}

/******************** interrupt function ******************/

/******************** global function *********************/
MEMORY_RETURN memory_init(void)
{
	initiliazed = false;

	SPIDRV_Init_t initData = SPIDRV_MASTER_USART1;

	SPIDRV_Init( handle, &initData );

	char transmitData[5];
	char receiveBuffer[5];
	transmitData[0] = GET_DEVICE_ID_DATA;

	transferComplete = false;
	SPIDRV_MTransfer(handle, &transmitData, &receiveBuffer, 4, TransferComplete);
	while(!transferComplete)
		;
	if((receiveBuffer[1] != MANUFACTURER_ID) && (receiveBuffer[2] != MEMORY_TYPE) && (receiveBuffer[3] != MEMORY_CAPACITY))
		return MEMORY_NO_INIT;

	initiliazed = true;
	return MEMORY_SUCCESS;
}

MEMORY_RETURN memory_chipErase(void)
{
	if(!initiliazed)
		return MEMORY_NO_INIT;

	uint8_t status;
	config_write(WR_ENABLE);

	status = read_status();
	if (!(status & WEL_BIT))
		DEBUG_BREAK

	config_write(CHIP_ERASE);

	do status = read_status();
	while (status & WIP_BIT);

  	return MEMORY_SUCCESS;
}

MEMORY_RETURN memory_readMemory(uint32_t address, uint8_t *result, uint32_t numOfBytes)
{
	if(!initiliazed)
		return MEMORY_NO_INIT;

	uint8_t tx_data[SPI_TRANSFER_SIZE];
	uint8_t rx_data[SPI_TRANSFER_SIZE];

	tx_data[0] = READ_DATA;
	tx_data[1] = (address >> 16);
	tx_data[2] = (address >> 8);
	tx_data[3] = address;

	transferComplete = false;
	SPIDRV_MTransfer( handle, &tx_data, &rx_data, SPI_TRANSFER_SIZE, TransferComplete);
	while(!transferComplete)
		;

	// Fill the result from the right index
	for (int i=0; i< PAGE_SIZE; i++)
		result[i] = rx_data[i+4];

  	return MEMORY_SUCCESS;
}

MEMORY_RETURN memory_writeMemory(uint32_t address, uint8_t *data_buffer, uint32_t numOfBytes)
{
	if(!initiliazed)
		return MEMORY_NO_INIT;

	if (numOfBytes > PAGE_SIZE)
		DEBUG_BREAK

	uint8_t status;
	uint8_t dummy_rx[SPI_TRANSFER_SIZE];
	uint8_t tx_data[SPI_TRANSFER_SIZE];  // Need room for cmd + three address bytes

	tx_data[0] = PAGE_PROGRAM;
	tx_data[1] = (address >> 16);
	tx_data[2] = (address >> 8);
	tx_data[3] = address;

	for (int i=0; i < PAGE_SIZE; i++)
	{
		if (i >= numOfBytes) break;
		tx_data[i+4] = data_buffer[i];
	}

	config_write(WR_ENABLE);
	transferComplete = false;
	SPIDRV_MTransfer( handle, &tx_data, &dummy_rx, SPI_TRANSFER_SIZE, TransferComplete);
	while(!transferComplete)
		;

	do    status = read_status();
	while (status & WIP_BIT);

  	return MEMORY_SUCCESS;
}
