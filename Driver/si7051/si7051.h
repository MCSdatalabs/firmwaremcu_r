/**
 * @file			si7051.h
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef SI7051_H_
#define SI7051_H_

#include <stdint.h>
#include <stdbool.h>

#include "../i2c.h"

#define SI7051_ADDRESS		0x80	//!< SI7051 I2C address

/**
 * @brief Return codes for the SI7051.
 */
typedef enum {
	SI7051_SUCCESS = 0,				//!< Transfer completed successfully.
    SI7051_FAIL = -1,          		//!< SI7051 error.
    SI7051_I2C_ERR = -2,       		//!< I2C error.
    SI7051_WRONG_FIRMWARE = -10,	//!< Wrong firmware version received.
    SI7051_WRONG_ID = -11,      	//!< Wrong ID received.
    SI7051_NO_INIT = -99       		//!< SI7051 not initialized.
}SI7051_RETURN;

/**
 * @brief Checks if a SI7051 is connected.
 *
 * @attention This function must be called before any temperature measurement can be done.
 *
 * @return SI7051_RETURN
 * @retval SI7051_SUCCESS			Transfer completed successfully
 * @retval SI7051_I2C_ERR			I2C communication error
 * @retval SI7051_WRONG_ID			A different temperature sensor was detected but not initialized
 * @retval SI7051_WRONG_FIRMWARE	A different firmware version than 2.0 detected
 */
SI7051_RETURN si7051_detect(void);

SI7051_RETURN si7051_disable(void);

/**
 * @brief Get the measured temperature.
 *
 * @param temperature Measured temperature value as integer in 0.01�C
 *
 * @return SI7051_RETURN
 * @retval SI7051_SUCCESS	Transfer completed successfully
 * @retval SI7051_I2C_ERR	I2C communication error
 * @retval SI7051_NO_INIT	No SI7051 was detected
 */
//SI7051_RETURN si7051_measure();
//
///**
// * @brief Get the measured temperature.
// *
// * @param temperature Measured temperature value as integer in 0.01�C
// */
SI7051_RETURN si7051_getTemperature(int16_t *temperature);

#endif /* SI7051_H_ */
