/**
 * @file			si7051Defines.h
 * @date			20.04.2017
 *	
 * @details
 */

#ifndef SI7051DEFINES_H_
#define SI7051DEFINES_H_

/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */

#define SI7051_RH_READ_TEMP_READ           	0xE3
#define SI7051_RH_READ_TEMP_READ_NO_HOLD   	0xF3
#define SI7051_RH_READ_RESET               	0xFE
#define SI7051_USER1_WRITE         			0xE6
#define SI7051_RH_READ_USER1_READ          	0xE7
#define SI7051_READ_ID_FIRST_BYTE_1			0xFA
#define SI7051_READ_ID_FIRST_BYTE_2			0x0F
#define SI7051_READ_ID_SECOND_BYTE_1		0xFC
#define SI7051_READ_ID_SECOND_BYTE_2		0xC9
#define SI7051_READ_FIRMWARE_1				0x84
#define SI7051_READ_FIRMWARE_2				0xB8

#define SI7051_FIRMWARE_REV_10				0xFF
#define SI7051_FIRMWARE_REV_20 				0x20
#define SI7051_DEVICE_ID 					0x33

/** @endcond */

#endif /* SI7051DEFINES_H_ */
