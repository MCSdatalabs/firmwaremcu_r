/**
 * @file			si7021.c
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "si7051.h"
#include "si7051Defines.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/
static bool initialized = false;
//static int32_t localTemp = -27315;

/******************** private function ********************/

/******************** global function *********************/
SI7051_RETURN si7051_detect(void)
{
	initialized = false;
	uint8_t i2c_write_data[2];
	uint8_t i2c_read_data[8];

	i2c_write_data[0] = SI7051_READ_ID_FIRST_BYTE_1;
	i2c_write_data[1] = SI7051_READ_ID_FIRST_BYTE_2;
	if(i2c_writeRead(SI7051_ADDRESS, i2c_write_data, 2, i2c_read_data, 8) != 0)
		return SI7051_I2C_ERR;

	i2c_write_data[0] = SI7051_READ_ID_SECOND_BYTE_1;
	i2c_write_data[1] = SI7051_READ_ID_SECOND_BYTE_2;
	if(i2c_writeRead(SI7051_ADDRESS, i2c_write_data, 2, i2c_read_data, 8) != 0)
		return SI7051_I2C_ERR;

//	if(i2c_read_data[0] != SI7051_DEVICE_ID)
//		return SI7051_WRONG_ID;

	i2c_write_data[0] = SI7051_READ_FIRMWARE_1;
	i2c_write_data[1] = SI7051_READ_FIRMWARE_2;

	if(i2c_writeRead(SI7051_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
		return SI7051_I2C_ERR;

	if(i2c_read_data[0] != SI7051_FIRMWARE_REV_20)
		return SI7051_WRONG_FIRMWARE;

	initialized = true;

	return SI7051_SUCCESS;
}

SI7051_RETURN si7051_disable(void)
{
	initialized = false;

	return SI7051_SUCCESS;
}

SI7051_RETURN si7051_getTemperature(int16_t *temperature)
{
	*temperature = -27315;

	if(!initialized)
		return SI7051_NO_INIT;

	uint8_t i2c_write_data[1];
	uint8_t i2c_read_data[2];

	i2c_write_data[0] = SI7051_RH_READ_TEMP_READ;
	if(i2c_writeRead(SI7051_ADDRESS, i2c_write_data, 1, i2c_read_data, 2) != 0)
		return SI7051_I2C_ERR;

	int32_t temp = (int32_t)(i2c_read_data[0] << 8 | i2c_read_data[1]) * 17572/65536-4685;
	*temperature = (int16_t)temp;

	return SI7051_SUCCESS;

}
