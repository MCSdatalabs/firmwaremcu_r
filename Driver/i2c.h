/**
 * @file			i2c.h
 * @date			15.03.2017
 *	
 * @details
 */

#ifndef I2C_H_
#define I2C_H_

#include "em_cmu.h"
#include "em_i2c.h"
#include "em_gpio.h"

#ifndef I2C0_SCL_PIN
#define I2C0_SCL_PIN    (1)			//!< I2C SCL pin number.
#define I2C0_SCL_PORT   (gpioPortA)	//!< I2C SCL pin port.
#endif

#ifndef I2C0_SDA_PIN
#define I2C0_SDA_PIN    (0)			//!< I2C SDA pin number.
#define I2C0_SDA_PORT   (gpioPortA)	//!< I2C SDA pin port.
#endif

#define I2C_BUFFER_SIZE	32 			//!< I2C buffer size.

/**
 * @brief Return codes for i2c communication.
 */
typedef enum {
	I2C_TRANSFER_IN_PROGRESS = 1,   //!< Transfer in progress.
	I2C_SUCCESS = 0,     			//!< Transfer completed successfully.
	I2C_NACK = -1,        			//!< NACK received during transfer.
	I2C_BUS_ERR = -2,    			//!< Bus error during transfer (misplaced START/STOP).
	I2C_ARB_LOST = -3,   			//!< Arbitration lost during transfer.
	I2C_USAGE_FAULT = -4,			//!< Usage fault.
	I2C_SW_FAULT = -5,   			//!< SW fault.
	I2C_NO_INIT = -99    			//!< I2C not initialized.
}I2C_RETURN;

/**
 * @brief Initialize I2C
 *
 * The initialized i2c index is I2C0 on port PA0 and PA1 (route location zero).
 *
 * @return I2C_RETURN
 */
I2C_RETURN i2c_init(void);

/**
 * @brief Check if I2C is initialized
 *
 * @return I2C_RETURN
 * @retval I2C_SUCCESS i2c initialized
 * @retval I2C_NO_INIT i2c not initialized
 */
I2C_RETURN i2c_getInitialized(void);

I2C_RETURN i2c_disable(void);

/**
 * @brief Starts the I2C communication without reading.
 *
 * It is only possible to write data to a device.
 *
 * @param addr  I2C bus address in 7-bit format
 * @param txBuf TX buffer
 * @param txSize TX buffer size
 * @return I2C_RETURN
 */
I2C_RETURN i2c_write(uint8_t addr, uint8_t* txBuf, uint32_t txSize);

/**
 * @brief Starts the I2C communication with reading and writing.
 *
 * It is possible to write and read data at the same time.
 *
 * @param addr I2C bus address in 7-bit format
 * @param txBuf TX buffer
 * @param txSize TX buffer size
 * @param rxBuf RX buffer
 * @param rxSize RX buffer size, set to 0 if no data will be received
 * @return I2C_RETURN
 */
I2C_RETURN i2c_writeRead(uint8_t addr, uint8_t* txBuf, uint32_t txSize, uint8_t* rxBuf, uint32_t rxSize);

#endif /* I2C_H_ */
