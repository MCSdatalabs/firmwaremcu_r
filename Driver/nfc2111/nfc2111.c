/**
 * @file			nfc2111.c
 * @author			grli
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "nfc2111.h"
#include "nfc2111Defines.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/
static bool initialized = false;

/******************** private function ********************/

/******************** global function *********************/
NFC2111_RETURN nfc2111_detect(void)
{
	initialized = false;
	uint8_t i2c_write_data[1];
	uint8_t i2c_read_data[8];

	i2c_write_data[0] = NFC2111_GET_FIRST_BLOCK;
	if(i2c_writeRead(NFC2111_ADDRESS, i2c_write_data, 1, i2c_read_data, 16) != 0)
		return NFC2111_I2C_ERR;

	if(i2c_read_data[0] != NFC2111_SERIAL_NUMBER_FIRST_BYTE)
		return NFC2111_NO_INIT;

	initialized = true;
	return NFC2111_SUCCESS;
}

NFC2111_RETURN nfc2111_getSerialNumber(uint8_t *data)
{
	if(!initialized)
		return NFC2111_NO_INIT;

	uint8_t i2c_write_data[1];
	uint8_t i2c_read_data[8];

	i2c_write_data[0] = NFC2111_GET_FIRST_BLOCK;
	if(i2c_writeRead(NFC2111_ADDRESS, i2c_write_data, 1, i2c_read_data, 16) != 0)
		return NFC2111_I2C_ERR;

	uint8_t len = 0;
	for(len = 0; len < 8; len++)
		data[len] = i2c_read_data[len];

	return NFC2111_SUCCESS;
}
