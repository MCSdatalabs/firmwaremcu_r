/**
 * @file			nfc2111Defines.h
 * @author			GrLi
 * @date			21.04.2017
 *	
 * @details
 */

#ifndef NFC2111DEFINES_H_
#define NFC2111DEFINES_H_

/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */

#define NFC2111_GET_FIRST_BLOCK				0x00
#define NFC2111_SERIAL_NUMBER_FIRST_BYTE	0x04

/** @endcond */

#endif /* NFC2111DEFINES_H_ */
