/**
 * @file			nfc2111.h
 * @author			grli
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef NFC2111_H_
#define NFC2111_H_

#include <stdint.h>
#include <stdbool.h>

#include "../i2c.h"


#define NFC2111_ADDRESS	0xAA		//!< NFC2111 I2C address

/**
 * @brief Return codes for the NFC2111.
 */
typedef enum {
	NFC2111_SUCCESS = 0,			//!< Transfer completed successfully.
	NFC2111_FAIL = -1,          	//!< NFC2111 error.
	NFC2111_I2C_ERR = -2,       	//!< I2C error.
	NFC2111_NO_INIT = -99       	//!< NFC2111 not initialized.
}NFC2111_RETURN;

/**
 * @brief Checks if a SI1133 is connected.
 *
 * @return NFC2111_RETURN
 * @retval NFC2111_SUCCESS			Transfer completed successfully
 * @retval NFC2111_I2C_ERR			I2C communication error
 * @retval NFC2111_NO_INIT			NFC2111 not initialized
 */
NFC2111_RETURN nfc2111_detect(void);

/**
 * @brief Reads the serial number from the NFC Tag.
 *
 * @param data Returns the serial number as an array of 8 Values.
 *
 * @return NFC2111_RETURN
 * @retval NFC2111_SUCCESS			Transfer completed successfully
 * @retval NFC2111_I2C_ERR			I2C communication error
 * @retval NFC2111_NO_INIT			NFC2111 not initialized
 */
NFC2111_RETURN nfc2111_getSerialNumber(uint8_t *data);

#endif /* NFC2111_H_ */
