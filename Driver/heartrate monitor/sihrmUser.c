/**************************************************************************//**
 * @brief Implementation specific functions for HRM code
 * @version 3.20.3
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include "sihrmUser.h"
#include "si114x_functions.h"
#include "heartrate_monitor.h"
#include "lib/si114x/dynamic/sihrm.h"
//#include "accel_sys_out.h"
#include "rtcdriver.h"

/*  I2C port configuration */
static Si114xPortConfig_t _handle;

/*  interrupt sequence counter  */
static uint16_t irqSequence = 0;
/*  interrupt queue size in bytes  */
#define	IRQ_QUEUE_SIZE	360			//15 x sizeof(SihrmIrqSample_t)
/*  interrupt queue data */
static u8 IrqQueue[IRQ_QUEUE_SIZE];
/*  interrupt queue current get index  */
static u16 irqQueueGetIndex = 0;
/*  interrupt queue current put index  */
static u16 irqQueuePutIndex = 0;

static bool newAccelData = false;
static int16_t accel_x = 0;
static int16_t accel_y = 0;
static int16_t accel_z = 0;


/*  Non-exported Function Prototypes  */
static int32_t sihrmUser_i2c_smbus_write_byte_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t data, uint8_t block);
static int32_t sihrmUser_i2c_smbus_read_byte_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t *data, uint8_t block);
static int32_t sihrmUser_i2c_smbus_write_i2c_block_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t const* data, uint8_t block);
static int32_t sihrmUser_i2c_smbus_read_i2c_block_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t* data, uint8_t block);
static int32_t sihrmUser_SampleQueue_Put(sihrmUserHandle_t sihrmUser_handle, SihrmIrqSample_t *samples);


/**************************************************************************//**
 * @brief Enables/Disables USB debug output.
 *****************************************************************************/
int32_t sihrmUser_SetupDebug(sihrmUserHandle_t sihrmUser_handle, int32_t enable, void *debug)
{
  (void) sihrmUser_handle;
//do nothing here...everything is done in usb_debug.c and sihrm.c
  return SIHRM_SUCCESS;
}
/**************************************************************************//**
 * @brief Prints USB debug output.
 *****************************************************************************/
int32_t sihrmUser_OutputDebugMessage(sihrmUserHandle_t sihrmUser_handle, const uint8_t *message)
{
#if defined(FLASH_DEBUG) || defined(USB_DEBUG)
  uint16_t i;
  uint8_t message_buffer[((99)+3)&~3] __attribute__ ((aligned(4)));
  uint8_t *Message_Buffer[1] = {message_buffer};
  (void) sihrmUser_handle;
  message_buffer[0] = 0x10;
  for(i=0; i<strlen((const char *)message); i++)
    message_buffer[i+1] = message[i];
  message_buffer[strlen((const char *)message)+1] = 0x10;
  message_buffer[strlen((const char *)message)+2] = 0x0D;
#endif

#ifdef FLASH_DEBUG

	if (message_buffer[1] == 0x43)   { // 'C' is configuration, only thing in flash page.
	  WR_Samples_to_Flash(message_buffer, strlen(message) + 3, true);

	} else	if (dump_IRQ_message_to_flash  == true)  {
		/* DLE shielded message such as:
		  80032140: xx xx xx xx xx xx xx xx xx xx xx xx 10 44 46 73    00000000000  DFs
		  80032150: 20 3D 20 32 35 39 2C 20 50 69 20 3D 20 33 34 32     = 259, Pi = 342
		  80032160: 2C 20 53 74 61 74 75 73 20 3D 20 30 2C 20 50 75    , Status = 0, Pu
		  80032170: 6C 73 65 20 3D 20 36 36 62 70 6D 2C 20 53 70 4F    lse = 66bpm, SpO
		  80032180: 32 20 3D 20 38 39 25 10 0D xx xx xx xx xx xx xx    2 = 89%       03
		*/
	  WR_Samples_to_Flash(message_buffer, strlen(message) + 3, false);
	}; // if (dump_IRQ_message_to_flash == true)
#endif //FLASH_DEBUG

#ifdef USB_DEBUG
  USBD_Write(CDC_EP_DATA_IN, (void*) Message_Buffer[0], strlen((const char *)message)+3, NULL);
#endif
  return SIHRM_SUCCESS;
}


/**************************************************************************//**
 * @brief Outputs sample to USB debug interface
 *****************************************************************************/
int32_t sihrmUser_OutputSampleDebugMessage(sihrmUserHandle_t sihrmUser_handle, SihrmIrqSample_t *sample)
{
#ifdef USB_DEBUG

  if (USBDebug_IsUSBConfigured() == true)                                    // Dump data to USB if usb_debug is enabled
    USBDebug_ProcessUSBOutput((int8_t *)sample);
#endif // si114xHRM_USB_DEBUG

#ifdef FLASH_DEBUG
  FlashDebug_ProcessUSBOutput((int8_t *)sample);
#endif

  return SIHRM_SUCCESS;
}


/**************************************************************************//**
 * @brief Write to Si114x register
 *****************************************************************************/
int32_t sihrmUser_WriteToRegister(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t data)
{
  return sihrmUser_i2c_smbus_write_byte_data(sihrmUser_handle, address, data, true);
}

/**************************************************************************//**
 * @brief Read from Si114x register.
 *****************************************************************************/
int32_t sihrmUser_ReadFromRegister(sihrmUserHandle_t sihrmUser_handle, uint8_t address)
{	
  u8 data;
  sihrmUser_i2c_smbus_read_byte_data(sihrmUser_handle, address, &data, true);
  return data;
}

/**************************************************************************//**
 * @brief block write to si114x
 *****************************************************************************/
int32_t sihrmUser_BlockWrite(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t const *values)
{
  return sihrmUser_i2c_smbus_write_i2c_block_data(sihrmUser_handle,
                                               address,
                                               length,
                                               values, 
                                               true);
}

/**************************************************************************//**
 * @brief Block read from Si114x.
 *****************************************************************************/
int32_t sihrmUser_BlockRead(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t *values)
{
    return sihrmUser_i2c_smbus_read_i2c_block_data(sihrmUser_handle,
                           address,    length,     values, true);
}

/**************************************************************************//**
 * @brief Disable GPIO interrupt for Si114x interrupt.
 *****************************************************************************/
void DisableSi114xInterrupt ()
{

	GPIO_IntDisable(1<<_handle.irqPin);
}

/**************************************************************************//**
 * @brief Enable GPIO interrupt for Si114x.
 *****************************************************************************/
void EnableSi114xInterrupt ()
{
	if (GPIO_PinInGet(_handle.irqPort, _handle.irqPin) == 0)
		GPIO_IntSet(1<<_handle.irqPin);
	GPIO_IntEnable(1<<_handle.irqPin);
}

/**************************************************************************//**
 * @brief Main interrupt processing routine for Si114x.
 *****************************************************************************/
int32_t sihrmUser_ProcessIrq(sihrmUserHandle_t sihrmUser_handle, uint16_t timestamp)
{
  u8 data_buffer[13];
  s16 error;
  SihrmIrqSample_t sample;
  irqSequence++;
  sihrmUser_i2c_smbus_read_i2c_block_data(sihrmUser_handle, 0x21, 13, data_buffer, false);		// Grab all data registers
  sihrmUser_i2c_smbus_write_byte_data(sihrmUser_handle, 0x21, data_buffer[0], false);      // Clear interrupts

  sample.sequence = irqSequence;       // sequence number
  sample.timestamp = timestamp;      // 16-bit Timestamp to record
  sample.pad= 0;
  sample.irqstat = data_buffer[0];        // 8-bit irq status
  sample.vis = (((u16)(data_buffer[2]) << 8) & 0xff00) | data_buffer[1];            // VIS
  sample.ir = (((u16)(data_buffer[4]) << 8) & 0xff00) | data_buffer[3];             // IR
  sample.ps1 = (((u16)(data_buffer[6]) << 8) & 0xff00) | data_buffer[5];            // PS1
  sample.ps2 = (((u16)(data_buffer[8]) << 8) & 0xff00) | data_buffer[7];            // PS2
  sample.ps3 = (((u16)(data_buffer[10]) << 8) & 0xff00) | data_buffer[9];            // PS3
  sample.aux = (((u16)(data_buffer[12]) << 8) & 0xff00) | data_buffer[11];;            // AUX

  if(newAccelData)
  {
	  sample.accelerometer_x = accel_x;
	  sample.accelerometer_y = accel_y;
	  sample.accelerometer_z = accel_z;
	  newAccelData = false;
  }
  else
  {
	  sample.accelerometer_x = 0;
	  sample.accelerometer_y = 0;
	  sample.accelerometer_z = 0;

  }
  error = sihrmUser_SampleQueue_Put(sihrmUser_handle, &sample);
  return error;
}


void sihrmUser_setAccelData (int16_t *accel)
{
	newAccelData = true;
	accel_x = accel[0];
	accel_y = accel[1];
	accel_z = accel[2];
}

/**************************************************************************//**
 * @brief Query number of entries in the interrupt queue.
 *****************************************************************************/
int32_t sihrmUser_SampleQueueNumentries(sihrmUserHandle_t sihrmUser_handle)
{	
  (void) sihrmUser_handle;
  u16 runnerIndex = irqQueueGetIndex;
  s16 count=0;
  while (runnerIndex != irqQueuePutIndex)
  {
    runnerIndex++;
    count++;
    if(runnerIndex == IRQ_QUEUE_SIZE)
      runnerIndex = 0;
  }
  return (count/sizeof(SihrmIrqSample_t));

}

/**************************************************************************//**
 * @brief Get sample from the interrupt queue.
 *****************************************************************************/
int32_t sihrmUser_SampleQueue_Get(sihrmUserHandle_t sihrmUser_handle, SihrmIrqSample_t *samples)
{	
  (void) sihrmUser_handle;
  int16_t error = 0;
  uint16_t i;
  int8_t *data = (int8_t *)samples;
  DisableSi114xInterrupt ();
  if (irqQueueGetIndex == irqQueuePutIndex)
    error = -1;
  else
  {
    for(i=0; i<sizeof(SihrmIrqSample_t); i++)
    {
      data[i] = IrqQueue[irqQueueGetIndex];
      irqQueueGetIndex++;
      if(irqQueueGetIndex == IRQ_QUEUE_SIZE)
        irqQueueGetIndex = 0;

    }

  }
  EnableSi114xInterrupt();

/*  In previous versions this is where we dumped the sample our USB and stored to flash.
 *  That is now done by the algorithm via the function si114xOutputSampleDebugMessage()
 */



  return error;
}

/**************************************************************************//**
 * @brief Put new sample in the interrupt queue.
 *****************************************************************************/
static int32_t sihrmUser_SampleQueue_Put(sihrmUserHandle_t sihrmUser_handle, SihrmIrqSample_t *samples)
{
  uint16_t i;
  u8 *data = (u8 *)samples;
  (void) sihrmUser_handle;

  for(i=0; i<sizeof(SihrmIrqSample_t); i++)
  {
    IrqQueue[irqQueuePutIndex] = data[i];
    irqQueuePutIndex++;
    if(irqQueuePutIndex == IRQ_QUEUE_SIZE)
      irqQueuePutIndex = 0;
  }
  if (irqQueueGetIndex == irqQueuePutIndex)
  {
    irqQueueGetIndex += sizeof(SihrmIrqSample_t);			// if we have wrapped around then we must delete one sample
    if(irqQueueGetIndex >= IRQ_QUEUE_SIZE)						//handle wrapping
    	irqQueueGetIndex = irqQueueGetIndex - IRQ_QUEUE_SIZE;
    return -1; //indicate to caller something bad happened
  }
  return 0;
}

/**************************************************************************//**
 * @brief Empty the interrupt queue.
 *****************************************************************************/
int32_t sihrmUser_SampleQueue_Clear(sihrmUserHandle_t sihrmUser_handle)
{	
  (void) sihrmUser_handle;
  irqQueueGetIndex = 0;
  irqQueuePutIndex = 0;
  return 0;
}

/**************************************************************************//**
 * @brief Initialize low level handle and clear irq queue.
 *****************************************************************************/
int32_t sihrmUser_Initialize(void *port, int16_t options, sihrmUserHandle_t *sihrmUser_handle)
{	
  s16 error = 0;
  u8 data;
  (void) options;
  *sihrmUser_handle = (HANDLE)&_handle;
  _handle.i2cAddress = ((Si114xPortConfig_t*)port)->i2cAddress<<1;
  _handle.i2cBusSelect = ((Si114xPortConfig_t*)port)->i2cBusSelect;
  _handle.irqPort = ((Si114xPortConfig_t*)port)->irqPort;
  _handle.irqPin = ((Si114xPortConfig_t*)port)->irqPin;

  data = sihrmUser_ReadFromRegister((*sihrmUser_handle), REG_PART_ID);

  if ((_handle.i2cAddress == (0x60<<1)) && (data != 0x46) && (data != 0x47))
    error = -1;
  if ((_handle.i2cAddress == (0x5A<<1)) && (data != 0x43) && (data != 0x44))
    error = -1;

  sihrmUser_SampleQueue_Clear(*sihrmUser_handle);

  return error;
}

/**************************************************************************//**
 * @brief Close Si114x.
 *****************************************************************************/
int32_t sihrmUser_Close(sihrmUserHandle_t sihrmUser_handle)
{	
  (void) sihrmUser_handle;
  _handle.i2cAddress = 0xff;
  return 0;
}

/**************************************************************************//**
 * @brief Reset not implemented.
 *****************************************************************************/
s16 sihrmUser_SysReset(HANDLE si114x_handle)
{
  (void) si114x_handle;
  return 0;
}

/**************************************************************************//**
 * @brief 10ms delay required by Si114x reset sequence.
 *****************************************************************************/
void delay_10ms(void)
{
  RTCDRV_Delay(10);
  return;
}

/**************************************************************************//**
 * @brief Write to Si114x i2c.
 *****************************************************************************/
static int32_t sihrmUser_i2c_smbus_write_byte_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t data, uint8_t block)
{
  I2C_RETURN ret;
  uint8_t i2c_write_data[2];
  uint8_t i2c_read_data[1];
  (void) sihrmUser_handle;
  if (block)
    DisableSi114xInterrupt ();

  i2c_write_data[0] = address;
  i2c_write_data[1] = data;

  ret = i2c_writeRead(_handle.i2cAddress, i2c_write_data, 2, i2c_read_data, 1);


  if (block)
    EnableSi114xInterrupt();
  if (ret != I2C_SUCCESS)
  {
    return((int) ret);
  }
  return((int) 0);
}

/**************************************************************************//**
 * @brief Read from Si114x i2c.
 *****************************************************************************/
static int32_t sihrmUser_i2c_smbus_read_byte_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t *data, uint8_t block)
{
  //  si114x_handle is not used in the EFM32.   We use a global instead
  I2C_RETURN ret;
  uint8_t i2c_write_data[1];
  (void) sihrmUser_handle;
  if (block)
    DisableSi114xInterrupt ();

  i2c_write_data[0] = address;

  do
  {
	  ret = i2c_writeRead(_handle.i2cAddress, i2c_write_data, 1, data, 1);
	  if(ret == I2C_TRANSFER_IN_PROGRESS)
		  delay(1);
  } while (ret == I2C_TRANSFER_IN_PROGRESS);

  if (block)
    EnableSi114xInterrupt ();
  if (ret != I2C_SUCCESS)
  {
    *data = 0xff;
    return((int) ret);
  }
  return((int) 1);
}

/**************************************************************************//**
 * @brief Write block of data to Si114x i2c.
 *****************************************************************************/
static int32_t sihrmUser_i2c_smbus_write_i2c_block_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t const* data, uint8_t block)
{
	I2C_RETURN ret;
  uint8_t i2c_write_data[10];
  uint8_t i2c_read_data[1];
  int i;
  (void) sihrmUser_handle;
  if (block)
    DisableSi114xInterrupt ();

  i2c_write_data[0] = address;
  for (i = 0; i < length; i++)
    i2c_write_data[i+1] = data[i];

  ret = i2c_writeRead(_handle.i2cAddress, i2c_write_data, length + 1, i2c_read_data, 1);

  if (block)
    EnableSi114xInterrupt ();
  if (ret != I2C_SUCCESS)
  {
    return((int) ret);
  }

  return((int) 0);
}

/**************************************************************************//**
 * @brief Read block of data from Si114x i2c.
 *****************************************************************************/
static int32_t sihrmUser_i2c_smbus_read_i2c_block_data(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t* data, uint8_t block)
{
  I2C_RETURN ret;
  uint8_t i2c_write_data[1];
  (void) sihrmUser_handle;
  if (block)
    DisableSi114xInterrupt ();

  i2c_write_data[0] = address;

  ret = i2c_writeRead(_handle.i2cAddress, i2c_write_data, 1, data, length);

  if (block)
    EnableSi114xInterrupt ();
  if (ret != I2C_SUCCESS)
  {
    return((int) ret);
  }

  return((int) 0);
}


