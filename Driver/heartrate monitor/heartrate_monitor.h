/**************************************************************************//**
 * @file
 * @brief Heart Rate state machine
 * @version 3.20.3
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#ifndef __HEARTRATEMONITOR_H
#define __HEARTRATEMONITOR_H
//#include "accel_sys_out.h"

#include "em_cmu.h"
#include "em_gpio.h"
#include "em_letimer.h"
#include "sihrmUser.h"

typedef enum
{
	DYNAMIC_BIOMETRIC_EXP,
	//DYNAMIC_SI1143_PS,
	DYNAMIC_FITNESS_EVB_GGG,
	//DYNAMIC_FITNESS_EVB_GRI,
	//DYNAMIC_SI1147_PS
}HeartRateMonitorDynamic_Config_t ;

typedef enum
{
  STATIC_BIOMETRIC_EXP,
  //STATIC_SI1143_PS,
  //STATIC_SI1147_PS
}HeartRateMonitorStatic_Config_t ;

typedef enum HRMSpO2State
{
	HRM_STATE_IDLE,				//hrm is not running...the applicaiton periodically checks for skin contact
	HRM_STATE_NOSIGNAL,
	HRM_STATE_ACQUIRING,		//hrm has started but it have not yet processed its first frame
	HRM_STATE_ACTIVE,		//hrm is running normally
	HRM_STATE_INVALID
} HRMSpO2State_t;

typedef struct Si114xPortConfig
{
    uint8_t i2cBusSelect;
	uint8_t i2cAddress;
	GPIO_Port_TypeDef irqPort;
	int irqPin;
}Si114xPortConfig_t;

typedef struct HeartRateConfig
{
	Si114xPortConfig_t i2cPort;
	HRMSpO2State_t hrmState;
	char hrmVersion[32];
	bool dynamicLibUsed;
	bool hrmCheckSkinContact;
	bool hrmActive;
	int16_t hrmPulse;
	int16_t hrmSpo2;
}HeartRateConfig_t;

#define HRM_Idle_Check 0

void HeartRateMonitor_Init( Si114xPortConfig_t* i2c, bool dynamicLibUsed, HeartRateMonitorDynamic_Config_t dynamicConfigSelect, HeartRateMonitorStatic_Config_t staticConfigSelect, int debugEnabled );
bool HeartRateMonitor_Loop (bool dynamicLibUsed);
//void HeartRateMonitor_TimerEventHandler(void);
void HeartRateMonitor_Interrupt (bool dynamicLibUsed);
bool HeartRateMonitor_SamplesPending (bool dynamicLibUsed);
void HeartRateMonitor_Stop(bool dynamicLibUsed);

void HeartRateMonitor_GetVersion (bool dynamicLibUsed, char *hrmVersion);

void HeartRateMonitor_GetData(bool dynamicLibUsed, bool *skinContact, int16_t *pulse, int16_t *spo2);

#endif /* __HEARTRATEMONITOR_H */
