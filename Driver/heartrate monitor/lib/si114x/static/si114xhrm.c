/*********************************************************************************
  This software is confidential and proprietary information owned by Silicon
  Labs which is subject to worldwide copyright protection.  No use or other
  rights associated with this software may exist other than as expressly
  stated by written agreement with Silicon Labs.  No licenses to the
  intellectual property rights included in the software, including patent and
  trade secret rights, are otherwise given by implication, estoppel or otherwise.

***********************************************************************************/


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "../../../../heartrate monitor/si114x_functions.h"
#include "si114xhrm.h"
#include "../../../../heartrate monitor/si114x_types.h"
#include "si114xhrm_config.h"

#if (SI114xHRM_BUILD_TARGET == SI114xHRM_BUILD_TARGET_GECKO)
  //#include "si114xhrm_user_functions.h"
  #include "../../../../heartrate monitor/sihrmUser.h"
  #include "em_device.h"
#endif

//-----------------------------------------------------------------------------
//	GLOBAL VARIABLES




/****************************************************************************
*  Non-Exported Defines
*****************************************************************************/
#define	si114xHRM_VERSION	"1.0.3"    //should never be more than 32 characters long

#define QF_SCALER				8		//Q-format for Normalization_Scaler
#define FS_ESTIMATE_DURATION	60		// (sample)
#define FS_UPDATE_RATE			30		// (%)

//#define ZR_BIAS_SCALE	(+1/4) //%butter BPF, BPF(PS) biases to positive side
#define ZR_BIAS_SCALE	(-1/4) //%cheby2 BPF, BPF(PS) biases to negative side
#define ZC_HR_TOLERANCE 25

#define HRM_PS_DC_REFERENCE	15000	//Used for the auto HRM PS AC scaler based on the 1s-averaged PS DC.
#define HRM_PS_VPP_MAX_GREEN_FINGERTIP		HRM_PS_DC_REFERENCE*30/300  //PI=20.0% if positive and negative AC amplitudes are same.

#define HRM_CREST_FACTOR_THRESH_FINGERTIP	15

#define	HRM_PS_RAW_SKIN_CONTACT_THRESHOLD	1500		/*	if PS raw value is greater than this
                              threshold the algorithm believes that
                              skin contact is detected
                            */
#define HRM_DC_SENSING_SKIN_CONTACT_THRESH	HRM_PS_RAW_SKIN_CONTACT_THRESHOLD*10/10
#define HRM_DC_SENSING_WORK_LEVEL	HRM_DC_SENSING_SKIN_CONTACT_THRESH*60/10 //PS=9000 results in AC range of 9 at PI=0.1%.
#define HRM_PI_MIN_THRESHOLD				20	//20 means PI=0.2%		//If PI is below the threshold, the HRM AGC increases the LED current by 1 notch, up to 0x0f.

#define HRM_AMBIENT_BLOCKED_THRESHOLD		500

#define HRM_DC_SENSING_START				0x00	//DC sensing scheme is looking for the next DC sensing.
#define HRM_DC_SENSING_DELAY				0x01	//Delay to allow the finger off-to-on transmition passing, and then initialize the variable for DC sensing.
#define HRM_DC_SENSING_CHANGE_PARAMETERS	0x02	//Change LED current from 0xf to 0x1, and then measure the PS as DC for each current.
#define HRM_DC_SENSING_SENSE_FINISH			0x04	//Finish the DC measurement and select the proper current for the HRM opeartion.
#define HRM_DC_SENSING_RESTORE_FS			0x08	//Restore Fs if the Fs was changed durng the DC sensing.
#define HRM_DC_SENSING_SEEK_NO_CONTACT		0x10	//Monitor to see if PS is below the finger-off threshold. Resture Si114x current and set HRM_DC_Sensing_Flag=HRM_DC_SENSING_START for the next DC sensing.

#define INTERPOLATOR_L 4

#define HRM_BPF_ACTIVE			0x01				//BPF_Active_Flag bit
#define SpO2_RED_BPF_ACTIVE		0x02				//BPF_Active_Flag bit
#define SpO2_IR_BPF_ACTIVE		0x04				//BPF_Active_Flag bit

#if	HRM_DATA_LOG
  FILE * pFile_TS_PS_output;
#endif

/****************************************************************************
*  Types
*****************************************************************************/
  //BPF parameters
#define	BPF_BIQUAD_STAGE_MAX	3		//Coefs of all BPFs below have 3 stages.
typedef struct
{
  int32_t x[BPF_BIQUAD_STAGE_MAX][3];		//BPF data buffer
  int32_t yi[BPF_BIQUAD_STAGE_MAX][3];	//BPF data buffer
  int32_t yf[BPF_BIQUAD_STAGE_MAX][3];	//BPF data buffer
} SI114X_BPF;

typedef struct {
  SI114X_BPF SpO2_Red_BPF, SpO2_IR_BPF; //BPF structs for SpO2_Red and SpO2_IR

  int16_t SpO2_RED_AC_sample_buffer[MAX_FRAME_SAMPLES];				//data buffer for the framed samples that can be processed in a background job.
  int16_t SpO2_IR_AC_sample_buffer[MAX_FRAME_SAMPLES];				//data buffer for the framed samples that can be processed in a background job.
  uint16_t SpO2_RED_DC_sample_buffer[MAX_FRAME_SAMPLES];				//data buffer for the framed samples that can be processed in a background job.
  uint16_t SpO2_IR_DC_sample_buffer[MAX_FRAME_SAMPLES];				//data buffer for the framed samples that can be processed in a background job.

  //Use each of 4 SpO2 frame sample buffers as a circular buffer, so that SpO2 sample process adds samples to it and SpO2 frame process outputs samples from it.
  int16_t SpO2_Buffer_In_index;										//Input pointer to the frame sample circular buffer.
    int16_t SpO2_Percent;												//SpO2(%) and the debug message
  int16_t SpO2_raw_level_count;										//Used for Finger On/Off validation
  uint16_t SpO2_Red_interpolator_buf[INTERPOLATOR_L*2], SpO2_IR_interpolator_buf[INTERPOLATOR_L*2];
  //These were local static vars in V0.6.2:
  uint16_t SpO2_Red_BPF_Ripple_Count;									//SpO2 BPF-ripple reduction variable
  uint16_t SpO2_Red_PS_Input_old;										//SpO2 BPF-ripple reduction variable
  uint16_t SpO2_IR_BPF_Ripple_Count;									//SpO2 BPF-ripple reduction variable
  uint16_t SpO2_IR_PS_Input_old;										//SpO2 BPF-ripple reduction variable
  uint16_t TimeSinceFingerOff;											//(sample)
  uint16_t spo2DcSensingResult[15*2];								//15 locations for 15 currents per LED.
} Si114xSPO2_STRUCT;

typedef struct {
    Si114xSPO2_STRUCT *spo2;
  HANDLE si114x_handle;

  int32_t uv_part;													//0 = non-uv part (Si1141/2/3; 1=uv_part(Si1132, Si1145/6/7)
  int16_t	measurement_rate;
  int16_t Fs;															//(Hz) Si114x PS sampling rate
  int16_t BPF_biquads, BPF_output_scaler;
  int32_t *pBPF_b_0;													//pointer to the first BPF coefs.
  int16_t hrUpdateInterval;											//HRM_Inits set to 1(s)*Fs
  int16_t	T_Low0,T_High0;												//(sample), times for min and max heart rates.
  int16_t	hrIframe;													//(sample), Frame length.
  int16_t NumOfLowestHRCycles;										//# of lowest-HR cycle. The longer the frame scale is, more accurate the heart rate is.
  int16_t HRM_sample_buffer[MAX_FRAME_SAMPLES];						//data buffer for the framed samples that can be processed in a background job.
  int16_t HRM_Buffer_In_Index;										//Input index to the frame sample circular buffer.
  SI114X_BPF HRM_BPF;												//BPF structs for HRM
  uint16_t HRM_Normalization_Scaler;									//Normalization scaler for HRM PS based on ADC gain, LED current and etc.
  uint16_t RED_Normalization_Scaler;									//Normalization scaler for SpO2 Red PS based on ADC gain, LED current and etc.
  uint16_t IR_Normalization_Scaler;									//Normalization scaler for SpO2 IR PS based on ADC gain, LED current and etc.
  int16_t sample_count;												//sample count
  int16_t	HRM_PS_raw_level_count;
  uint16_t hrmRawPs;
  uint16_t Normalized_HRM_PS;
  int16_t HRM_PS_Vpp_max;
  int16_t HRM_PS_Vpp_min;
  int16_t HRM_PS_CrestFactor_Thresh;

  uint8_t hrm_ps_select;			//0=PS1, 1=PS2, 2=PS3
  uint8_t spo2_ir_ps_select;		//0=PS1, 1=PS2, 2=PS3
  uint8_t spo2_red_ps_select;		//0=PS1, 1=PS2, 2=PS3
  uint16_t hrmDcSensingResult[15];	//15 locations for 15 LED currents.
  uint16_t Si114x_adcgain_current;	//HRM DC Sensing: ADCgain<<12+current[2]<<8+current[1]<<4+current[0]
  uint16_t Si114x_adcgain_current0;//HRM DC Sensing: Initial ADCgain and currents
  uint8_t HRM_PS_AGC_Flag;
  uint8_t HRM_Interpolator_Factor;
  uint16_t HRM_interpolator_buf[INTERPOLATOR_L*2];
  int16_t *pHRM_Interpolator_Coefs;
  int32_t flag_samples;
  uint16_t TimeStamp_Old;												//Used for Fs estimation from the time stamp
  uint16_t dTS_count;													//Used for Fs estimation from the time stamp
  int32_t Accumlated_dTS;												//Used for Fs estimation from the time stamp
  uint16_t Accumlated_dTS_count;										//Used for Fs estimation from the time stamp
  uint16_t HRM_DC_Sensing_Count;										//Used in HRM DC sensing
  uint16_t HRM_DC_Sensing_Count1;										//Used in HRM DC sensing
  uint16_t HRM_Raw_PS_old;
  uint16_t HRM_DC_Sensing_Flag;										//Used in HRM DC sensing
  uint16_t Si114x_adcgain_current_Sensing;								//Used for PS1/PS2/PS3 in DC sensing
  uint16_t HRM_current_mask;											//Used in HRM DC sensing
  uint16_t HRM_current;												//Used in HRM DC sensing
  uint16_t PS_AGC_count;												//PS AGC
  int16_t meas_rate_DC_Sensing;										//Used for Fs=10 & 25Hz HRM & SpO2
  uint8_t HRM_Interpolator_Factor_Saved;								//Used for Fs=10 & 25Hz HRM & SpO2
  uint8_t BPF_Active_Flag;												//BPF-ripple-reduction flags
  uint16_t HRM_BPF_Ripple_Count;										//HRM BPF-ripple reduction variable
  int16_t HeartRateInvalidation_Previous;
  uint32_t HRM_PS_DC;
  uint16_t hrmPerfusionIndex;											//20 means PI=20/10000=0.2%
  uint16_t HRM_PS_Input_old;											//HRM BPF-ripple reduction variable
  uint16_t timestampClockFreq;											// Actual Fs. (10000 or 8192)
} SI114xHRM_STRUCT;


/****************************************************************************
*  Non-Exported Function Prototypes
*****************************************************************************/
static int si114xhrm_EnableInterrupts(Si114xhrmHandle_t _handle);
static int si114xhrm_DisableInterrupts(Si114xhrmHandle_t _handle);
static int si114xhrm_GetSample(Si114xhrmHandle_t handle, SI114X_IRQ_SAMPLE *samples);
static int si114xhrm_FrameProcess(Si114xhrmHandle_t handle, int16_t *heart_rate, int32_t *HeartRateInvalidation, Si114xhrmData_t *hrm_data);
static int si114xhrm_SampleProcess(Si114xhrmHandle_t handle, uint16_t HRM_timestamp, uint16_t hrmPs, uint16_t SpO2_PSred, uint16_t SpO2_PSir, Si114xhrmData_t *hrm_data);
static int si114xhrm_InitMeasurementParameters (Si114xhrmHandle_t handle, int16_t measurement_rate);
static int si114xhrm_InitializeBuffers(Si114xhrmHandle_t handle);
static int si114xhrm_BPF_filtering(Si114xhrmHandle_t handle, int PS_input, short *pPS_output, SI114X_BPF *pBPF);
static int si114xhrm_Pick_LED_Current(uint16_t *hrmDcSensingResult);
static int32_t si11xhrm_IsAmbientBlocked(uint16_t als_vis);

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  static int si114xhrm_SpO2FrameProcess(Si114xhrmHandle_t handle, int16_t *SpO2, int32_t *HeartRateInvalidation, Si114xhrmData_t *hrm_data);
#endif
static int32_t si114xhrm_IdentifyPart(Si114xhrmHandle_t _handle, int16_t *part_id);


/****************************************************************************
*  Global Variables and Constants
*****************************************************************************/
#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_60Hz == 1)
static int32_t BPF_60Hz[]={ //Q16. Fs=79; [BPF_b,BPF_a] = cheby2(3,35, [18, 500]/60/Fs*2); 0dB at 45BMP. -1dB at 160BPM, -6dB at 200BPM. No overflow.
                0x00001000, 0x00000000, 0xFFFFF000, 0x0000FFFF, 0xFFFE43CF, 0x0000BFDC,
                0x00004000, 0xFFFFA265, 0x00004000, 0x0000FFFF, 0xFFFE4604, 0x0000CF60,
                0x0000FFFF, 0xFFFE001E, 0x0000FFFF, 0x0000FFFF, 0xFFFE0A1A, 0x0000F69F
              };
static const short BPF_output_scaler_60Hz=0x7ebc; //Q15
#endif

#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_95Hz == 1)
static int32_t BPF_95Hz[]={	
                0x00001000, 0x00000000, 0xFFFFF000l, 0x0000FFFF, 0xFFFE37E6, 0x0000CA9D,
                0x00004000, 0xFFFF97C4, 0x00004000, 0x0000FFFF, 0xFFFE36EF, 0x0000D793,
                0x0000FFFF, 0xFFFE0015, 0x0000FFFF, 0x0000FFFF, 0xFFFE0832, 0x0000F84B
              };
static const short BPF_output_scaler_95Hz=0x679c; //Q15
#endif

#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_185Hz == 1)
static int32_t BPF_185Hz[]={	//Q16
                0x00000CCD, 0x00000000, 0xFFFFF333, 0x0000FFFF, 0xFFFE1D35, 0x0000E37D,
                0x00003333, 0xFFFF9ED3, 0x00003333, 0x0000FFFF, 0xFFFE1961, 0x0000EA8B,
                0x0000CCCC, 0xFFFE666C, 0x0000CCCC, 0x0000FFFF, 0xFFFE041C, 0x0000FC06
               };
static const short BPF_output_scaler_185Hz=0x6aac; //Q15
#endif

#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_229Hz == 1)
static int32_t BPF_229Hz[]={	 //Q16
                0x00000CCD, 0x00000000, 0xFFFFF333, 0x0000FFFF, 0xFFFE17B5, 0x0000E8C1,
                0x00003333, 0xFFFF9D08, 0x00003333, 0x0000FFFF, 0xFFFE140B, 0x0000EE89,
                0x0000CCCC, 0xFFFE666B, 0x0000CCCC, 0x0000FFFF, 0xFFFE034E, 0x0000FCC8
              };
static const short BPF_output_scaler_229Hz=0x5727; //Q15
#endif

#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_430Hz == 1)
static int32_t BPF_430Hz[]={	//Q16
                0x00000AAB, 0x00000000, 0xFFFFF555, 0x0000FFFF, 0xFFFE0CC9, 0x0000F359,
                0x00002AAB, 0xFFFFAB7D, 0x00002AAA, 0x0000FFFF, 0xFFFE0A36, 0x0000F689,
                0x0000AAAA, 0xFFFEAAAD, 0x0000AAAA, 0x0000FFFF, 0xFFFE01C1, 0x0000FE47
              };
static const short BPF_output_scaler_430Hz=0x52ab; //Q15
#endif


static const short HRM_Interpolator_Coefs_R9[]={//In Q15. R=9, L=4, Alpha=0.5
       //0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000,
         0xFFA9, 0x0216, 0xF761, 0x7B41, 0x0F74, 0xFAE1, 0x018C, 0xFFB9,
         0xFF64, 0x03AE, 0xF171, 0x7257, 0x20BD, 0xF5ED, 0x02FB, 0xFF79,
         0xFF36, 0x04B1, 0xEE28, 0x65D7, 0x32FC, 0xF1AB, 0x0423, 0xFF47,
         0xFF22, 0x0515, 0xED54, 0x5682, 0x453D, 0xEEA4, 0x04E0, 0xFF28,
         0xFF28, 0x04E0, 0xEEA4, 0x453D, 0x5682, 0xED54, 0x0515, 0xFF22,
         0xFF47, 0x0423, 0xF1AB, 0x32FC, 0x65D7, 0xEE28, 0x04B1, 0xFF36,
         0xFF79, 0x02FB, 0xF5ED, 0x20BD, 0x7257, 0xF171, 0x03AE, 0xFF64,
         0xFFB9, 0x018C, 0xFAE1, 0x0F74, 0x7B41, 0xF761, 0x0216, 0xFFA9,
       };
static const short HRM_Interpolator_Coefs_R4[]={//In Q15. R=4, L=4, Alpha=0.5
       //0x0000, 0x0000, 0x0000, 0x7FFF, 0x0000, 0x0000, 0x0000, 0x0000,
         0xFF56, 0x03FE, 0xF061, 0x6F86, 0x253F, 0xF4C6, 0x034D, 0xFF6B,
         0xFF22, 0x050D, 0xEDBE, 0x4E0F, 0x4E0F, 0xEDBE, 0x050D, 0xFF22,
         0xFF6B, 0x034D, 0xF4C6, 0x253F, 0x6F86, 0xF061, 0x03FE, 0xFF56,
       };


//static const int16_t NumOfLowestHRCyclesMin=375;		//3.75*60/45BPM=5.0s, Min(Initial) #(x100) of lowest-HR cycle. The longer the frame scale is, more accurate the heart rate is.
static const int16_t NumOfLowestHRCyclesMin=300;		//3.00*60/45BPM=4.0s, Min(Initial) #(x100) of lowest-HR cycle. The longer the frame scale is, more accurate the heart rate is.
static const int16_t NumOfLowestHRCyclesMax=500;		//5.00*60/45BPM=6.7s, Max(Final) #(x100) of lowest-HR cycle. The longer the frame scale is, more accurate the heart rate is.
static const int16_t f_Low=45;							//(bpm), min heart rate.
static const int16_t f_High=200;						//(bpm), max heart rate.
static const uint16_t HRM_PS_Raw_Min_Thresh=6000;		//HRM PS threshold for validation (this depends on the LED active current: 16000 for current=0xF(359mA))


#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  int16_t SpO2_Crest_Factor_Thresh=12;				//Crest factor (C^2) thresh
  uint16_t SpO2_DC_to_ACpp_Ratio_Low_Thresh=20;		
  uint16_t SpO2_DC_to_ACpp_Ratio_High_Thresh=400;		// 1/60=1.67%, 1/100=1%. DC to ACpp ratio threshold for validation. Normally, <250 for finger. <700 for wrist.
  #if SPO2_REFLECTIVE_MODE
    uint16_t SpO2_DC_Min_Thresh=4000;	//Red or IR PS threshold for validation.
  #else
    uint16_t SpO2_DC_Min_Thresh=9000;	//Red or IR PS threshold for validation.
  #endif
#endif


/****************************************************************************
*  Error Checking Macros
*****************************************************************************/
#ifndef checkErr
#define checkErr(fCall)      if (error = (fCall), (error = (error < 0) ? error : SI114xHRM_SUCCESS)) \
                                 {goto Error;}
#endif


/*  This function will start the Si114x autonomous measurement operation.
 * 	The device must be configured before calling this function.
 *
 */
int32_t si114xhrm_Run(Si114xhrmHandle_t _handle)
{	int error = SI114xHRM_SUCCESS;
    int16_t part_id = 0;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  uint16_t meas_rate_uv;


  if (handle->flag_samples != 1)		// if taking fresh samples
  {
    if (!si114xhrm_IdentifyPart(_handle, &part_id))
    {
      error = SI114xHRM_ERROR_INVALID_PART_ID;
      goto Error;
    }


    // Set up how often the device wakes up to make measurements
    // measurementRate, for example can be the following values:
    //    0xa0 = Device Wakes up every ~30 ms
    //    0x94 = Device Wakes up every ~20 ms
    //    0x84 = Device Wakes up every ~10 ms
    //    0xB9 = Device Wakes up every ~100 ms
    //    0xFF = Device Wakes up every ~2 sec
    si114xhrm_EnableInterrupts(_handle);
    if (handle->uv_part == 0)
      Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE, (uint8_t)handle->measurement_rate);
    else
    {
      meas_rate_uv = Uncompress (handle->measurement_rate);
      Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_MSB, (meas_rate_uv & 0xff00) >> 8);
      Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_LSB, meas_rate_uv & 0x00ff);
    }
  }

    //Init Si114x parameters based on measurementRate, and clear buffers and initialize variables
    si114xhrm_InitMeasurementParameters (handle, handle->measurement_rate); // This function calls si114xhrm_InitializeBuffers(_handle)

    if (handle->flag_samples != 1)		// if taking fresh samples
    // Enable Autonomous Operation
    Si114xPsAlsAuto(handle->si114x_handle);

Error:
  return error;
}


int32_t si114xhrm_Pause(Si114xhrmHandle_t _handle)
{	int error = SI114xHRM_SUCCESS;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  int16_t part_id = 0;
  if (!si114xhrm_IdentifyPart(_handle, &part_id))
  {
    error = SI114xHRM_ERROR_INVALID_PART_ID;
    goto Error;
  }

  //Set the measurement rate register(s) to 0 to allow for forced measurements
  if(handle->uv_part == 0)
    Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE, 0);
  else
  {	Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_MSB, 0);
    Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_LSB, 0);
  }

  // Pause Autonomous Operation
  Si114xPauseAll(handle->si114x_handle);

  si114xhrm_DisableInterrupts(_handle);

Error:
  return error;
}

static int si114xhrm_EnableInterrupts(Si114xhrmHandle_t _handle)
{	int error = SI114xHRM_SUCCESS;
  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  error = Si114xWriteToRegister(handle->si114x_handle, REG_IRQ_ENABLE,
           IE_ALS_EVRYSAMPLE +
           IE_PS1_EVRYSAMPLE +
           IE_PS2_EVRYSAMPLE +
           IE_PS3_EVRYSAMPLE );
  return error;
}
static int si114xhrm_DisableInterrupts(Si114xhrmHandle_t _handle)
{	int error = SI114xHRM_SUCCESS;
  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  error = Si114xWriteToRegister(handle->si114x_handle, REG_IRQ_ENABLE, 0);				//disable interrupts
  return error;
}

/* This function will perform a single forced measurement and determine whether or not
 * the sensor is contacting skin in such a way that we can try a HRM measurement
 *
 *
 * Autonomous Measurements must be disabled when this function is called
 *
 * Note:	A forced measurement will generate an interrupt and the IRQ handler
 * 			will execute resulting in the data in the irq queue.  This can be ignored
 * 			and will not cause a problem since the software will clear the queue prior
 * 			to restarting autonomous measurements
 */
int32_t si114xhrm_DetectSkinContact(Si114xhrmHandle_t _handle, int32_t *is_contact)
{	int error = SI114xHRM_SUCCESS;
  volatile uint8_t ps1_data0, ps1_data1, als_data0, als_data1;
  uint16_t ps1_data, als_data;
  uint8_t response;
  uint8_t saved_chlist;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  saved_chlist = Si114xParamRead(handle->si114x_handle, PARAM_CH_LIST);	//force ALS on
  Si114xParamSet(handle->si114x_handle, PARAM_CH_LIST, ALS_VIS_TASK);

  Si114xAlsForce(handle->si114x_handle);	//This function will not return until the measurement is complete
  //check for saturation after the forced measurement and clear it if found
  //otherwise the next si114x cmd will not be performed
  response = Si114xReadFromRegister(handle->si114x_handle, REG_RESPONSE);
  while((response & 0x80) != 0)
  { // Send the NOP Command to clear the error...we cannot use Si114xNop()
    // because it first checks if REG_RESPONSE < 0 and if so it does not
    // perform the cmd. Since we have a saturation REG_RESPONSE will be <0
    Si114xWriteToRegister(handle->si114x_handle, REG_COMMAND, 0x00);
    response = Si114xReadFromRegister(handle->si114x_handle, REG_RESPONSE);
  }

  als_data0 = Si114xReadFromRegister(handle->si114x_handle, REG_ALS_VIS_DATA0);
  als_data1 = Si114xReadFromRegister(handle->si114x_handle, REG_ALS_VIS_DATA1);

/*  This statement is broken up to avoid the warning "the order of volatile accessses is undefined
       als_data = (((uint16_t)(als_data1) << 8) & 0xff00) | als_data0;
*/
  als_data = ((uint16_t)(als_data1) << 8) & 0xff00;
  als_data |= als_data0;

  Si114xParamSet(handle->si114x_handle, PARAM_CH_LIST, saved_chlist);

  if (si11xhrm_IsAmbientBlocked(als_data) == 1)  //finger on will block ambient
  {
    Si114xPsForce (handle->si114x_handle);	//This function will not return until the measurement is complete

    //check for saturation after the forced measurement and clear it if found
    //otherwise the next si114x cmd will not be performed
    response = Si114xReadFromRegister(handle->si114x_handle, REG_RESPONSE);
    while((response & 0x80) != 0)
      { // Send the NOP Command to clear the error...we cannot use Si114xNop()
      // because it first checks if REG_RESPONSE < 0 and if so it does not
      // perform the cmd. Since we have a saturation REG_RESPONSE will be <0
        Si114xWriteToRegister(handle->si114x_handle, REG_COMMAND, 0x00);
        response = Si114xReadFromRegister(handle->si114x_handle, REG_RESPONSE);
      }
    ps1_data0 = Si114xReadFromRegister(handle->si114x_handle, REG_PS1_DATA0 + (handle->hrm_ps_select << 1));
    ps1_data1 = Si114xReadFromRegister(handle->si114x_handle, REG_PS1_DATA1 + (handle->hrm_ps_select << 1));
 
 /*  This statement is broken up to avoid the warning "the order of volatile accessses is undefined
         ps1_data = (((uint16_t)(ps1_data1) << 8) & 0xff00) | ps1_data0;
*/
    ps1_data = ((uint16_t)(ps1_data1) << 8) & 0xff00;
    ps1_data |= ps1_data0;
  }
  else
    ps1_data = 0;			//if ALS is too high then set ps1_data=0 to make skin detection fail

    if(ps1_data > HRM_PS_RAW_SKIN_CONTACT_THRESHOLD)
      *is_contact = 1;
    else
      *is_contact = 0;

    return error;

}


// if a finger is on the sensor it blocks the ambient light resulting in an
// als_vis reading of < HRM_AMBIENT_BLOCKED_THRESHOLD.
static int32_t si11xhrm_IsAmbientBlocked(uint16_t als_vis)
{
  if (als_vis > HRM_AMBIENT_BLOCKED_THRESHOLD)
    return 0;
  else
    return 1;
}

int32_t si114xhrm_Configure(Si114xhrmHandle_t _handle, Si114xhrmConfiguration_t *configuration)
{	int32_t error = SI114xHRM_SUCCESS;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  int16_t current[3];
  int16_t adcMux[3];
  int16_t part_id = 0;

  if (_handle == NULL)
  {   error = SI114xHRM_ERROR_BAD_POINTER;
      goto Error;
  }

  if (!si114xhrm_IdentifyPart(_handle, &part_id))
  {
    error = SI114xHRM_ERROR_INVALID_PART_ID;
    goto Error;
  }

  current[0] = configuration->ledCurrent & 0xF;
  current[1] = (configuration->ledCurrent>>4) & 0xF;
  current[2] = (configuration->ledCurrent>>8) & 0xF;
  adcMux[0] = (configuration->adcMux>>0) & 0xF;
  adcMux[1] = (configuration->adcMux>>4) & 0xF;
  adcMux[2] = (configuration->adcMux>>8) & 0xF;
  handle->timestampClockFreq = (configuration->timestampClockFreq);
  error = si114xhrm_ConfigureDiscretes(_handle, configuration->hrmSpo2PsSelect, current, configuration->tasklist, configuration->psLedSelect, configuration->measurementRate, configuration->adcGain, configuration->adcMisc, adcMux, configuration->psAlign);

Error:
    return error;
}

int32_t si114xhrm_ConfigureDiscretes(Si114xhrmHandle_t _handle, int16_t hrmSpo2PsSelect, int16_t *current, int16_t tasklist, int16_t psLedSelect, int16_t measurementRate, int16_t adcGain, int16_t adcMisc, int16_t *adcMux, int16_t psAlign)
{	int error = SI114xHRM_SUCCESS;
  int16_t  retval=0;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;
  int16_t part_id = 0;
  if (!si114xhrm_IdentifyPart(_handle, &part_id))
  {
    error = SI114xHRM_ERROR_INVALID_PART_ID;
    goto Error;
  }

  //ToDo:  Add parameter range checking.

  handle->hrm_ps_select = (hrmSpo2PsSelect>>0)&0xF;
  handle->spo2_ir_ps_select = (hrmSpo2PsSelect>>4)&0xF;
  handle->spo2_red_ps_select = (hrmSpo2PsSelect>>8)&0xF;
  handle->Si114x_adcgain_current = (adcGain << 12) + (current[2] << 8) + (current[1] << 4) + current[0];	//Save for auto PS AGC
  handle->Si114x_adcgain_current0 = handle->Si114x_adcgain_current;									//Init ADCgain
  handle->measurement_rate = measurementRate;		// store this for use in the si114xhrm_Run function

  if (handle->flag_samples != 1)
  {
    // Note that the Si114xReset() actually performs the following functions:
    //     1. Pauses all prior measurements
    //     2. Clear  i2c registers that need to be cleared
    //     3. Clears irq status to make sure INT* is negated
    //     4. Delays 10 ms
    //     5. Sends HW Key
    retval += Si114xReset(handle->si114x_handle);

    // Get the LED current passed from the caller
    // If the value passed is negative, just pick a current... 202 mA
    {
      uint8_t i21, i3;
      if (current == 0)	//current is a pointer so we check for a null pointer
      {
        i21 = 0xbb; // current = LEDI_202;
        i3 = 0x0b;
      }
      else
      {
        i21 = (current[1] << 4) + current[0];
        i3 = current[2];
      }
      retval += Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED21, i21);
      retval += Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED3, i3);
    }

    // Initialize CHLIST Parameter from caller to enable measurement
    // Valid Tasks are: ALS_VIS_TASK, ALS_IR_TASK, PS1_TASK
    //                  PS2_TASK, PS3_TASK and AUX_TASK
    // However, if we are passed a 'negative' task, we will
    // turn on ALS_IR, ALS_VIS and PS1. Otherwise, we will use the
    // task list specified by the caller.
    if (tasklist < 0)
      tasklist = ALS_IR_TASK + ALS_VIS_TASK + PS1_TASK + PS2_TASK + PS3_TASK; //Addedd PS2 and PS3
    retval += Si114xParamSet(handle->si114x_handle, PARAM_CH_LIST, ALS_VIS_TASK | tasklist);  //Force ALS on

    if (psLedSelect < 0)
      psLedSelect = 0x0421;
    retval += Si114xParamSet(handle->si114x_handle, PARAM_PSLED12_SELECT, psLedSelect & 0x77);
    retval += Si114xParamSet(handle->si114x_handle, PARAM_PSLED3_SELECT, (psLedSelect >> 8) & 0x7);

    // Set IRQ Modes and INT CFG to interrupt on every sample
    //Note: Interrupts are setup here but not enabled until _Run().
    retval += Si114xWriteToRegister(handle->si114x_handle, REG_INT_CFG, ICG_INTOE);

    retval += Si114xWriteToRegister(handle->si114x_handle, REG_IRQ_MODE1,
      IM1_ALS_EVRYSAMPLE +
      IM1_PS1_EVRYSAMPLE +
      IM1_PS2_EVRYSAMPLE);

    retval += Si114xWriteToRegister(handle->si114x_handle, REG_IRQ_MODE2,
      IM2_PS3_EVRYSAMPLE);

    retval += Si114xParamSet(handle->si114x_handle, PARAM_PS_ADC_GAIN, adcGain);
    retval += Si114xParamSet(handle->si114x_handle, PARAM_PS_ADC_MISC, adcMisc);
    retval += Si114xParamSet(handle->si114x_handle, PARAM_PS_ENCODING, psAlign);//Default: PS1_ALIGN=PS2_ALIGN=PS3_ALIGN=0
    retval += Si114xParamSet(handle->si114x_handle, PARAM_PS1_ADC_MUX, adcMux[0]);
    retval += Si114xParamSet(handle->si114x_handle, PARAM_PS2_ADC_MUX, adcMux[1]);
    retval += Si114xParamSet(handle->si114x_handle, PARAM_PS3_ADC_MUX, adcMux[2]);

    // Configure the ALS VIS range bit
    retval += Si114xParamSet(handle->si114x_handle, PARAM_ALSVIS_ADC_MISC, 0x0);		//This should be zero for finger detect to work
    //ToDo: In the future support VIS_RANGE = 1 so that we can do HRM and UV at the same time.

    // Configure the ALS IR channel for the same settings as PS
    retval += Si114xParamSet(handle->si114x_handle, PARAM_ALSIR_ADC_GAIN, adcGain);
    retval += Si114xParamSet(handle->si114x_handle, PARAM_ALSIR_ADC_MISC, adcMisc & 0x20);
    retval += Si114xParamSet(handle->si114x_handle, PARAM_IR_ADC_MUX, adcMux[0]);

    /*	Initially set the measurement rate register to 0 to enable forced measurements.  The specified
    * 	measurement rate is set in the function si114xhrm_Run just before autonomous measurements are
    * 	started.
    */

    if (handle->uv_part == 0)
    {
      retval += Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE, 0);

      // if 0x08, VIS, IR, AUX Measurements every time device wakes up.
      retval += Si114xWriteToRegister(handle->si114x_handle, REG_ALS_RATE, 0x08);
    }
    else
    {
      retval += Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_MSB, 0);
      retval += Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_LSB, 0);
    }

    // if 0x08, PS1, PS2 and PS3 made every time device wakes up.
    retval += Si114xWriteToRegister(handle->si114x_handle, REG_PS_RATE, 0x08);

    // If nothing went wrong after all of this time, the value
    // returned will be 0. Otherwise, it will be some negative
    // number
  }

  checkErr(si114xhrm_InitMeasurementParameters (_handle, measurementRate));

Error:
  return retval;
}


static int si114xhrm_InitializeBuffers(Si114xhrmHandle_t _handle)
{	int i, j;
  int error = SI114xHRM_SUCCESS;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;



  //Buffer inits
  for(i=0; i<MAX_FRAME_SAMPLES; i++)
  {
    handle->HRM_sample_buffer[i]=0;

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
    if (handle->spo2 != NULL)
    {
     handle->spo2->SpO2_RED_AC_sample_buffer[i]=0;
     handle->spo2->SpO2_RED_DC_sample_buffer[i]=0;
     handle->spo2->SpO2_IR_AC_sample_buffer[i] =0;
     handle->spo2->SpO2_IR_DC_sample_buffer[i] =0;
    }
#endif
  }

  for(i=0; i<handle->BPF_biquads; i++) //zero-out the input and output buffer
  {
    for(j=0; j<3; j++)
    {
      handle->HRM_BPF.x[i][j] =0;
      handle->HRM_BPF.yi[i][j]=0;
      handle->HRM_BPF.yf[i][j]=0;
#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
      if (handle->spo2 != NULL) //SpO2 BPF buffer inits
      {
       handle->spo2->SpO2_Red_BPF.x[i][j] =0;
       handle->spo2->SpO2_Red_BPF.yi[i][j]=0;
       handle->spo2->SpO2_Red_BPF.yf[i][j]=0;

       handle->spo2->SpO2_IR_BPF.x[i][j] =0;
       handle->spo2->SpO2_IR_BPF.yi[i][j]=0;
       handle->spo2->SpO2_IR_BPF.yf[i][j]=0;
          }
#endif
    }
  }

  for (i=0; i<INTERPOLATOR_L*2; i++) handle->HRM_interpolator_buf[i]=0; //Initialized the whole HRM interpolaor buffer to 0.

  #if	HRM_DATA_LOG //Create and open the data log file
    pFile_TS_PS_output = fopen("output.txt","w");
  #endif	//HRM_DATA_LOG

  handle->sample_count = 0;
  handle->HRM_PS_raw_level_count=0;
  handle->HRM_Buffer_In_Index=0;
  handle->HRM_PS_AGC_Flag = HRM_PS_AGC_ENABLED; //HRM_PS_AGC_DISABLED, HRM_PS_AGC_ENABLED
  handle->HRM_current_mask = 0xf<<(handle->hrm_ps_select*4);

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  if (handle->spo2 != NULL) //Variable Inits
  {
    handle->HRM_current_mask |= ((0xf<<(handle->spo2_red_ps_select*4)) | (0xf<<(handle->spo2_ir_ps_select*4))); //Add more mask bits for SpO2.
    for (i=0; i<INTERPOLATOR_L*2; i++)
    {
      handle->spo2->SpO2_Red_interpolator_buf[i]=0; //Initialized the whole SpO2 Red interpolaor buffer to 0.
      handle->spo2->SpO2_IR_interpolator_buf[i]=0; //Initialized the whole SpO2 Red interpolaor buffer to 0.
    }
    handle->spo2->SpO2_Buffer_In_index=0;
    handle->spo2->SpO2_raw_level_count=0;
    handle->spo2->SpO2_Red_BPF_Ripple_Count=0;
    handle->spo2->SpO2_Red_PS_Input_old=0;
    handle->spo2->SpO2_IR_BPF_Ripple_Count=0;
    handle->spo2->SpO2_IR_PS_Input_old=0;
    handle->spo2->TimeSinceFingerOff=0;
  }
#endif

  handle->TimeStamp_Old=0;
  handle->dTS_count=0;
  handle->HRM_DC_Sensing_Count=0;
  handle->HRM_Raw_PS_old=0;
  handle->HRM_DC_Sensing_Flag=HRM_DC_SENSING_START;
  handle->PS_AGC_count=0;
  handle->meas_rate_DC_Sensing=0;
  handle->Accumlated_dTS_count=0;
  handle->Accumlated_dTS=0;
  handle->BPF_Active_Flag=(0xff-HRM_BPF_ACTIVE-SpO2_RED_BPF_ACTIVE-SpO2_IR_BPF_ACTIVE);	//BPF-ripple-reduction flags. Clear these bits.
  handle->HRM_BPF_Ripple_Count=0;
  handle->HRM_PS_Input_old=0;
  handle->HeartRateInvalidation_Previous=SI114xHRM_STATUS_FINGER_OFF;
  handle->HRM_PS_DC=0;

  if (handle->flag_samples == 0)
    Si114xIrqQueue_Clear(handle->si114x_handle);

  return error;
}


static int si114xhrm_InitMeasurementParameters (Si114xhrmHandle_t _handle, int16_t measurement_rate)
{
  int error = SI114xHRM_SUCCESS;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  handle->HRM_Interpolator_Factor=1;
  if (measurement_rate>=0xb4 && measurement_rate<=0xbc ) //Fs=9~12Hz. Interpolate by a factor of 9 to Fs=81~108Hz
  {
    handle->HRM_Interpolator_Factor=9;
    handle->pHRM_Interpolator_Coefs=(int16_t *)HRM_Interpolator_Coefs_R9;
  }
  else if (measurement_rate >=0xa4 && measurement_rate <=0xa8) //Fs=21~25Hz. Interpolate by a factor of 4 to Fs=84~100Hz
  {
    handle->HRM_Interpolator_Factor=4;
    handle->pHRM_Interpolator_Coefs=(int16_t *)HRM_Interpolator_Coefs_R4;
  }

  //Set Fs and the BPF coefs per measurementRate
  switch(measurement_rate)
  {
#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_60Hz == 1)
    case 0xa0: //Fs=30Hz
    case 0x91: //Fs=57Hz
    case 0x90:
    case 0x8E:
      handle->Fs=600;				//(Hz*10)
      handle->BPF_biquads=sizeof(BPF_60Hz)/6/4;
      handle->pBPF_b_0=BPF_60Hz;
      handle->BPF_output_scaler=BPF_output_scaler_60Hz;
      break;
#endif
#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_95Hz == 1)
    case 0xbc: //Fs=09Hz
    case 0xb8: //Fs=10Hz
    case 0xb4: //Fs=12Hz
    case 0xb0: //Fs=16Hz
    case 0xa8: //Fs=21Hz
    case 0xa5: //Fs=24Hz
    case 0xa4: //Fs=25Hz
    case 0xa2: //Fs=27Hz
    case 0x84:
    //case 0x140:	//uv_part only
      handle->Fs=950;				//(Hz*10)
      handle->BPF_biquads=sizeof(BPF_95Hz)/6/4;
      handle->pBPF_b_0=BPF_95Hz;
      handle->BPF_output_scaler=BPF_output_scaler_95Hz;
      break;
#endif
#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_185Hz == 1)
    case 0x74:
      handle->Fs=1850;			//(Hz*10)
      handle->BPF_biquads=sizeof(BPF_185Hz)/6/4;
      handle->pBPF_b_0=BPF_185Hz;
      handle->BPF_output_scaler=BPF_output_scaler_185Hz;
      break;
#endif
#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_229Hz == 1)
    case 0x70:
      handle->Fs=2290;			//(Hz*10)
      handle->BPF_biquads=sizeof(BPF_229Hz)/6/4;
      handle->pBPF_b_0=BPF_229Hz;
      handle->BPF_output_scaler=BPF_output_scaler_229Hz;
      break;
#endif

#if (SI114xHRM_ENABLE_MEASUREMENT_RATE_430Hz == 1)
    case 0x60:
      handle->Fs=4300;			//(Hz*10)
      handle->BPF_biquads=sizeof(BPF_430Hz)/6/4;
      handle->pBPF_b_0=BPF_430Hz;
      handle->BPF_output_scaler=BPF_output_scaler_430Hz;
      break;
#endif
    default:
      error = SI114xHRM_ERROR_INVALID_MEASUREMENT_RATE;
      goto Error;
//break not needed because of goto statement above      break;
  }

  handle->hrUpdateInterval=1*(handle->Fs+5)/10;	//(sample) in 1s

  handle->T_Low0 =60*(handle->Fs+5)/10/f_Low -1 ;		//(sample), -1 for the auto-corr max validation.
  handle->T_High0=60*(handle->Fs+5)/10/f_High+1;		//(sample), +1 for the auto-corr max validation.
  //handle->hrIframe=handle->T_Low0*NumOfLowestHRCycles0/100; //(sample)

  si114xhrm_InitializeBuffers(_handle);

  //Normalize HRM PS to around 20000 for the HRM algorithm
  handle->HRM_Normalization_Scaler =(1<<QF_SCALER);	//Scaler=1.0 in Qf
  #if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
    handle->RED_Normalization_Scaler = (1<<QF_SCALER);	//Scaler=1.0 in Qf
    handle->IR_Normalization_Scaler  = (1<<QF_SCALER);	//Scaler=1.0 in Qf
  #endif

  //Seems we can use same values for 3 cases.
  handle->HRM_PS_Vpp_max = HRM_PS_VPP_MAX_GREEN_FINGERTIP;
  handle->HRM_PS_CrestFactor_Thresh=HRM_CREST_FACTOR_THRESH_FINGERTIP;
  handle->HRM_PS_Vpp_min = handle->HRM_PS_Vpp_max * (-1);

Error:
  return error;
}

int32_t si114xhrm_ProcessExternalSample(Si114xhrmHandle_t _handle, int16_t *heart_rate, int16_t *SpO2, int32_t *hrm_status, Si114xhrmData_t *hrm_data, SI114X_IRQ_SAMPLE *samples)
{	int error = SI114xHRM_SUCCESS;
  uint16_t *ps_ptr[3] ;  //array of pointers used to select the ps value used for the measurements

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;
  ps_ptr[0] = &samples->ps1;
  ps_ptr[1] = &samples->ps2;
  ps_ptr[2] = &samples->ps3;
  checkErr(si114xhrm_SampleProcess(_handle, (uint16_t)samples->timestamp, *ps_ptr[handle->hrm_ps_select], *ps_ptr[handle->spo2_red_ps_select], *ps_ptr[handle->spo2_ir_ps_select], hrm_data));

  checkErr(si114xhrm_FrameProcess(_handle, heart_rate, hrm_status, hrm_data));

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  if (handle->spo2 != NULL)//Call SpO2 Frame process
  {  checkErr(si114xhrm_SpO2FrameProcess(_handle, SpO2, hrm_status, hrm_data));
  }
#endif

Error:
  return error;
}

int32_t si114xhrm_Process(Si114xhrmHandle_t _handle, int16_t *heart_rate, int16_t *SpO2, int32_t *hrm_status, Si114xhrmData_t *hrm_data)
{	int error = SI114xHRM_SUCCESS;
  SI114X_IRQ_SAMPLE samples;

  checkErr(si114xhrm_GetSample(_handle, &samples));
  error = si114xhrm_ProcessExternalSample(_handle, heart_rate, SpO2, hrm_status,hrm_data, &samples);

  if (si11xhrm_IsAmbientBlocked((uint16_t)samples.vis) == 0)
  {	*hrm_status |= SI114xHRM_STATUS_FINGER_OFF | SI114xHRM_STATUS_SPO2_FINGER_OFF;

  }
Error:
  return error;
}

/*  revision char buffer must be at least 32 characters  */
int32_t si114xhrm_QuerySoftwareRevision(int8_t *revision)
{
  strcpy((char *)revision, si114xHRM_VERSION);

  return SI114xHRM_SUCCESS;
}

int32_t si114xhrm_IsUvDevice(Si114xhrmHandle_t _handle, int32_t *is_uv_device)
{	int error = SI114xHRM_SUCCESS;
    int16_t part_id = 0;

    if (!si114xhrm_IdentifyPart(_handle, &part_id))
  {
    error = SI114xHRM_ERROR_INVALID_PART_ID;
    goto Error;
  }

  switch ( part_id )
    {
    case 0x41:
        case 0x42:
        case 0x43:
      *is_uv_device = 0;
      break;

        case 0x32:
        case 0x44:
    case 0x45:
        case 0x46:
        case 0x47:
      *is_uv_device = 1;
      break;

    default:
      error = SI114xHRM_ERROR_INVALID_PART_ID;
      break;
    }

  Error:
  return error;
}


static int si114xhrm_GetSample(Si114xhrmHandle_t _handle, SI114X_IRQ_SAMPLE *samples)
{	int error = SI114xHRM_SUCCESS;

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;


  if(Si114xIrqQueueNumentries(handle->si114x_handle)>0)
        {	Si114xIrqQueue_Get(handle->si114x_handle, samples);
        }
    else
      error = SI114xHRM_ERROR_SAMPLE_QUEUE_EMPTY;

    return error;
}



static int si114xhrm_SampleProcess(Si114xhrmHandle_t _handle, uint16_t HRM_timestamp, uint16_t hrmPs, uint16_t SpO2_PS_RED, uint16_t SpO2_PS_IR, Si114xhrmData_t *hrm_data)
{
  int error = SI114xHRM_SUCCESS;
  uint16_t TimeStamp;
  uint16_t PS_Local;
  uint16_t Raw_PS_Local;
  uint16_t i, j, iInterpolator;
#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  uint16_t SpO2_PS_RED_Scaled, SpO2_PS_IR_Scaled;
#endif  
  int Interpolator_PS;
    uint16_t meas_rate_uv;
  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;


  TimeStamp=HRM_timestamp;	//Time stamp From Si114x EVB or the captured file
  PS_Local=hrmPs;					//From Si114x EVB or the captured file
  Raw_PS_Local=hrmPs;				//Use for display purpose

  //1) Determine the sampling frequency from the timestamp (When measrate or ADC gain changes, the first 10 timestamps are old in the debug mode)
  //Accumulate the time-stamp difference for a period of time and then estimate Fs.
  if (handle->dTS_count<FS_ESTIMATE_DURATION)
  {
    handle->dTS_count++;
    if ( TimeStamp>handle->TimeStamp_Old && handle->dTS_count>2) //Don't use TimeStamp that is wrapped at 2^16. Don't use the first 2 TimeStamp in case these may be bad.
    {
      handle->Accumlated_dTS +=TimeStamp-handle->TimeStamp_Old;
      handle->Accumlated_dTS_count++;
    }
    if (handle->Accumlated_dTS_count>=4)
    {
		//	handle->Fs=(8192*handle->Accumlated_dTS_count)/(handle->Accumlated_dTS/10); //(Hz*10)
			handle->Fs=(handle->timestampClockFreq*handle->Accumlated_dTS_count)/(handle->Accumlated_dTS/10); //(Hz*10)
			handle->Fs *=handle->HRM_Interpolator_Factor;
      //After real-time FS is estimated, re-initialize the parameter
      handle->hrUpdateInterval=1*(handle->Fs+5)/10;	//(sample) in 1s
      handle->T_Low0 =60*(handle->Fs+5)/10/f_Low -1 ;	//(sample), -1 for the auto-corr max validation.
      handle->T_High0=60*(handle->Fs+5)/10/f_High+1;	//(sample), +1 for the auto-corr max validation.
      handle->NumOfLowestHRCycles=NumOfLowestHRCyclesMin;
      handle->hrIframe=handle->T_Low0*handle->NumOfLowestHRCycles/100; //(sample)
      handle->dTS_count=FS_ESTIMATE_DURATION;			//Stop the initial FS estimate code after Fs is estimated
    }
  }

  //Update Fs in the real-time basis. (This is needed when Fs changes with temperation)
  if (handle->dTS_count==FS_ESTIMATE_DURATION && TimeStamp > handle->TimeStamp_Old)  //Exclude the time stamp that wraps around at 2^16. Update Fs.
  {
    if ((handle->HRM_Interpolator_Factor==1) && (handle->HRM_DC_Sensing_Flag!=HRM_DC_SENSING_CHANGE_PARAMETERS && handle->HRM_DC_Sensing_Flag!=HRM_DC_SENSING_SENSE_FINISH && handle->HRM_DC_Sensing_Flag!=HRM_DC_SENSING_RESTORE_FS))
    {//Only update Fs when the interpolator isn't used or the DC sensing isn't running.
    //	handle->Fs=(handle->Fs*(100-FS_UPDATE_RATE)+1000*10*10/(TimeStamp-handle->TimeStamp_Old)*FS_UPDATE_RATE)/100; //(Hz*10)
    	handle->Fs=(handle->Fs*(100-FS_UPDATE_RATE)+handle->timestampClockFreq*10/(TimeStamp-handle->TimeStamp_Old)*FS_UPDATE_RATE)/100; //(Hz*10)
    }
  }
  handle->TimeStamp_Old=TimeStamp; //Update the old time stamp for the next sample.

#if HRM_PS_AGC //Change the HRM normalized scaler, so that the HRM BPF will not see the disturbance due to LED current change.
  //Si114x PS has some delay (up to 1 sample for LED current change, up to 39 samples for ADCgain change) to response to the LED current or ADCgain change, so the
  //   code looks for the change in a N-sample delay window.
  #define HRM_PS_AGC_DELAY_WINDOW			50 //(samples)
  if ((handle->HRM_PS_AGC_Flag&HRM_PS_AGC_GAIN_CHANGED)==HRM_PS_AGC_GAIN_CHANGED && handle->PS_AGC_count<HRM_PS_AGC_DELAY_WINDOW)
  {
    handle->PS_AGC_count++;
    i = handle->hrmRawPs > Raw_PS_Local ? (handle->hrmRawPs - Raw_PS_Local) : (Raw_PS_Local - handle->hrmRawPs); //i=abs(hrm_data->hrmRawPs - Raw_PS_Local)
    if ( i>(handle->hrmRawPs/20) || handle->PS_AGC_count==HRM_PS_AGC_DELAY_WINDOW) //change the scaler if difference is 5% or handle->PS_AGC_count==20
    {
      handle->HRM_Normalization_Scaler = handle->HRM_Normalization_Scaler * handle->hrmRawPs / Raw_PS_Local; //Modify the normalization scaler based on the current and previous samples.

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
      if (handle->spo2 != NULL)//SpO2 normalized scaler change when HRM PS AGC change in case that HRM shares a LED with SpO2.
      {
        if (handle->hrm_ps_select==handle->spo2_red_ps_select)
          handle->RED_Normalization_Scaler=handle->HRM_Normalization_Scaler;
        else if (handle->hrm_ps_select==handle->spo2_ir_ps_select)
          handle->IR_Normalization_Scaler=handle->HRM_Normalization_Scaler;
          //printf("SpO2 : Red_PS=%d, IR_PS=%d, Red_Scaler=%d, IR_Scaler=%d\n", SpO2_PS_RED, SpO2_PS_IR, handle->RED_Normalization_Scaler, handle->IR_Normalization_Scaler);
      }
#endif

      handle->HRM_PS_AGC_Flag ^= HRM_PS_AGC_GAIN_CHANGED; //Clear the bit HRM_PS_AGC_GAIN_CHANGED
      handle->PS_AGC_count=0;
      for (i=0; i<(INTERPOLATOR_L*2); i++) handle->HRM_interpolator_buf[i] = handle->HRM_interpolator_buf[i]*Raw_PS_Local /handle->hrmRawPs; //Scale the HRM interpolator data buffer

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
      if (handle->spo2 != NULL)//Scale the SpO2 interpolator buffer when HRM PS AGC change in case that HRM shares a LED with SpO2.
      {
        if (handle->hrm_ps_select==handle->spo2_red_ps_select)
        {
          for (i=0; i<(INTERPOLATOR_L*2); i++) handle->spo2->SpO2_Red_interpolator_buf[i] = handle->spo2->SpO2_Red_interpolator_buf[i]*Raw_PS_Local /handle->hrmRawPs;	//Scale the SpO2 Red interpolator data buffer
        }
        else if (handle->hrm_ps_select==handle->spo2_ir_ps_select)
        {
          for (i=0; i<(INTERPOLATOR_L*2); i++) handle->spo2->SpO2_IR_interpolator_buf[i]  = handle->spo2->SpO2_IR_interpolator_buf[i]*Raw_PS_Local /handle->hrmRawPs;	//Scale the SpO2 IR interpolator data buffer
        }
      }
#endif
      
    }
  }
#endif
  handle->hrmRawPs=Raw_PS_Local;    //Save the raw PS for next sample or frame process.

  //Interpolator causes a delay of INTERPOLATOR_L-1 samples due to the its filter.
  for (i=0; i<(INTERPOLATOR_L*2-1); i++) handle->HRM_interpolator_buf[i]=handle->HRM_interpolator_buf[i+1];  //ToDO:  use memmove() instead for efficiency
  handle->HRM_interpolator_buf[i]=PS_Local; //Add the new HRM sample to the interpolator buffer

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  if (handle->spo2 != NULL) //SpO2 interpolator buffer shift and add new samples.
  {
    for (i=0; i<(INTERPOLATOR_L*2-1); i++) //Shift the buffer
    {
      handle->spo2->SpO2_Red_interpolator_buf[i]=handle->spo2->SpO2_Red_interpolator_buf[i+1];		//ToDO:  use memmove() instead for efficiency
      handle->spo2->SpO2_IR_interpolator_buf[i] =handle->spo2->SpO2_IR_interpolator_buf[i+1];			//ToDO:  use memmove() instead for efficiency
    }
    handle->spo2->SpO2_Red_interpolator_buf[i]=SpO2_PS_RED; //Add the new SpO2 Red sample to the interpolator buffer
    handle->spo2->SpO2_IR_interpolator_buf[i] =SpO2_PS_IR;  //Add the new SpO2 IR sample to the interpolator buffer
  }
#endif
  
  for (iInterpolator=0; iInterpolator<handle->HRM_Interpolator_Factor; iInterpolator++)
  {
  if (iInterpolator==0)
  {
    if (handle->HRM_Interpolator_Factor>1) //Don't touch hrmPs and SpO2_PS_RED/IR if handle->HRM_Interpolator_Factor==1;
    {
      PS_Local=handle->HRM_interpolator_buf[INTERPOLATOR_L-1];

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
      if (handle->spo2 != NULL) //SpO2 interpolating
      {
        SpO2_PS_RED=handle->spo2->SpO2_Red_interpolator_buf[INTERPOLATOR_L-1];
        SpO2_PS_IR =handle->spo2->SpO2_IR_interpolator_buf[INTERPOLATOR_L-1];
      }
#endif
    }
  }
  else
  {
    Interpolator_PS=0;
    for (j=0; j<INTERPOLATOR_L*2; j++)
      Interpolator_PS += handle->HRM_interpolator_buf[j]*(handle->pHRM_Interpolator_Coefs[(iInterpolator-1)*INTERPOLATOR_L*2+j]); //-1 because of no 1st interpolator
    PS_Local= Interpolator_PS<0 ? 0: (Interpolator_PS>>15);	// Interpolator_Coefs are in Q15

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
    if (handle->spo2 != NULL)//SpO2 interpolating
    {
      Interpolator_PS=0;
      for (j=0; j<INTERPOLATOR_L*2; j++)
        Interpolator_PS += handle->spo2->SpO2_Red_interpolator_buf[j]*(handle->pHRM_Interpolator_Coefs[(iInterpolator-1)*INTERPOLATOR_L*2+j]); //-1 because of no 1st interpolator
      SpO2_PS_RED= Interpolator_PS<0 ? 0: (Interpolator_PS>>15);	// Interpolator_Coefs are in Q15
      Interpolator_PS=0;
      for (j=0; j<INTERPOLATOR_L*2; j++)
        Interpolator_PS += handle->spo2->SpO2_IR_interpolator_buf[j] *(handle->pHRM_Interpolator_Coefs[(iInterpolator-1)*INTERPOLATOR_L*2+j]); //-1 because of no 1st interpolator
      SpO2_PS_IR = Interpolator_PS<0 ? 0: (Interpolator_PS>>15);	// Interpolator_Coefs are in Q15
    }
#endif    
  }

  Raw_PS_Local=PS_Local; //Raw_PS_Local after interpolator if handle->HRM_Interpolator_Factor>1.

    //0) Normalize PS
  PS_Local=(uint16_t)(((int)PS_Local*(int)handle->HRM_Normalization_Scaler)>>QF_SCALER);	//HRM_Normalization_Scaler in Q8

  si114xhrm_BPF_filtering(_handle, (int)PS_Local, &handle->HRM_sample_buffer[handle->HRM_Buffer_In_Index], &handle->HRM_BPF); //Add the BPF(PS) in the framed sample circular buffer.

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  if (handle->spo2 != NULL) //SpO2 BPF process and compute Red_DC, Red_AC, IR_DC and IR_AC
  {
    //Normalize SpO2 PSs, removing the DC floor of 256
    SpO2_PS_RED_Scaled=(uint16_t)(((int)(SpO2_PS_RED-256)*(int)handle->RED_Normalization_Scaler)>>QF_SCALER);	//RED_Normalization_Scaler in Q8
    SpO2_PS_IR_Scaled=(uint16_t)(((int)(SpO2_PS_IR-256)*(int)handle->IR_Normalization_Scaler)>>QF_SCALER);	//IR_Normalization_Scaler in Q8

    si114xhrm_BPF_filtering(_handle, (int)SpO2_PS_RED_Scaled, &handle->spo2->SpO2_RED_AC_sample_buffer[handle->spo2->SpO2_Buffer_In_index], &handle->spo2->SpO2_Red_BPF); //Add the BPF(PS2) in the framed sample circular buffer.
    si114xhrm_BPF_filtering(_handle, (int)SpO2_PS_IR_Scaled,  &handle->spo2->SpO2_IR_AC_sample_buffer[handle->spo2->SpO2_Buffer_In_index],  &handle->spo2->SpO2_IR_BPF); //Add the BPF(PS3) in the framed sample circular buffer.
    handle->spo2->SpO2_RED_DC_sample_buffer[handle->spo2->SpO2_Buffer_In_index]=(uint16_t)SpO2_PS_RED_Scaled;
    handle->spo2->SpO2_IR_DC_sample_buffer[handle->spo2->SpO2_Buffer_In_index] =(uint16_t)SpO2_PS_IR_Scaled;
    if (++handle->spo2->SpO2_Buffer_In_index==MAX_FRAME_SAMPLES) handle->spo2->SpO2_Buffer_In_index=0; //Wrap around
  } 
#endif   //SI114XHRM_BUILD_GECKO_SPO2 

#if	HRM_DATA_LOG //Dump out the HRM raw PS and normalized PS, BPF(normalized PS)
  fprintf(pFile_TS_PS_output, "%d\t%d", Raw_PS_Local, PS_Local); //Dump out raw PS and the normalized PS
  //fprintf(pFile_TS_PS_output, "%d\t%d\n", TimeStamp, PS_Local); //Receive the timestamp(in 0.1ms) and PS
  //fprintf(pFile_TS_PS_output, "%d\t%d", Raw_PS_Local, handle->HRM_sample_buffer[handle->HRM_Buffer_In_Index]); //Dump out HRM raw PS and BPF(PS)

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
  if (handle->spo2 != NULL)//Log out SpO2 data
  {
    if (--handle->spo2->SpO2_Buffer_In_index<0) handle->spo2->SpO2_Buffer_In_index +=MAX_FRAME_SAMPLES; //Wrap around
    //fprintf(pFile_TS_PS_output, "%d\t%d", SpO2_PS_RED_Scaled, SpO2_PS_IR_Scaled); //Dump out SpO2 Red & IR PS.
    //fprintf(pFile_TS_PS_output, "%d\t%d", PS_Local, SpO2_PS_RED_Scaled); //Dump out HRM raw PS and SpO2_PS_RED_Scaled
    //fprintf(pFile_TS_PS_output, "%d\t%d", SpO2_PS_RED_Scaled, handle->SpO2_RED_AC_sample_buffer[handle->SpO2_Buffer_In_index]); //Dump out SpO2 Red DC & AC.
    //fprintf(pFile_TS_PS_output, "%d\t%d", handle->SpO2_RED_AC_sample_buffer[handle->SpO2_Buffer_In_index], handle->SpO2_IR_AC_sample_buffer[handle->SpO2_Buffer_In_index]); //Dump out SpO2 Red & IR AC.
    if (++handle->spo2->SpO2_Buffer_In_index==MAX_FRAME_SAMPLES) handle->spo2->SpO2_Buffer_In_index=0; //Wrap around
  }
#endif

  //fprintf(pFile_TS_PS_output, "\t%d", handle->HRM_sample_buffer[handle->HRM_Buffer_In_Index]); //Dump out BPF(PS)
  fprintf(pFile_TS_PS_output, "\n");
#endif


  //HRM DC Sensing: Sense HRM DC after Finger-On event, and select LED current to save power. Also, increase LED current if PI<HRM_PI_MIN_THRESHOLD (Helps with wrist band) or PS<HRM_DC_SENSING_WORK_LEVEL.
  if (++handle->HRM_DC_Sensing_Count >= 0xffff) handle->HRM_DC_Sensing_Count--; //limit to 0xffff
  if ((Raw_PS_Local>HRM_DC_SENSING_SKIN_CONTACT_THRESH) | (handle->HRM_DC_Sensing_Flag & HRM_DC_SENSING_CHANGE_PARAMETERS)) //Run the DC sensing during the current-change state even if PS is below the threshold
  {
    if (handle->HRM_DC_Sensing_Flag == HRM_DC_SENSING_START)
    {
      handle->HRM_DC_Sensing_Count=0;
      handle->HRM_DC_Sensing_Flag = HRM_DC_SENSING_DELAY;
    }
    else if (handle->HRM_DC_Sensing_Flag == HRM_DC_SENSING_DELAY)
    {
      i = Raw_PS_Local>handle->HRM_Raw_PS_old ? Raw_PS_Local-handle->HRM_Raw_PS_old : handle->HRM_Raw_PS_old-Raw_PS_Local; //i=abs(Raw_PS_old-Raw_PS_Local)
      if ( i<20 && handle->HRM_DC_Sensing_Count>(2*handle->Fs/100)) // delay 0.2s to let the Fignger-Off-On transition pass.
      {
        if (handle->HRM_Interpolator_Factor>1)
        {
          handle->meas_rate_DC_Sensing=0x84;		//Set Si114x meas_rate that has Fs=95Hz for DC sensing, and Si114x meas_rate will be restored after HRM DC sensing.
          handle->HRM_Interpolator_Factor_Saved=handle->HRM_Interpolator_Factor; //Save it and will be restored after HRM DC sensing.
          handle->HRM_Interpolator_Factor=1;
          if (handle->flag_samples == 0)		//If we are passing samples then do not attempt to write to registers
          {
            if (handle->uv_part==0)
              Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE, (uint8_t)handle->meas_rate_DC_Sensing);
            else
            {
              meas_rate_uv = Uncompress(handle->meas_rate_DC_Sensing);
              Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_MSB, (meas_rate_uv & 0xff00) >> 8);
              Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_LSB, meas_rate_uv & 0x00ff);
            }
          }
        }

        handle->HRM_DC_Sensing_Flag = HRM_DC_SENSING_CHANGE_PARAMETERS;
        for(i=0; i<15; i++) handle->hrmDcSensingResult[i]=0;										//Initializion
        handle->HRM_DC_Sensing_Count1=handle->HRM_DC_Sensing_Count;
        handle->HRM_current=0xf;																	//Set to the highest LED current
        handle->Si114x_adcgain_current_Sensing = (handle->Si114x_adcgain_current&(0xffff^handle->HRM_current_mask)); //Clear the HRM sensing currents.
        handle->Si114x_adcgain_current_Sensing |= (handle->HRM_current<<(handle->hrm_ps_select*4));	//highest LED current for HRM.

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
        if (handle->spo2 != NULL)
        {
          handle->Si114x_adcgain_current_Sensing |= ((handle->HRM_current<<(handle->spo2_red_ps_select*4)) |(handle->HRM_current<<(handle->spo2_ir_ps_select*4))); //highest LED current for SpO2.
          for(i=0; i<(15*2); i++) handle->spo2->spo2DcSensingResult[i]=0;
        }
#endif
        
        if (handle->flag_samples == 0)		///If we are passing samples then do not attempt to write to registers
        {
          if (handle->HRM_current_mask & 0x0ff)
            Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED21,  handle->Si114x_adcgain_current_Sensing&0xff);
          if (handle->HRM_current_mask & 0xf00)
            Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED3 , (handle->Si114x_adcgain_current_Sensing>>8)&0xf);
        }
      }
    }
    else if (handle->HRM_DC_Sensing_Flag == HRM_DC_SENSING_CHANGE_PARAMETERS)
    {
      if (handle->HRM_DC_Sensing_Count-handle->HRM_DC_Sensing_Count1>=2)//2 sampels for each current, so total sensing time=2*15=30 samples (333ms at Fs=90Hz). 1 sample/current doesn't work.
      {
        handle->hrmDcSensingResult[handle->HRM_current-1]=Raw_PS_Local;//handle->hrmDcSensingResult[i] has Raw_PS with ADCgain=1;

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
        if (handle->spo2 != NULL)
        {
          handle->spo2->spo2DcSensingResult[ 0+handle->HRM_current-1]=SpO2_PS_RED;
          handle->spo2->spo2DcSensingResult[15+handle->HRM_current-1]=SpO2_PS_IR;
        }
#endif        

        if(handle->HRM_current>1)
        {
          handle->HRM_current--;
          handle->Si114x_adcgain_current_Sensing = (handle->Si114x_adcgain_current&(0xffff^handle->HRM_current_mask)) | (handle->HRM_current<<(handle->hrm_ps_select*4)); //Set HRM2 LED currents.
       
#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
          if (handle->spo2 != NULL)
          {
            handle->Si114x_adcgain_current_Sensing |= ((handle->HRM_current<<(handle->spo2_red_ps_select*4)) |(handle->HRM_current<<(handle->spo2_ir_ps_select*4))); //Set SpO2 LED currents.
          }
#endif
          
          if (handle->flag_samples == 0)		//If we are passing samples then do not attempt to write to registers
          {
            if (handle->HRM_current_mask & 0x0ff)
              Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED21,  handle->Si114x_adcgain_current_Sensing&0xff);
            if (handle->HRM_current_mask & 0xf00)
              Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED3 , (handle->Si114x_adcgain_current_Sensing>>8)&0xf);
          }
          handle->HRM_DC_Sensing_Count1=handle->HRM_DC_Sensing_Count;
        }
        else
          handle->HRM_DC_Sensing_Flag = HRM_DC_SENSING_SENSE_FINISH; //Don't sense with current=0, in case PS with current=1 still is larger than the DC thresh.
      }
    }
    handle->HRM_Raw_PS_old=Raw_PS_Local;
  }
  else
  {//Finger-off state
    handle->HRM_Raw_PS_old=0;
    if (handle->HRM_DC_Sensing_Flag == HRM_DC_SENSING_SEEK_NO_CONTACT && handle->HRM_DC_Sensing_Count>10) //Seek the no-contact (finger-off) for 10 samples, then start to sense with LED current.
    {
      handle->Si114x_adcgain_current=handle->Si114x_adcgain_current0;					//Reset HRM current to the default.
      if (handle->flag_samples == 0)		//If we are passing samples then do not attempt to write to registers
      {
        if (handle->HRM_current_mask & 0x0ff)
          Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED21,  handle->Si114x_adcgain_current&0xff);
        if (handle->HRM_current_mask & 0xf00)
          Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED3 , (handle->Si114x_adcgain_current>>8)&0xf);
      }
      handle->HRM_DC_Sensing_Flag = HRM_DC_SENSING_START;
    }
  }

  if (handle->HRM_DC_Sensing_Flag == HRM_DC_SENSING_SENSE_FINISH)
  {
    i=(uint16_t) si114xhrm_Pick_LED_Current(&handle->hrmDcSensingResult[0]);
    handle->Si114x_adcgain_current = (handle->Si114x_adcgain_current&(0xffff^handle->HRM_current_mask)) | ((i+1)<<(handle->hrm_ps_select*4));	//Set HRM current, (i+1)=1~15
    handle->HRM_Normalization_Scaler = (uint16_t)(((int)(1<<QF_SCALER)*20000)/handle->hrmDcSensingResult[(i+1)-1]); //HRM_Normalization_Scaler in Qf

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
    if (handle->spo2 != NULL) //SpO2 DC sensing set the normalized scalers for IR and Red LEDs.
    {
      if (handle->spo2_red_ps_select!=handle->hrm_ps_select)
      {
        i=(uint16_t) si114xhrm_Pick_LED_Current(&handle->spo2->spo2DcSensingResult[0]);
        handle->Si114x_adcgain_current |= ((i+1)<<(handle->spo2_red_ps_select*4)); //Set SpO2 LED currents.
        handle->RED_Normalization_Scaler = (uint16_t)(((int)(1<<QF_SCALER)*20000)/handle->spo2->spo2DcSensingResult[0+(i+1)-1]);
      }
      else
        handle->RED_Normalization_Scaler=handle->HRM_Normalization_Scaler;

      if (handle->spo2_ir_ps_select!=handle->hrm_ps_select)
      {
        i=(uint16_t) si114xhrm_Pick_LED_Current(&handle->spo2->spo2DcSensingResult[15]);
        handle->Si114x_adcgain_current |= ((i+1)<<(handle->spo2_ir_ps_select*4)); //Set SpO2 LED currents.
        handle->IR_Normalization_Scaler = (uint16_t)(((int)(1<<QF_SCALER)*20000)/handle->spo2->spo2DcSensingResult[15+(i+1)-1]);
      }
      else
        handle->IR_Normalization_Scaler=handle->HRM_Normalization_Scaler;
    //printf("SpO2 : Red_PS=%d, IR_PS=%d, Red_Scaler=%d, IR_Scaler=%d\n", SpO2_PS_RED, SpO2_PS_IR, handle->RED_Normalization_Scaler, handle->IR_Normalization_Scaler);
    }
#endif 
    
    if (handle->flag_samples == 0)		//If we are passing samples then do not attempt to write to registers
    {
      if (handle->HRM_current_mask & 0x0ff)
        Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED21,  handle->Si114x_adcgain_current&0xff);
      if (handle->HRM_current_mask & 0xf00)
        Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED3 , (handle->Si114x_adcgain_current>>8)&0xf);
    }

    //for(j=0; j<=14; j++) printf("%x,%d\n", j, handle->hrmDcSensingResult[j]);
    //printf("HRM DC Sensing: ADCgain_Current=%04x, Scaler=%d\n", handle->Si114x_adcgain_current, handle->HRM_Normalization_Scaler);
    handle->HRM_DC_Sensing_Flag=HRM_DC_SENSING_RESTORE_FS; //Restore Fs if the Fs was changed durng the DC sensing.
    handle->HRM_DC_Sensing_Count=0;
  }

  //Count how many samples are valid since the last Finger-Off. Finger-On detection uses the raw PS and its detection threshold.
  if (++handle->HRM_PS_raw_level_count>10000) handle->HRM_PS_raw_level_count--;		//limit the count
  //Skip the Finger-On detection during the active HRM DC sensing as the PS level may be below the threshold.
  if (handle->HRM_DC_Sensing_Flag==HRM_DC_SENSING_CHANGE_PARAMETERS || handle->HRM_DC_Sensing_Flag==HRM_DC_SENSING_SENSE_FINISH || handle->HRM_DC_Sensing_Flag==HRM_DC_SENSING_RESTORE_FS)
  {
    if (handle->HRM_DC_Sensing_Flag==HRM_DC_SENSING_RESTORE_FS)//Stay this state for up to 3 samples.
    {
      if (handle->meas_rate_DC_Sensing>0) //Restore the Fs after DC sensing and initialize the interpolaor buffer.
      {
        if (handle->HRM_DC_Sensing_Count>3) //Wait # of samples to have the correct PS at the preferred current.
        {
          handle->meas_rate_DC_Sensing=0;
          handle->HRM_Interpolator_Factor=handle->HRM_Interpolator_Factor_Saved; //Restore after the HRM DC sensing
          for (j=0; j<INTERPOLATOR_L*2; j++) handle->HRM_interpolator_buf[j]=handle->hrmDcSensingResult[((handle->Si114x_adcgain_current>>(handle->hrm_ps_select*4))&0xf)-1]; //Initialized the whole interpolator buffer to the DC of the preferred current.

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
          if (handle->spo2 != NULL) //Initialized the whole interpolaor buffer to the DC of the preferred current
          {
            for (j=0; j<INTERPOLATOR_L*2; j++) handle->spo2->SpO2_Red_interpolator_buf[j]=handle->spo2->spo2DcSensingResult[ 0+((handle->Si114x_adcgain_current>>(handle->spo2_red_ps_select*4))&0xf)-1];
            for (j=0; j<INTERPOLATOR_L*2; j++) handle->spo2->SpO2_IR_interpolator_buf[j] =handle->spo2->spo2DcSensingResult[15+((handle->Si114x_adcgain_current>>(handle->spo2_ir_ps_select*4))&0xf)-1];
          }
#endif
          
          if (handle->flag_samples == 0)		//If we are passing samples then do not attempt to write to registers
          {// Restore Fs after the HRM DC sensing.
            if (handle->uv_part==0)
              Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE, (uint8_t)handle->measurement_rate);
            else
            {
              meas_rate_uv = Uncompress(handle->measurement_rate);
              Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_MSB, (meas_rate_uv & 0xff00) >> 8);
              Si114xWriteToRegister(handle->si114x_handle, REG_MEAS_RATE_LSB, meas_rate_uv & 0x00ff);
            }
          }
          handle->HRM_DC_Sensing_Flag=HRM_DC_SENSING_SEEK_NO_CONTACT;
        }
      }
      else
        handle->HRM_DC_Sensing_Flag=HRM_DC_SENSING_SEEK_NO_CONTACT;
    }
  }
  else if (handle->HRM_DC_Sensing_Flag==HRM_DC_SENSING_SEEK_NO_CONTACT && handle->HRM_DC_Sensing_Count<10){} //Stay for up to 10 samples in case LED current or/and FS cause low PS.
  else
  {
    if (Raw_PS_Local<HRM_DC_SENSING_SKIN_CONTACT_THRESH)	handle->HRM_PS_raw_level_count=0; //reset the count to 0 (Finger-Off is detected).
  }

  handle->sample_count++;

  if(hrm_data != 0) //Copy these varibles to hrm_data for display.
  {	hrm_data->Fs = handle->Fs/handle->HRM_Interpolator_Factor;
    hrm_data->hrIframe = handle->hrIframe;
    hrm_data->hrUpdateInterval = handle->hrUpdateInterval/handle->HRM_Interpolator_Factor;
    hrm_data->hrmRawPs = Raw_PS_Local;
    hrm_data->hrmPs = PS_Local;
    hrm_data->hrmNumSamples = handle->HRM_Interpolator_Factor;
    hrm_data->hrmInterpolatedPs[iInterpolator] = Raw_PS_Local;
    hrm_data->hrmSamples[iInterpolator] = handle->HRM_sample_buffer[handle->HRM_Buffer_In_Index];
    hrm_data->hrmAdcgainCurrent=handle->Si114x_adcgain_current;
  }

  if (++handle->HRM_Buffer_In_Index==MAX_FRAME_SAMPLES) handle->HRM_Buffer_In_Index=0; //Wrap around
  }
  handle->Normalized_HRM_PS=PS_Local; //Save the HRM raw PS for next sample or frame process.

  return error;
}


static int si114xhrm_Pick_LED_Current(uint16_t *hrmDcSensingResult)
{	//Observed a case that the finger was removed during the HRM DC sensing, and got HRM hrmDcSensingResult[i]=0 except for hrmDcSensingResult[7]=0x0ae4.
  //  Solution: find the index for the max. If the max is less than the finger-on thresh, take the finger-on thresh for the normalized scaler calculation.
  uint16_t i, j;
  j=0;
  for(i=0; i<15; i++)
  {
    if ( hrmDcSensingResult[j] < hrmDcSensingResult[i]) j=i; //j is the index to the max of hrmDcSensingResult[]
  }
  if (hrmDcSensingResult[j] < HRM_DC_SENSING_SKIN_CONTACT_THRESH) //In case the max is less than the finger-on thresh, take the finger-on thresh for the normalized scaler calculation.
    hrmDcSensingResult[j] = HRM_DC_SENSING_SKIN_CONTACT_THRESH;
  for(i=0; i<=j; i++)
  {
    if (hrmDcSensingResult[i]>HRM_DC_SENSING_WORK_LEVEL)
      break;
  }
  if (i==(j+1)) i=j;	//If the max of the hrmDcSensingResult[] is less than HRM_DC_SENSING_WORK_LEVEL, take the max.
  return (int)i;		//Return the LED current index: i=0~14 for LED current=i+1;
}


static int si114xhrm_BPF_filtering(Si114xhrmHandle_t _handle, int PS_input, short *pPS_output, SI114X_BPF *pBPF)
{  //Band-pass filter: 32-bit integer-point precision.
#define SOS_SCALE_SHIFT	16
#define SCALE_I_SHIFT	5
#define SCALE_F_SHIFT	8

  const short BPF_Q15=(1<<15)-1;
  int error = SI114xHRM_SUCCESS;
  int16_t i,j;
  int BPF_new_sample;
  int32_t	*pBPF_a, *pBPF_b;		//pointers for the BPF calculations
  int New_Sample_i1, New_Sample_i2; //May use __int64 for debug
  int New_Sample_i=0, New_Sample_f=0;
  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

// BPF-Ripple Reduction: After finger-On is detected, check the PS DC after 1s and clear BPF data buffers with pBPF->x[0][:]=current PS.
  uint8_t BPF_Current_Flag;
  uint16_t PS_Normlized_Thresh, PS_Normalization_Scaler, *pBPF_Ripple_Count, *pPS_Input_old;

  if (pBPF == &handle->HRM_BPF)
  {
    PS_Normlized_Thresh=HRM_PS_Raw_Min_Thresh;
    BPF_Current_Flag = HRM_BPF_ACTIVE;
    pBPF_Ripple_Count = &handle->HRM_BPF_Ripple_Count;
    pPS_Input_old = &handle->HRM_PS_Input_old;
    PS_Normalization_Scaler=handle->HRM_Normalization_Scaler;
  }
 
 #if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
//SpO2 BPF-ripple reduction

  else if ((handle->spo2 != NULL) && (pBPF == &handle->spo2->SpO2_Red_BPF))
  {
      if (handle->spo2 != NULL)
      {
    PS_Normlized_Thresh=SpO2_DC_Min_Thresh;
    BPF_Current_Flag = SpO2_RED_BPF_ACTIVE;
    pBPF_Ripple_Count = &handle->spo2->SpO2_Red_BPF_Ripple_Count;
    pPS_Input_old = &handle->spo2->SpO2_Red_PS_Input_old;
    PS_Normalization_Scaler=handle->RED_Normalization_Scaler;
      }
  }
  else if ((handle->spo2 != NULL) && (pBPF == &handle->spo2->SpO2_IR_BPF))
  {
      if (handle->spo2 != NULL)
      {
    PS_Normlized_Thresh=SpO2_DC_Min_Thresh;
    BPF_Current_Flag = SpO2_IR_BPF_ACTIVE;
    pBPF_Ripple_Count = &handle->spo2->SpO2_IR_BPF_Ripple_Count;
    pPS_Input_old = &handle->spo2->SpO2_IR_PS_Input_old;
    PS_Normalization_Scaler=handle->IR_Normalization_Scaler;
      }
  }
#endif
  
  else
  {
      error = SI114xHRM_ERROR_BAD_POINTER;
      goto Error;
  }

  if (PS_input<PS_Normlized_Thresh) //Look for the finger Off-to-On transition and then initialize the BPF variables
  {
    handle->BPF_Active_Flag &=(0xff-BPF_Current_Flag); //Skip HRM BPF filter
        *pBPF_Ripple_Count=0;
  }
  else
  {
    //When Finger On is detected, check diff(PS) and Ripple Count before initialize the BPF buffer.
    if ((handle->BPF_Active_Flag & BPF_Current_Flag)==0)
    {
      (*pBPF_Ripple_Count)++;
      i = PS_input>*pPS_Input_old ? PS_input-*pPS_Input_old : *pPS_Input_old-PS_input; //i=abs(PS-PS_old)
      if ( (i<((10*PS_Normalization_Scaler)>>QF_SCALER)) && (*pBPF_Ripple_Count>(10*handle->Fs/100))) // delay 1s
      {
        handle->BPF_Active_Flag |= BPF_Current_Flag;
        for(i=0; i<handle->BPF_biquads; i++) //zero-out the input and output buffer
        {
          for(j=0; j<3; j++)
          {
            pBPF->x[i][j] =0;
            pBPF->yi[i][j]=0;
            pBPF->yf[i][j]=0;
          }
        }
        //Use the current level to initialize these BPF variables to significantly reduce the BPF ripple.
        for(j=0; j<3; j++)
          pBPF->x[0][j]=PS_input; //Initialized to the HRM PS DC.
      }
    }
  }

  if ((handle->BPF_Active_Flag & BPF_Current_Flag)==0)
  {
    *pPS_Input_old=PS_input;
    *pPS_output=0;
    return error;
  }

  pBPF_b=handle->pBPF_b_0;			//init to the start of b coefs.
  pBPF_a=handle->pBPF_b_0+3;			//init to the start of a coefs.
  BPF_new_sample=PS_input; //normalized input

  for(i=0; i<handle->BPF_biquads; i++)
  {//filter biquad process
    for(j=3-1; j>0; j--) //Shift the BPF in/out data buffers and add the new input sample
    {
      pBPF->x[i][j]  = pBPF->x[i][j-1];
      pBPF->yi[i][j] = pBPF->yi[i][j-1];
      pBPF->yf[i][j] = pBPF->yf[i][j-1];
    }
    pBPF->x[i][0]=(int)BPF_new_sample;	//Add new sample for the current biquad
    New_Sample_i1=0;					//BPF_new_sample is used for the next biquad
    New_Sample_i2=0;
    for(j=0; j<3; j++)
    {
      if (abs(pBPF->x[i][j])<(1<<14)) //If |x| is large, scale down first before the 32-bit-int multiplication.
        New_Sample_i1 += ((((pBPF_b[j] * pBPF->x[i][j]))>>(SCALE_I_SHIFT-1))+1)>>1; //+1 is round-up
      else
        New_Sample_i1 += ((((pBPF_b[j] * (pBPF->x[i][j]>>2)))>>(SCALE_I_SHIFT-2-1))+1)>>1; //+1 is round-up
    }
    for(j=1; j<3; j++) //Shift the BPF in/out data buffers and add the new input sample
    {
      if (abs(pBPF->yi[i][j])<(1<<14)) //If |y| is large, scale down first before the 32-bit-int multiplication.
        New_Sample_i1 -= ((((pBPF_a[j] * (int)pBPF->yi[i][j]))>>(SCALE_I_SHIFT-1))+1)>>1; //+1 is round-up
      else
        New_Sample_i1 -= ((((pBPF_a[j] * (int)(pBPF->yi[i][j]>>2)))>>(SCALE_I_SHIFT-2-1))+1)>>1; //+1 is round-up

      New_Sample_i2 -= ((pBPF_a[j] * pBPF->yf[i][j]));
    }
    New_Sample_i  = ((New_Sample_i1>>(SOS_SCALE_SHIFT-SCALE_I_SHIFT-1))+1)>>1; //+1 is round-up
    New_Sample_i += ((New_Sample_i2>>(SOS_SCALE_SHIFT+SCALE_F_SHIFT-1))+1)>>1; //+1 is round-up

        New_Sample_f  = ((New_Sample_i1>>(SOS_SCALE_SHIFT-SCALE_I_SHIFT-SCALE_F_SHIFT-1))+1)>>1; //+1 is round-up
    New_Sample_f += ((New_Sample_i2>>(SOS_SCALE_SHIFT-1))+1)>>1; //+1 is round-up
    New_Sample_f -= New_Sample_i<<SCALE_F_SHIFT;

    BPF_new_sample= New_Sample_i;

    pBPF->yi[i][0]=New_Sample_i;
    pBPF->yf[i][0]=New_Sample_f;
    //fprintf(pFile_TS_PS_output, "%9.4f\n", BPF_new_sample); //Debug data log. Dump out BPF(PS)

    //update new input sample for next 2nd-order stage. point pBPF_b/_a to the next 2nd-order stage
    pBPF_b +=6;
    pBPF_a +=6;
  }//for
  *pPS_output=(short)(New_Sample_i*handle->BPF_output_scaler/BPF_Q15); //0.5=roundup

Error:
  return error;
}


static int si114xhrm_FrameProcess(Si114xhrmHandle_t _handle, int16_t *heart_rate, int32_t *HeartRateInvalidation, Si114xhrmData_t *hrm_data)
{	int error = SI114xHRM_SUCCESS;
  int16_t i, j, k, m;
  int16_t ZC_count, ZC_HR;					//Zero-crossing count and the heart rate estimated from ZC_count
  int16_t hrmPsVppLow=0, hrmPsVppHigh=0;	//Varibles for BPF(PS) min and max. Inits are needed to avoid the usage before set.
  int16_t	T_Low, T_High;						//(sample), times for min and max heart rates based on Zero-crossing count.
  int CF_abs_max=0, CF_abs_energy=1000;	//Inits are needed to avoid the usage before set.

  int HRM_PS_AutoCorr_max, HRM_PS_AutoCorr_current, HRM_PS_AutoCorr_max_index; //Auto correlation variables
  const int HRM_PS_AutoCorrMax_Thresh=100000/2880/4;	//Auto correlation threshold for validation (may need for the wristband as the pulse is usually weaker than that on fingertip))
  int16_t HeartRateX10;
  int16_t HRM_Buffer_Out_Index;				//Output index to the circular buffer of the frame sample buffer.

  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;
  T_Low=handle->T_Low0;
    T_High=handle->T_High0;						//(sample), times for min and max heart rates based on Zero-crossing count.


  handle->HRM_PS_DC += handle->Normalized_HRM_PS*handle->HRM_Interpolator_Factor;	//HRM_PS_DC is the PS DC accumulator. PS is the normalized PS and also input to BPF.

  //3) Frame process (Background job)
  if (handle->sample_count>=handle->hrUpdateInterval)
  {//Received new hrUpdateInterval samples
    handle->sample_count -= handle->hrUpdateInterval;						//adjust the count for the next block of samples
    *HeartRateInvalidation = SI114xHRM_STATUS_SUCCESS;						//Clears all HRM and SpO2 status bits.

#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
    if (handle->spo2 != NULL)
    {
      handle->spo2->SpO2_Percent=SPO2_STATUS_PROCESS_SPO2_FRAME;				//Notify SpO2 to process the SpO2 frame.
    }
#endif

    //Re-initialize hrIframe to the min length when "Finger-off", so to speed up the next HR reporting.
    if ((handle->HeartRateInvalidation_Previous & (SI114xHRM_STATUS_FINGER_OFF|SI114xHRM_STATUS_FINGER_ON|SI114xHRM_STATUS_BPF_PS_VPP_OFF_RANGE|SI114xHRM_STATUS_AUTO_CORR_MAX_INVALID)))
    {
      handle->NumOfLowestHRCycles=NumOfLowestHRCyclesMin;
      handle->hrIframe=handle->T_Low0*handle->NumOfLowestHRCycles/100; //(sample)
    }
    //After the first valid HR is detected, hrIframe is increased by hrUpdateInterval up to the max(final) length.
    if (((handle->HeartRateInvalidation_Previous & SI114xHRM_STATUS_HRM_MASK)==SI114xHRM_STATUS_SUCCESS) && (handle->NumOfLowestHRCycles < NumOfLowestHRCyclesMax) && (handle->hrIframe+handle->hrUpdateInterval)<MAX_FRAME_SAMPLES)//Make sure hrIframe<MAX_FRAME_SAMPLES.
    {
      handle->hrIframe +=handle->hrUpdateInterval; //(sample)
      handle->NumOfLowestHRCycles += handle->hrUpdateInterval*100/handle->T_Low0; //cycles(x100)
    }

  #if HRM_PS_AGC //When HR is valid, increase LED current if PS is below the work level or PI is below 0.2%(could occur on wrist).
    if ((handle->HeartRateInvalidation_Previous & SI114xHRM_STATUS_HRM_MASK)==SI114xHRM_STATUS_SUCCESS)
    {
      //HRM AGC on raw PS signal
      if ((handle->HRM_PS_AGC_Flag&HRM_PS_AGC_ENABLED)==HRM_PS_AGC_ENABLED && (handle->HRM_PS_AGC_Flag&HRM_PS_AGC_GAIN_CHANGED)!=HRM_PS_AGC_GAIN_CHANGED)
      {
        i=(handle->Si114x_adcgain_current>>(handle->hrm_ps_select*4))&0xf; //HRM LED current
        if  ((handle->hrmRawPs < HRM_DC_SENSING_WORK_LEVEL || handle->hrmPerfusionIndex<HRM_PI_MIN_THRESHOLD) && i<0xf)
        {//Increase the LED current by 1: 1) if HRM DC drafts lower or the DC sensing is lower due to motion, 2) PI in the last frame is less than 0.2%.
          handle->HRM_PS_AGC_Flag |= HRM_PS_AGC_GAIN_CHANGED;
          handle->Si114x_adcgain_current=(handle->Si114x_adcgain_current&(0xffff^(0xf<<(handle->hrm_ps_select*4)))) | ((i+1)<<(handle->hrm_ps_select*4)); //Increase LED current
          if (handle->flag_samples == 0)		//If we are passing samples then do not attempt to write to registers
          {
            if (handle->HRM_current_mask & 0x0ff)
              Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED21,  handle->Si114x_adcgain_current&0xff);
            if (handle->HRM_current_mask & 0xf00)
              Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED3 , (handle->Si114x_adcgain_current>>8)&0xf);
          }
        }
      }
    }
  #endif
    HRM_Buffer_Out_Index=handle->HRM_Buffer_In_Index-handle->hrIframe; //Set the output pointer based on the Input pointer.
    if (HRM_Buffer_Out_Index<0) HRM_Buffer_Out_Index +=MAX_FRAME_SAMPLES; //Wrap around

    //Validation: Invalidate if any sample in the current frame is below the level threshold
    if (handle->HRM_PS_raw_level_count < handle->hrIframe)
    {
      if(handle->HRM_PS_raw_level_count<=handle->hrUpdateInterval)
        *HeartRateInvalidation |= SI114xHRM_STATUS_FINGER_OFF;
      else
        *HeartRateInvalidation |= SI114xHRM_STATUS_FINGER_ON;
    }
    else if ((handle->HeartRateInvalidation_Previous & SI114xHRM_STATUS_HRM_MASK) == SI114xHRM_STATUS_FINGER_ON)
    {//Validation: Still Finger-On mode if the last frame is Finger-On and the leading zeros in the current frame are 5% or more of iFrame samples.
      i=HRM_Buffer_Out_Index;
      for(k=0; k<handle->hrIframe*5/100; k++)
      {
        if (handle->HRM_sample_buffer[i]!=0) break; //Break if non-zero
        if (++i==MAX_FRAME_SAMPLES) i=0; //Wrap around
      }
      if (k==handle->hrIframe*5/100) *HeartRateInvalidation|=SI114xHRM_STATUS_FINGER_ON;
    }

    if (*HeartRateInvalidation==0) //Skip below if the frame is invalid for heart-rate detection
    {
      //a) Calculate the max/min values and zero-crossing counts
      ZC_count=0;
      hrmPsVppHigh=-20000;
      hrmPsVppLow =+20000;
      i=HRM_Buffer_Out_Index;
      handle->HRM_PS_DC /= handle->hrUpdateInterval;	//Now, HRM_PS_DC is the averaged normalized PS over hrUpdateInterval sample (1s)
      for(k=0; k<handle->hrIframe; k++) //Scaled HRM PS AC, max/min values
      {
        //Scaled HRM PS AC based on HRM PS DC with reference of HRM_PS_DC_REFERENCE
        m=(handle->HRM_sample_buffer[i]*HRM_PS_DC_REFERENCE+(uint16_t)handle->HRM_PS_DC/2)/(uint16_t)handle->HRM_PS_DC; //Round-up
        handle->HRM_sample_buffer[i]=m; //Scaled HRM_PS_AC
        if (hrmPsVppHigh<m) hrmPsVppHigh=m;
        if (hrmPsVppLow >m) hrmPsVppLow =m;
        if (++i==MAX_FRAME_SAMPLES) i=0; //Wrap around
      }
      //Validation: Invalidate if the HRM_PS_Vpp is out of the range
      if (hrmPsVppLow<handle->HRM_PS_Vpp_min || hrmPsVppHigh>handle->HRM_PS_Vpp_max)
        *HeartRateInvalidation |= SI114xHRM_STATUS_BPF_PS_VPP_OFF_RANGE;

      i=HRM_Buffer_Out_Index;
      for(k=0; k<handle->hrIframe-1; k++) //zero-crossing counts
      {	//Find zero-crossing counts for BPF(PS)-hrmPsVppHigh/ZR_BIAS_SCALE as BPF(PS) is asymmetric.
        m=i+1;
        if (m==MAX_FRAME_SAMPLES) m=0; //Wrap around
        if (((int)(handle->HRM_sample_buffer[i]-hrmPsVppHigh*(ZR_BIAS_SCALE))*2+1)*((int)(handle->HRM_sample_buffer[m]-hrmPsVppHigh*(ZR_BIAS_SCALE))*2+1)<0)
          ZC_count++;
        if (++i==MAX_FRAME_SAMPLES)	i=0; //Wrap around
      }

      //ZC_HR=ZC_count./2.*60./(iFrame/Fs); %(bmp), rough heart-rate estimate from the zero-cross count
      ZC_HR=ZC_count*60*(handle->Fs+5)/10/handle->hrIframe/2;

      //Validation: Invalidate if the zero-crossing HR is out of the HR range
      if (ZC_HR<f_Low || ZC_HR>f_High)
        *HeartRateInvalidation |= SI114xHRM_STATUS_ZERO_CROSSING_INVALID;
      else
      {
        //Modify T_High and T_Low based on rough heart-rate estimate that is from the zero-cross count.
        //T_Low =round(60./(max(ZC_HR.*(1-0.17),f_Low )./Fs));    %(sample)
        //T_High=round(60./(min(ZC_HR.*(1+0.17),f_High)./Fs));    %(sample)
        T_Low  = ZC_HR*(100-ZC_HR_TOLERANCE)< f_Low*100  ? handle->T_Low0  : 60*(handle->Fs+5)/10*100/(ZC_HR*(100-ZC_HR_TOLERANCE));
        T_High = ZC_HR*(100+ZC_HR_TOLERANCE)> f_High*100 ? handle->T_High0 : 60*(handle->Fs+5)/10*100/(ZC_HR*(100+ZC_HR_TOLERANCE));
        //T_Low=T_Low0; T_High=T_High0; //Don't limit T_Low and T_T_High based on Zero-crossing counts

        //b) Compute Crest factor of PS
        CF_abs_max= hrmPsVppHigh > abs(hrmPsVppLow) ? hrmPsVppHigh : abs(hrmPsVppLow);
        CF_abs_energy=0;
        i=HRM_Buffer_Out_Index;
        for(k=0; k<handle->hrIframe; k++) //zero-crossing counts
        {	//Find zero-crossing counts for BPF(PS)-hrmPsVppHigh/ZR_BIAS_SCALE as BPF(PS) is asymmetric.
          if (abs(handle->HRM_sample_buffer[i]) < handle->HRM_PS_Vpp_max )
          {
            CF_abs_energy += handle->HRM_sample_buffer[i]*handle->HRM_sample_buffer[i];
          }
          else
          {
            CF_abs_energy += handle->HRM_PS_Vpp_max*handle->HRM_PS_Vpp_max;
          }
          if (++i==MAX_FRAME_SAMPLES) i=0; //Wrap around
        }
      }


      //%Validation 1: Invalidate the frame if CF > CF_threshold
      if (CF_abs_max*CF_abs_max > handle->HRM_PS_CrestFactor_Thresh*(CF_abs_energy/handle->hrIframe))
        *HeartRateInvalidation |= SI114xHRM_STATUS_CREST_FACTOR_TOO_HIGH;

      if (*HeartRateInvalidation!=0)
      {//Scaled back HRM PS AC, so that HRM sample buffer is same as before
        i=HRM_Buffer_Out_Index;
        for(k=0; k<handle->hrIframe; k++)
        {
          handle->HRM_sample_buffer[i]=(handle->HRM_sample_buffer[i]*(uint16_t)handle->HRM_PS_DC+HRM_PS_DC_REFERENCE/2)/HRM_PS_DC_REFERENCE; //Round up
          if (++i==MAX_FRAME_SAMPLES) i=0; //Wrap around
        }
      }
    }//HeartRateInvalidation

    if (*HeartRateInvalidation==0) //Skip if the frame is invalid for heart-rate detection
    {
      //Compute the auto correlation on BPF(PS)
      //HRM_PS_AutoCorr=xcorr2(PS(t_Frame)); %Only need compute the auto correlation for the delay [T_High, T_Low]
      //int HRM_PS_AutoCorr[1000];	for(i=0; i<handle->T_Low0; i++) HRM_PS_AutoCorr[i]=0;	//Debug: Auto correlation buffer, inits to 0.
      HRM_PS_AutoCorr_max=-100000000;				//A large negative number
      HRM_PS_AutoCorr_max_index=T_High;
      for(i=T_High; i<=T_Low; i++)
      {
        HRM_PS_AutoCorr_current=0;
        j=HRM_Buffer_Out_Index;
        for(k=0; k<handle->hrIframe-i; k++)
        {
          m=j+i;
          if (m>=MAX_FRAME_SAMPLES) m -= MAX_FRAME_SAMPLES; //Wrap around
          HRM_PS_AutoCorr_current += handle->HRM_sample_buffer[j]*handle->HRM_sample_buffer[m];
          if (++j==MAX_FRAME_SAMPLES) j=0; //Wrap around
        }
        //HRM_PS_AutoCorr[i]=HRM_PS_AutoCorr_current; //Debug
        //Find the max
        if (HRM_PS_AutoCorr_current > HRM_PS_AutoCorr_max)
        {
          HRM_PS_AutoCorr_max = HRM_PS_AutoCorr_current;
          HRM_PS_AutoCorr_max_index = i;
        }
      }

      //Validation: Invalidate if the auto-correlation max is too small.
      if (HRM_PS_AutoCorr_max < HRM_PS_AutoCorrMax_Thresh*handle->hrIframe)
      {
        *HeartRateInvalidation |= SI114xHRM_STATUS_AUTO_CORR_TOO_LOW;
      }
      else if (HRM_PS_AutoCorr_max_index == T_Low || HRM_PS_AutoCorr_max_index == T_High)
      {//Validation: Invalidate if the auto-correlation max occurs at the search bounday of [T_Low, T_High].
        *HeartRateInvalidation |=SI114xHRM_STATUS_AUTO_CORR_MAX_INVALID;
      }
      else
      {
        //HeartRate=(60*Fs*2/HRM_PS_AutoCorr_max_index+1)/2; //+1 for Roundup
        HeartRateX10=(600*(handle->Fs+5)/10*2/HRM_PS_AutoCorr_max_index+1)/2; //+1 for Roundup
        *heart_rate = (HeartRateX10/5+1)/2;
      }

      //Scaled back HRM PS AC, so that HRM sample buffer is same as before
      i=HRM_Buffer_Out_Index;
      for(k=0; k<handle->hrIframe; k++)
      {
        handle->HRM_sample_buffer[i]=(handle->HRM_sample_buffer[i]*(uint16_t)handle->HRM_PS_DC+HRM_PS_DC_REFERENCE/2)/HRM_PS_DC_REFERENCE; //Round up
        if (++i==MAX_FRAME_SAMPLES) i=0; //Wrap around
      }
    }//HeartRateInvalidation

    handle->HeartRateInvalidation_Previous=*HeartRateInvalidation & SI114xHRM_STATUS_HRM_MASK; //Copy all HRM bits.
    *HeartRateInvalidation |=SI114xHRM_STATUS_FRAME_PROCESSED;
    if(hrm_data != 0) //Copy these varibles to hrm_data for display.
    {	hrm_data->hrmPsVppLow  = ( hrmPsVppLow*(uint16_t)handle->HRM_PS_DC+HRM_PS_DC_REFERENCE/2)/HRM_PS_DC_REFERENCE; //Round up;
      hrm_data->hrmPsVppHigh = (hrmPsVppHigh*(uint16_t)handle->HRM_PS_DC+HRM_PS_DC_REFERENCE/2)/HRM_PS_DC_REFERENCE; //Round up;
      if ((CF_abs_energy/handle->hrIframe)==0)
        hrm_data->hrmCrestFactor=-1; //Invalid Crest factor
      else
        hrm_data->hrmCrestFactor=(CF_abs_max*CF_abs_max)/(CF_abs_energy/handle->hrIframe);
      if (hrm_data->hrmPs!=0)
        handle->hrmPerfusionIndex=10000*(hrm_data->hrmPsVppHigh - hrm_data->hrmPsVppLow)/hrm_data->hrmPs;
      else
        handle->hrmPerfusionIndex=0;
      hrm_data->hrmPerfusionIndex=handle->hrmPerfusionIndex;
      for(i=0; i<15; i++) hrm_data->hrmDcSensingResult[i]=handle->hrmDcSensingResult[i];
    }
    handle->HRM_PS_DC=0;				//Reset HRM PS DC accumulator
  }//Frame process, sample_count==hrUpdateInterval

  return error;
}


#if (SI114XHRM_BUILD_GECKO_SPO2 == 1) //Compile In/Out SpO2
static int si114xhrm_SpO2FrameProcess(Si114xhrmHandle_t _handle, int16_t *SpO2, int32_t *HeartRateInvalidation, Si114xhrmData_t *hrm_data)
{	int error = SI114xHRM_SUCCESS;
  int SpO2_RED_DC, SpO2_IR_DC=1;
  int SpO2_RED_AC2, SpO2_IR_AC2;
  int16_t SpO2_RED_AC_min, SpO2_RED_AC_max, SpO2_IR_AC_min=0, SpO2_IR_AC_max=0;
  int SpO2_R;
  uint16_t SpO2_R_Root, SpO2_R_Root_Shift, SpO2_R_Root_TMP;
  uint16_t SpO2_AC2_Scaler;
  int16_t SpO2_RED_Crest_Factor, SpO2_IR_Crest_Factor;
  int16_t SpO2_Buffer_Out_index;	//Output index to the frame sample circular buffer.
  uint16_t spo2DcToAcRatio=0;										//Ratio of DC to ACpp. For debug reporting
  int16_t spo2CrestFactor=0;										//Crest factor (C^2)
  int16_t i, k;
  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

    if (handle->spo2 == NULL)
    return SI114xHRM_STATUS_SPO2_EXCEPTION;
  //3) Frame process (Background job)
  if (handle->spo2->SpO2_Percent==SPO2_STATUS_PROCESS_SPO2_FRAME) //HRM_frame_process sets the flag value when a new frame of samples is available.
  {
    handle->spo2->SpO2_Percent ^= SPO2_STATUS_PROCESS_SPO2_FRAME;		//Clear the flag. SpO2_Percent=0;
    //Count how many samples are Finger-On since the last Finger-Off.
    i=handle->spo2->SpO2_Buffer_In_index-handle->hrUpdateInterval; //Set the output buffer index based on the Input index.
    if (i<0) i +=MAX_FRAME_SAMPLES; //Wrap around
    for(k=0; k<handle->hrUpdateInterval; k++) //Append the new block of samples
    {
      handle->spo2->SpO2_raw_level_count = handle->spo2->SpO2_raw_level_count>10000 ? handle->spo2->SpO2_raw_level_count:handle->spo2->SpO2_raw_level_count+1; //limit the count to 10000
#if SPO2_REFLECTIVE_MODE
      if ((handle->spo2->SpO2_RED_DC_sample_buffer[i]<SpO2_DC_Min_Thresh) || (handle->spo2->SpO2_IR_DC_sample_buffer[i]<SpO2_DC_Min_Thresh))
#else
      if ((handle->spo2->SpO2_RED_DC_sample_buffer[i]>SpO2_DC_Min_Thresh) || (handle->spo2->SpO2_IR_DC_sample_buffer[i]>SpO2_DC_Min_Thresh))
#endif
        handle->spo2->SpO2_raw_level_count=0;		//reset the count to 0
      if (++i==MAX_FRAME_SAMPLES) i=0; //Wrap around
    }

    SpO2_Buffer_Out_index=handle->spo2->SpO2_Buffer_In_index-handle->hrIframe; //Set the output buffer index based on the Input index.
    if (SpO2_Buffer_Out_index<0) SpO2_Buffer_Out_index +=MAX_FRAME_SAMPLES; //Wrap around

    handle->spo2->TimeSinceFingerOff = handle->spo2->TimeSinceFingerOff <60000 ? handle->spo2->TimeSinceFingerOff+1*handle->Fs/10 : handle->spo2->TimeSinceFingerOff; //Limit to 60000

    if (handle->spo2->SpO2_raw_level_count<handle->hrIframe)
    {//Validation 1: Finger Off or On
      if (handle->spo2->SpO2_raw_level_count < handle->hrUpdateInterval)
      {
        *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_FINGER_OFF;
        handle->spo2->TimeSinceFingerOff=0;						//Reset if Finger off
      }
      else
        *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_FINGER_ON;
    }
    else
    {
      SpO2_RED_DC=0;
      SpO2_IR_DC=0;
      SpO2_RED_AC2=0;		//sum(Red AC^2)
      SpO2_IR_AC2=0;		//sum(IR AC^2)
      SpO2_RED_AC_min=0x7fff;
      SpO2_IR_AC_min=0x7fff;
      SpO2_RED_AC_max=0x8000;
      SpO2_IR_AC_max=0x8000;
      SpO2_AC2_Scaler=1;
      i=SpO2_Buffer_Out_index;
      for(k=0; k<handle->hrIframe; k++)
      {
        SpO2_RED_DC += handle->spo2->SpO2_RED_DC_sample_buffer[i];
        SpO2_IR_DC  += handle->spo2->SpO2_IR_DC_sample_buffer[i];
        if (SpO2_RED_AC2>0x20000000 || SpO2_IR_AC2>0x20000000) //AC_sample_buffer[i]<0x7fff
        {
          SpO2_AC2_Scaler++; SpO2_RED_AC2 >>=1; SpO2_IR_AC2 >>=1; //Change the scaler shift and scale sum(AC^2).
        }
        SpO2_RED_AC2 +=(handle->spo2->SpO2_RED_AC_sample_buffer[i]*handle->spo2->SpO2_RED_AC_sample_buffer[i])>>SpO2_AC2_Scaler;	//This is sum(AC^2)
        SpO2_IR_AC2  +=(handle->spo2->SpO2_IR_AC_sample_buffer[i] *handle->spo2->SpO2_IR_AC_sample_buffer[i] )>>SpO2_AC2_Scaler;	//This is sum(AC^2)

        //Find the AC min and max.
        SpO2_RED_AC_min=handle->spo2->SpO2_RED_AC_sample_buffer[i]<SpO2_RED_AC_min ? handle->spo2->SpO2_RED_AC_sample_buffer[i]:SpO2_RED_AC_min;
        SpO2_IR_AC_min =handle->spo2->SpO2_IR_AC_sample_buffer[i] <SpO2_IR_AC_min  ? handle->spo2->SpO2_IR_AC_sample_buffer[i] :SpO2_IR_AC_min;
        SpO2_RED_AC_max=handle->spo2->SpO2_RED_AC_sample_buffer[i]>SpO2_RED_AC_max ? handle->spo2->SpO2_RED_AC_sample_buffer[i]:SpO2_RED_AC_max;
        SpO2_IR_AC_max =handle->spo2->SpO2_IR_AC_sample_buffer[i] >SpO2_IR_AC_max  ? handle->spo2->SpO2_IR_AC_sample_buffer[i] :SpO2_IR_AC_max;

        if (++i==MAX_FRAME_SAMPLES)
          i=0; //Wrap around
      }

      //Calculate Crest factor: C^2=|Vpeak|^2/Vrms^2.
      i= -SpO2_RED_AC_min > SpO2_RED_AC_max ?  -SpO2_RED_AC_min : SpO2_RED_AC_max; //i=|Vpeak|
      SpO2_RED_Crest_Factor = SpO2_RED_AC2>0 ? ((((i*i)>>SpO2_AC2_Scaler)*handle->hrIframe)/SpO2_RED_AC2) : 9999;

      i= -SpO2_IR_AC_min  > SpO2_IR_AC_max  ?  -SpO2_IR_AC_min  : SpO2_IR_AC_max;  //i=|Vpeak|
      SpO2_IR_Crest_Factor  = SpO2_IR_AC2 >0 ? ((((i*i)>>SpO2_AC2_Scaler)*handle->hrIframe)/SpO2_IR_AC2) : 9999;

      spo2CrestFactor= SpO2_RED_Crest_Factor>SpO2_IR_Crest_Factor ? SpO2_RED_Crest_Factor : SpO2_IR_Crest_Factor; //Take the larger one.

      //Validation 2: Check RED/IR AC with reference to its DC (to exclude the case on a non-finger surface or BPF transition).
      //Debug: check spo2DcToAcRatio. Can be removed.
      spo2DcToAcRatio=9999;
      if ((SpO2_RED_AC_max-SpO2_RED_AC_min)>0 && (SpO2_IR_AC_max-SpO2_IR_AC_min)>0)
      {
        spo2DcToAcRatio= (SpO2_RED_DC/handle->hrIframe)/(SpO2_RED_AC_max-SpO2_RED_AC_min);
        spo2DcToAcRatio = spo2DcToAcRatio>((SpO2_IR_DC/handle->hrIframe)/(SpO2_IR_AC_max-SpO2_IR_AC_min)) ? spo2DcToAcRatio:((SpO2_IR_DC/handle->hrIframe)/(SpO2_IR_AC_max-SpO2_IR_AC_min));
      }

      if ( ((SpO2_RED_DC/handle->hrIframe))>((SpO2_RED_AC_max-SpO2_RED_AC_min)*SpO2_DC_to_ACpp_Ratio_High_Thresh) || \
         ((SpO2_IR_DC/handle->hrIframe ))>((SpO2_IR_AC_max -SpO2_IR_AC_min )*SpO2_DC_to_ACpp_Ratio_High_Thresh) )
      {
        *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_TOO_LOW_AC;		//to exclude the case on a non-finger surface
      }
      else if ( ((SpO2_RED_DC/handle->hrIframe))<((SpO2_RED_AC_max-SpO2_RED_AC_min)*SpO2_DC_to_ACpp_Ratio_Low_Thresh) || \
              ((SpO2_IR_DC/handle->hrIframe ))<((SpO2_IR_AC_max -SpO2_IR_AC_min )*SpO2_DC_to_ACpp_Ratio_Low_Thresh) )
      {
        *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_TOO_HIGH_AC;	//Exclude possible BPF transition.
      }
      else if (spo2CrestFactor>SpO2_Crest_Factor_Thresh)
      {//Validation 3: Crest factor check to exclude the finger moving and BPF transition.
        *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_CREST_FACTOR_OFF;
      }
      else
      {
        //Average DC and AC
        SpO2_RED_DC /= handle->hrIframe;	//Averaged RED DC
        SpO2_IR_DC	/= handle->hrIframe;	//Averaged IR DC
        SpO2_RED_AC2/= handle->hrIframe;	//Averaged RED AC^2
        SpO2_IR_AC2	/= handle->hrIframe;	//Averaged IR AC^2

        if (SpO2_RED_AC2>0 && SpO2_IR_AC2>0)
        {
          //SpO2(%)=114 - 25*R, where R=(ACred/DCred)/(ACir/DCir)
          //1. Calculate RMS(R)^2 as AC2=RMS^2.
#define SQRT_SHIFT	16
#define R_SCALER	128
          SpO2_R=(R_SCALER*R_SCALER*((SpO2_IR_DC*SpO2_IR_DC)/SpO2_RED_DC))/SpO2_RED_DC; //(R_SCALER*IR_DC/RED_DC)^2
          SpO2_R=(SpO2_R*SpO2_RED_AC2)/SpO2_IR_AC2;

          //2. Calculate 32-bit square root(The input is 32-bit (max<=0x3fffffff). The output is the square root in 16-bit SpO2_R_Root)
          //SpO2_R=0x3fffffff; //Test: Input=0x3fffffff(Root=0x7fff). Input=0x10000000(Root=0x4000).
          SpO2_R_Root=1<<(SQRT_SHIFT-1);
          SpO2_R_Root_Shift=1<<(SQRT_SHIFT-1);
          SpO2_R_Root_TMP=0;
          for (i=0; i<SQRT_SHIFT; i++)
          {
            if (SpO2_R>=(SpO2_R_Root*SpO2_R_Root))
            {
              SpO2_R_Root_TMP=SpO2_R_Root;
            }
            SpO2_R_Root_Shift >>= 1;//Divided by 2
            SpO2_R_Root=SpO2_R_Root_Shift+SpO2_R_Root_TMP;
          }

          //3. SpO2(%)=114 - 25*R. SpO2 resolution is 25/R_SCALER=0.2% as R resolution is 1/R_SCALER.
          handle->spo2->SpO2_Percent=(int16_t)(114-(25*SpO2_R_Root+R_SCALER/2)/R_SCALER); //R_SCALER/2 due to the round-off.

          //4. Limit SpO2(%) to 75~99%
          handle->spo2->SpO2_Percent= handle->spo2->SpO2_Percent>99 ? 99 : handle->spo2->SpO2_Percent; //upper limit=99%
          if (handle->spo2->SpO2_Percent<75)
            *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_EXCEPTION; //If SpO2<75%, SpO2 is invalid.

          if (handle->spo2->TimeSinceFingerOff < 51*handle->Fs/10/10)	//Delay 5.1(s) to report SpO2 after the last Finger off.
            *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_EXCEPTION;
        }
        else
        {//Exception: Can occur if the finger is a little distance above the sensors without contact.
          *HeartRateInvalidation |= SI114xHRM_STATUS_SPO2_EXCEPTION;
        }
      }
    }

    *SpO2=handle->spo2->SpO2_Percent;
    if(hrm_data != 0) //Copy these varibles to hrm_data for display.
    {
      i=handle->spo2->SpO2_Buffer_In_index-1; //i points to the newest sample.
      if (i<0) i += MAX_FRAME_SAMPLES; //Wrap around
      hrm_data->spo2RedDcSample=handle->spo2->SpO2_RED_DC_sample_buffer[i];
      hrm_data->spo2IrDcSample=handle->spo2->SpO2_IR_DC_sample_buffer[i];
      hrm_data->spo2DcToAcRatio=spo2DcToAcRatio;
      hrm_data->spo2CrestFactor=spo2CrestFactor;
      hrm_data->spo2IrPerfusionIndex=10000*(SpO2_IR_AC_max-SpO2_IR_AC_min)/SpO2_IR_DC;
      for(i=0; i<15*2; i++) hrm_data->spo2DcSensingResult[i]=handle->spo2->spo2DcSensingResult[i];
    }
  } //sample_count==0

  return error;
}
#endif


static int32_t si114xhrm_IdentifyPart(Si114xhrmHandle_t _handle, int16_t *part_id)
{ int valid_part = 0;
  SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

  *part_id=Si114xReadFromRegister(handle->si114x_handle, REG_PART_ID);
  switch(*part_id)
  { case 0x41:
    case 0x42:
    case 0x43:
    case 0x44:
    case 0x32:
    case 0x45:
    case 0x46:
    case 0x47:
      valid_part = 1;
      break;
    default:
      valid_part = 0;
      break;
  }

  return valid_part;
}




/*  Platform Specific Functions - These functions are not portable and therefore will
  likely require modification for new platorms
*/
  /*
    options[3:1] - device number on module EVB
    options[0] - 0=get sample from device; 1=sample passed from application
  */

int32_t si114xhrm_Initialize(void *port_name, int32_t options, Si114xhrmHandle_t *handle)
{	int error = SI114xHRM_SUCCESS;
  int device;
  int16_t part_id = 0;

  SI114xHRM_STRUCT *_handle;

//uncomment these to confirm the value of HRM_DATA_SIZE and SPO2_DATA_SIZE
//  int16_t debug = sizeof(SI114xHRM_STRUCT);
//  int16_t debug2 = sizeof(Si114xSPO2_STRUCT);

#if (SI114xHRM_USE_DYNAMIC_DATA_STRUCTURE == 0)
  _handle = (SI114xHRM_STRUCT*)((Si114xDataStorage_t*)(*handle))->hrm;
  _handle->spo2 = (Si114xSPO2_STRUCT*)(((Si114xDataStorage_t*)(*handle))->spo2);
#else
  _handle = (SI114xHRM_STRUCT *)malloc(sizeof(SI114xHRM_STRUCT));
  _handle->spo2 = (Si114xSPO2_STRUCT *)malloc(sizeof(Si114xSPO2_STRUCT));
#endif

  device = ((options & 0xE)>>1) + 1;


  if ((options & 0x01) == 0)		// if taking new samples
  { Si114xInit(port_name, device, &(_handle->si114x_handle));


      if(si114xhrm_IdentifyPart(_handle, &part_id) == 0)
      { error = SI114xHRM_ERROR_INVALID_PART_ID;
        Si114xClose(_handle->si114x_handle);
#if (SI114xHRM_USE_DYNAMIC_DATA_STRUCTURE != 0)
        if(_handle != 0)
        {
          free(_handle);
          _handle = 0;
        }
#endif
      }
      else
      { si114xhrm_IsUvDevice(_handle, &_handle->uv_part);
        _handle->flag_samples = 0;
      }
    }
    else
    {
      _handle->flag_samples = 1;
      _handle->uv_part = 0;
    }

    (*handle) = (Si114xhrmHandle_t)_handle;

  return error;
}


  int32_t si114xhrm_Close(Si114xhrmHandle_t _handle)
  {	int error = SI114xHRM_SUCCESS;

    SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

    if (handle != 0)
    {
    #if	HRM_DATA_LOG //Close the data log file
          fclose(pFile_TS_PS_output);
    #endif	//HRM_DATA_LOG
      if (handle->flag_samples == 0)		//If we are passing samples then do not attempt to write to registers
      {

        Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED21, (0 << 4) + 0); //Set LED1 and LED2 currents to 0, so to turn off the LEDs to save power.
        Si114xWriteToRegister(handle->si114x_handle, REG_PS_LED3, 0);			//Set LED3 current to 0, so to turn off the LED3 to save power.
        Si114xClose(handle->si114x_handle);
      }
  #if (SI114xHRM_USE_DYNAMIC_DATA_STRUCTURE != 0)
      free(handle);
  #endif
    }

    return error;
  }


  int32_t si114xhrm_GetLowLevelHandle(Si114xhrmHandle_t _handle, HANDLE *si114x_handle)
  {	int error = SI114xHRM_SUCCESS;

    SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

    *si114x_handle = handle->si114x_handle;


  return error;
  }


  int32_t si114xhrm_SetupDebug(Si114xhrmHandle_t _handle, void *debug)
  {	int error = SI114xHRM_SUCCESS;
    SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;
    error = si114xSetupDebug(handle->si114x_handle, debug, 0);
    return error;
  }

  int32_t si114xhrm_FindEvb(int8_t *port_description, int8_t *last_port, int32_t *num_ports_found)
  {	int error = SI114xHRM_SUCCESS;
#if (SI114xHRM_BUILD_TARGET == SI114xHRM_BUILD_TARGET_WINDOWS)
      error = si114xFindEvb((char *)port_description, (char *)last_port, num_ports_found);
    if (error == -1)
      error = SI114xHRM_ERROR_EVB_NOT_FOUND;
#else
    (void) port_description;  //this line "touches" the parameter to remove the parameter not used warning
    (void) last_port;         //this line "touches" the parameter to remove the parameter not used warning
    (void) num_ports_found;   //this line "touches" the parameter to remove the parameter not used warning
    error = SI114xHRM_ERROR_FUNCTION_NOT_SUPPORTED;
    
#endif
    return error;
  }

  int32_t si114xhrm_OutputDebugMessage(Si114xhrmHandle_t _handle, int8_t * message)
  {
    int error = SI114xHRM_SUCCESS;
    SI114xHRM_STRUCT *handle = (SI114xHRM_STRUCT *)_handle;

    error = si114xOutputDebugMessage(handle->si114x_handle, (char *)message);
    return error;
  }
