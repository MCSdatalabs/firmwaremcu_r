/*********************************************************************************
	This software is confidential and proprietary information owned by Silicon 
	Labs which is subject to worldwide copyright protection.  No use or other 
	rights associated with this software may exist other than as expressly 
	stated by written agreement with Silicon Labs.  No licenses to the 
	intellectual property rights included in the software, including patent and 
	trade secret rights, are otherwise given by implication, estoppel or otherwise.
	
	This file is used to configuration compilation options.

***********************************************************************************/

#ifndef SI114XHRM_CFG_H__

#define	SI114XHRM_CFG_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "../../../../heartrate monitor/si114x_types.h"
#include "si114xhrm.h"


/* Build Targerts
 * 0 = Windows
 * 1 = GECKO
 */
#define	SI114xHRM_BUILD_TARGET_WINDOWS	0
#define	SI114xHRM_BUILD_TARGET_GECKO	1

#ifndef SI114xHRM_BUILD_TARGET
	#define SI114xHRM_BUILD_TARGET	SI114xHRM_BUILD_TARGET_WINDOWS
#endif

#ifndef SI114XHRM_BUILD_GECKO_SPO2
	#define SI114XHRM_BUILD_GECKO_SPO2	1
#endif


/*  The ENABLE_MEASUREMENT_RATE_xxHz compile switches will insert/remove code for
 * 	the supported measurement rates.  It is implemented this way save RAM.
 * 	Disabling one of the switched will remove the BPF coefficient global for all
 * 	the given measurement rate.  Note that the rate are the interpolated rate
 *  not the actual si114x sample rate.
 */
#if (SI114xHRM_BUILD_TARGET == SI114xHRM_BUILD_TARGET_WINDOWS)
	#include "Si114x_PGM_Toolkit.h"		//ToDo:  This is required for Windows...in the future we should refactor the programmer's toolkit so that all types are in si114x_types.h

	#define SI114X_SPO2				1	
	#define	MAX_FRAME_SAMPLES		7*430	//Max Fs=430Hz
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_60Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_95Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_185Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_229Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_430Hz	1
	#define	SI114xHRM_USE_DYNAMIC_DATA_STRUCTURE	1
#elif (SI114xHRM_BUILD_TARGET == SI114xHRM_BUILD_TARGET_GECKO)
	#define SI114X_SPO2				1
	#define	MAX_FRAME_SAMPLES		7*95	//Max Fs=95Hz
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_60Hz	0
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_95Hz	1  
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_185Hz	0
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_229Hz	0
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_430Hz	0
	#define	SI114xHRM_USE_DYNAMIC_DATA_STRUCTURE	0
#else
	#define SI114X_SPO2				1
	#define	MAX_FRAME_SAMPLES		7*430	//Max Fs=430Hz
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_60Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_95Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_185Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_229Hz	1
	#define	SI114xHRM_ENABLE_MEASUREMENT_RATE_430Hz	1
	#define	SI114xHRM_USE_DYNAMIC_DATA_STRUCTURE	1
#endif

#define SPO2_REFLECTIVE_MODE	1	//1--REFLECTIVE MODE, 0--TRANSMISSIVE MODE
#define HRM_PS_AGC				1 	//1--Increase LED current during valid HR if PS is below the work level or PI is below 0.2%(could occur on wrist).
#define HRM_DATA_LOG			0	//Debugging: 1--Dump out test data, such as PS, the normalized PS1 and BPF(PS1).

#define HRM_PS_AGC_DISABLED			0x00
#define HRM_PS_AGC_ENABLED			0x01
#define HRM_PS_AGC_GAIN_CHANGED		0x02
#define HRM_PS_AGC_NO_TOUCH			0x04

#define	SI114xHRM_MAX_INTERPOLATION_FACTOR		9

typedef struct Si114xhrmDebug
{						//ToDo:  This maybe should be moved elsewhere since it is Windows only
	int32_t enableUdp;
	int32_t udpPort;
	int32_t enableFile;
	int8_t* filename;
	Si114xhrmConfiguration_t *configuration;
} Si114xhrmDebug_t;




#ifdef __cplusplus
}
#endif

#endif		//SI114XHRM_CFG_H__
