/** @file sihrm.h
 *  @brief Main HRM header file for sihrm library.
 *******************************************************************************
 * @section License
 * <b>(C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *  This software is confidential and proprietary information owned by Silicon 
 *  Labs which is subject to worldwide copyright protection.  No use or other 
 *  rights associated with this software may exist other than as expressly 
 *  stated by written agreement with Silicon Labs.  No licenses to the 
 *  intellectual property rights included in the software, including patent and 
 *  trade secret rights, are otherwise given by implication, estoppel or otherwise.
 *  This contains the prototypes and datatypes needed
 *  to use the si114xhrm library.
 */


#ifndef SIHRM_H__

#define	SIHRM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define SIHRM_DEMO_VERSION "2.2.2"

/** 
 * Success return value
 */
#define	SIHRM_SUCCESS							0
#define	SIHRM_ERROR_EVB_NOT_FOUND				-1
#define	SIHRM_ERROR_INVALID_MEASUREMENT_RATE	-2
#define	SIHRM_ERROR_SAMPLE_QUEUE_EMPTY			-3
#define	SIHRM_ERROR_INVALID_PART_ID				-4
#define	SIHRM_ERROR_FUNCTION_NOT_SUPPORTED      -5
#define SIHRM_ERROR_BAD_POINTER                 -6
#define SIHRM_ERROR_DEBUG_DISABLED              -7
#define SIHRM_ERROR_NON_SUPPORTED_PART_ID       -8    //The feature is not support by the part
#define SIHRM_ERROR_PARAM0_OUT_OF_RANGE			-9
#define SIHRM_ERROR_PARAM1_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 1)
#define SIHRM_ERROR_PARAM2_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 2)
#define SIHRM_ERROR_PARAM3_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 3)
#define SIHRM_ERROR_PARAM4_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 4)
#define SIHRM_ERROR_PARAM5_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 5)
#define SIHRM_ERROR_PARAM6_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 6)
#define SIHRM_ERROR_PARAM7_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 7)
#define SIHRM_ERROR_PARAM8_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 8)
#define SIHRM_ERROR_PARAM9_OUT_OF_RANGE			(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 9)
#define SIHRM_ERROR_PARAM10_OUT_OF_RANGE		(SIHRM_ERROR_PARAM0_OUT_OF_RANGE - 10)



// The HRM status values are a bit field. More than one status can be 'on' at any given time.
#define SIHRM_STATUS_SKIN_CONTACT_OFF			(1<<0)
#define SIHRM_STATUS_RESERVED1					(1<<1)
#define SIHRM_STATUS_RESERVED2           		(1<<2)
#define SIHRM_STATUS_RESERVED3          		(1<<3)
#define SIHRM_STATUS_INTERPOLATION_NEXT_SAMPLE  (1<<4)	
#define SIHRM_STATUS_FRAME_PROCESS_NEXT_SAMPLE  (1<<5)	
#define SIHRM_STATUS_FRAME_PROCESSED			(1<<6)	
#define SIHRM_STATUS_RESERVED07      		    (1<<7)
#define SIHRM_STATUS_RESERVED08   		        (1<<8)
#define SIHRM_STATUS_RESERVED09		            (1<<9)
#define SIHRM_STATUS_RESERVED10          	    (1<<10)
#define SIHRM_STATUS_RESERVED11     			(1<<11)
#define SIHRM_STATUS_RESERVED12  			    (1<<12)
#define SIHRM_STATUS_RESERVED13  			    (1<<13)
#define SIHRM_STATUS_INTERPOLATED_HR			(1<<14)
#define SIHRM_STATUS_PS_DC_SENSE				(1<<15)	
#define SIHRM_STATUS_HIGHEST_BIT				15      



/* float type used in hrm algorithm  */
typedef float sihrmFloat_t;

typedef void *HANDLE;

/** 
 * HRM handle type
 */
typedef void * SihrmHandle_t; 

typedef struct SihrmConfiguration
{
	int16_t hrmPsSelect;  /**< bits 15:4 = unused, bits 3:0=HRM PS.  0=PS1, 1=PS2, 2=PS3. */
	int16_t ledCurrent;	      /**< bits 15:12 = unused, bits 11:8=led3 current, bits 7:4=led2 current, bits 3:0=led1 current */
    int16_t tasklist;
	int16_t psLedSelect;
    int16_t measurementRate;   /**< Always compressed. The algorithm will read uv_part from the chip and then program the chip with correct measurementRate accordingly.*/
	int16_t adcGain;				//Bit00~03: adcGain1. Bit04~07: adcGain2. Bit08~12: adcGain3. 
    int16_t adcMisc;
	int16_t adcMux;					//bits 15:12 = unused, bits 11:8=ps3, bits 7:4=ps2, bits 3:0=ps1
    int16_t psAlign;
    uint32_t timestampPeriod100ns;		//The timestamp period in units of 100 nanoseconds
	uint32_t accelerometerNormalizationFactorx10;   //The factor used to normalize the accelerometer data. This can be determined by averaging the accelerometer sum (sqrt(x^2 + y^2  z^2)) when the accelerometer is idle.  This is the 1G average. 
} SihrmConfiguration_t;


typedef struct SihrmData		
{
	int16_t Fs;
	sihrmFloat_t stepRate;
	uint32_t hrmAdcgainCurrent;	//HRM AGC: ((ADCgain2<<8+ADCgain1<<4+ADCgain)<<16)+current[2]<<8+current[1]<<4+current[0]
	uint16_t hrmRawPpg;
	uint16_t hrmRawPpg2,hrmRawPpg3;
	int16_t hrUpdateInterval;
	uint16_t hrmPerfusionIndex;
	uint16_t hrmHRQ;
} SihrmData_t;



// -----------------------------------------------------------------------------
// Interrupt Sample
//
typedef struct SihrmIrqSample
{
    uint16_t  sequence;       // sequence number
    uint16_t  timestamp;      // 16-bit Timestamp to record
    uint8_t   pad;
	uint8_t irqstat;        // 8-bit irq status
    uint16_t  vis;            // VIS
    uint16_t  ir;             // IR
    uint16_t  ps1;            // PS1
    uint16_t  ps2;            // PS2
    uint16_t  ps3;            // PS3
    uint16_t  aux;            // AUX
	int16_t accelerometer_x;			//Accelerometer X
	int16_t accelerometer_y;			//Accelerometer Y
	int16_t accelerometer_z;			//Accelerometer Z
} SihrmIrqSample_t;
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// Structure used for debug purposes in MS Windows platforms

typedef struct SihrmDebug
{						
	int32_t enableUdp;			
	int32_t udpPort;
	int32_t enableFile;
	int8_t *filename;
	SihrmConfiguration_t *configuration;
} SihrmDebug_t;
// -----------------------------------------------------------------------------



/** 
 * Number of bytes needed in RAM for HRM
 */
#define SIHRM_HRM_DATA_SIZE 3904


/** 
 * Struct for passing allocated RAM locations to HRM library
 */
typedef struct SihrmDataStorage
{
  uint8_t hrm[SIHRM_HRM_DATA_SIZE];  /**< HRM_DATA_SIZE bytes allocated */

} SihrmDataStorage_t;



/**************************************************************************//**
 * @brief
 *	Initialize the optical sensor device and the HRM algorithm
 *
 * @param[in] portName
 *	Platform specific data to specify the i2c port information.
 *
 * @param[in] options
 *	Initialization options flags.
 *
 * @param[in] handle
 *	Pointer to sihrm handle
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_Initialize(void *portName, int32_t options, SihrmHandle_t *handle);

/**************************************************************************//**
 * @brief
 *	Close the optical sensor device and algorithm
 *
 * @param[in] handle
 *	Pointer to sihrm handle
 *
 * @return
 *	Returns error status
 *****************************************************************************/
int32_t sihrm_Close(SihrmHandle_t handle);

/**************************************************************************//**
 * @brief
 *	Configure sihrm debugging mode.
 *
 * @param[in] handle
 *	Pointer to sihrm handle
 *
 * @param[in] enable
 *	Enable or Disable debug
 *
 * @param[in] debug
 *	Pointer to debug status
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_SetupDebug(SihrmHandle_t handle, int32_t enable, void *debug);

/**************************************************************************//**
 * @brief
 *	Find EVB (windows demo only)
 *
 * @param[in] portDescription
 *	Pointer to a string containing the port description
 *
 * @param[in] lastPort
 *	Pointer to a string containing the port description of the last port
 *  found by this function.  This is used when calling sihrm_FindEvb() 
 *  multiple times to find more ports
 *
 * @param[out] numPortsFound
 *	The number of ports found by sihrm_FindEvb
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_FindEvb(int8_t *portDescription, int8_t *lastPort, int32_t *numPortsFound);		//ToDo:  This is depracated.  We can probably remove this

/**************************************************************************//**
 * @brief
 *	Output debug message
 *
 * @param[in] handle
 *	Pointer to sihrm handle
 *
 * @param[in] message
 *	Message data
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_OutputDebugMessage(SihrmHandle_t handle, int8_t * message);

/**************************************************************************//**
 * @brief
 *	Configure device and algorithm
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @param[in] configuration
 *	Pointer to a configuration structure of type SihrmConfiguration_t
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_Configure(SihrmHandle_t handle, SihrmConfiguration_t *configuration);

/**************************************************************************//**
 * @brief
 *	Query the algorithm power mode setting.
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @param[out] mode
 *	Pointer to a location where the power mode will be returned
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_GetPowerMode(SihrmHandle_t _handle, uint8_t *mode);

/**************************************************************************//**
 * @brief
 *	Set the algorithm power mode setting.
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @param[in] mode
 *	The power mode.  0 = low power, 1 = performance mode
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_SetPowerMode(SihrmHandle_t _handle, uint8_t mode);

/**************************************************************************//**
 * @brief
 *	HRM process engine.  This function should be called at least once per sample
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @param[out] heartRate
 *	Pointer to a location where this function will return the heart rate result
 *
 * @param[out] spo2
 *	Pointer to a location where this function will return the spo2 result.  
 *  Note that spo2 is currently not supported.
 *
 * @param[out] hrmStatus
 *	Pointer to a integer where this function will report status flags
 *
 * @param[out] hrmData
 *	Optional pointer to a SihrmData_t structure where this function will return 
 *  auxiliary data useful for the application.  If the application is not 
 *  interested in this data it may pass NULL to this parameter.
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_Process(SihrmHandle_t handle, int16_t *heartRate, int16_t *spo2, int32_t *hrmStatus, SihrmData_t *hrmData);

/**************************************************************************//**
 * @brief
 *	Start the device's autonomous measurement operation.
 *  The device must be configured before calling this function.
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @param[in] reset
 *	Reset the internal parameters of the HRM algorithm.  If set to false the
 *	HRM algorithm will begin running using parameter values from the last time
 *	it was run.  If the users heart rate has changed significantly since the
 *	last time the algorithm has run and reset is set to false, it could take
 *	longer to converge on the correct heart rate.  It is recommended to set
 *	reset to true if the HRM algorithm has been stopped for greater than 15s.
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_Run(SihrmHandle_t _handle, int8_t reset);

/**************************************************************************//**
 * @brief
 *	Pause the device's autonomous measurement operation.
 *  HRM must be running before calling this function.
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_Pause(SihrmHandle_t handle);

/**************************************************************************//**
 * @brief
 *	Check for skin contact
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @param[out] isContact
 *	Set to non-zero value if skin contact is found
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_DetectSkinContact(SihrmHandle_t handle, int32_t *isContact);


/**************************************************************************//**
 * @brief
 *	Returns algorithm version
 *
 * @param[out] revision
 *	String representing the sihrm library version.  The version string has
 *  a maximum size of 32 bytes.
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_QuerySoftwareRevision(int8_t *revision);

/**************************************************************************//**
 * @brief
 *	Get low level i2c handle
 *
 * @param[in] _handle
 *	SiHRM handle
 *
 * @param[out] deviceHandle
 *	Low level i2c handle
 *
 * @return
 *	Returns error status.
 *****************************************************************************/
int32_t sihrm_GetLowLevelHandle(SihrmHandle_t _handle, HANDLE *deviceHandle);


#ifdef __cplusplus
}
#endif

#endif		//SIHRM_H__
