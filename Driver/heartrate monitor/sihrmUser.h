/**************************************************************************//**
 * @brief Implementation specific functions for HRM code
 * @version 3.20.3
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
//#include "em_gpio.h"		//required for GPIO_Port_TypeDef
#include "lib/si114x/dynamic/sihrm.h"

#ifndef SIHRMUSER_H
#define SIHRMUSER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "../i2c.h"

typedef void *            sihrmUserHandle_t;

extern void delay(uint32_t dlyTicks);
void sihrmUser_setAccelData (int16_t *accel);


int32_t sihrmUser_ProcessIrq(sihrmUserHandle_t sihrmUser_handle, uint16_t timestamp);
int32_t sihrmUser_SetupDebug(sihrmUserHandle_t sihrmUser_handle, int32_t enable, void *debug);
int32_t sihrmUser_OutputDebugMessage(sihrmUserHandle_t sihrmUser_handle, const uint8_t *message);
int32_t sihrmUser_OutputSampleDebugMessage(sihrmUserHandle_t sihrmUser_handle, SihrmIrqSample_t *sample);
int32_t sihrmUser_WriteToRegister(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t data);
int32_t sihrmUser_ReadFromRegister(sihrmUserHandle_t sihrmUser_handle, uint8_t address);
int32_t sihrmUser_BlockWrite(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t const *values);
int32_t sihrmUser_BlockRead(sihrmUserHandle_t sihrmUser_handle, uint8_t address, uint8_t length, uint8_t *values);
int32_t sihrmUser_ProcessIrq(sihrmUserHandle_t sihrmUser_handle, uint16_t timestamp);
int32_t sihrmUser_SampleQueueNumentries(sihrmUserHandle_t sihrmUser_handle);
int32_t sihrmUser_SampleQueue_Get(sihrmUserHandle_t sihrmUser_handle, SihrmIrqSample_t *samples);
int32_t sihrmUser_SampleQueue_Clear(sihrmUserHandle_t sihrmUser_handle);
int32_t sihrmUser_Initialize(void *port, int16_t options, sihrmUserHandle_t *sihrmUser_handle);
int32_t sihrmUser_Close(sihrmUserHandle_t sihrmUser_handle);

#ifdef __cplusplus
}
#endif

#endif   //SIHRMUSER_H
