/**************************************************************************//**
 * @file
 * @brief Heart Rate state machine
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2015 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

//#include "sihrmUser.h"
#include "si114x_functions.h"
#include "heartrate_monitor.h"
#include "lib/si114x/dynamic/sihrm.h"
#include "lib/si114x/static/si114xhrm.h"

// Timeout values
#define SKIN_CONTACT_TIMEOUT  16384     //LETIMER runs as 8192Hz...this value is 2 seconds of that timer

// Biometric EXP Si1146 config
static SihrmConfiguration_t dynamic_biometricEXPHRMConfig = {
  0x100,  // ps_select
  0x447,   // current_led
  0x83,   // tasklist
  0x421,  // led_select
  0xa4,   // measrate
  2,      // adcgain
  0x4,   // adcmisc
  0x0333, // adcmux
  0,      // ps_align
  1221,   // The timestamp period in 100 nanoseconds - 8192Hz
  2300    // accelerometerNormalizationFactorx10
};

//static SihrmConfiguration_t dynamic_biometricEXPHRMConfig = {
//  0x100,  // ps_select
//  0x42,   // current_led
//  0x83,   // tasklist
//  0x421,  // led_select
//  0xa4,   // measrate
//  1,      // adcgain
//  0x4,   // adcmisc
//  0x0333, // adcmux
//  0,      // ps_align
//  1221,   // The timestamp period in 100 nanoseconds - 8192Hz
//  2300    // accelerometerNormalizationFactorx10
//};

// Fitness EVB with GGG LEDs config
static SihrmConfiguration_t dynamic_fitnessGGGHRMConfig = {
		0x000,	//ps_select
		0x0003,	//ledCurrent
		0x1,		//tasklist
		0x2003,	//psLedSelect
		0xa4,     //measrate
		2,	    //adcgain
		0x04,     //adcmisc
		0x333,    //adcmux
		0,        //ps_align
		1221,     //The timestamp period in 100 nanoseconds - 8192Hz
		2300      //accelerometerNormalizationFactorx10
};
//static SihrmConfiguration_t dynamic_fitnessGGGHRMConfig = {
//  0x210,	//ps_select
//  0x0442,	//ledCurrent
//  0x7,		//tasklist
//  0x0421,	//psLedSelect
//  0xa4,     //measrate
//  1,	    //adcgain
//  0x04,     //adcmisc
//  0x333,    //adcmux
//  0,        //ps_align
//  1221,     //The timestamp period in 100 nanoseconds - 8192Hz
//  2300      //accelerometerNormalizationFactorx10
//};

// Store the timer value for use to calculate time since last skin contact check
static int32_t dynamic_skinContactTimer = 0;
static int32_t dynamic_idle_count = 0;
static SihrmDataStorage_t dynamicHrmDataStorage;
static SihrmConfiguration_t *dynamicCurrentHRMConfig;
static SihrmHandle_t dynamicHrmHandle = 0;
static HRMSpO2State_t dynamicState = HRM_STATE_IDLE; // stores the main state of the state machine
static int32_t dynamicHrmStatus = 0;
static bool dynamicSkinContact;
// Values stored for LCD display
static int16_t dynamicHeartRateValue = 0;
static int16_t dynamicStepRateValue = 0;
static int16_t dynamicPi = 0;

// Error flag indicates sample buffer full
static volatile bool dynamicBufferOverrunError = false;

static int32_t static_skinContactTimer = 0;
//static int32_t static_idle_count = 0;
static Si114xhrmDataStorage_t staticHrmData;
static Si114xSpO2DataStorage_t staticSpo2Data;
static Si114xDataStorage_t staticDataStorage;
static Si114xhrmConfiguration_t *staticCurrentHRMConfig;
static Si114xhrmHandle_t staticHrmHandle = 0;
static HRMSpO2State_t staticState = HRM_STATE_IDLE; // stores the main state of the state machine
static int32_t staticHrmStatus = 0;
static bool staticSkinContact;
// Values stored for LCD display
static int16_t staticHeartRateValue = 0;
static int16_t staticSpo2Value = 0;

// Error flag indicates sample buffer full
static volatile bool staticBufferOverrunError = false;

// Biometric EXP Si1146 config
static Si114xhrmConfiguration_t static_biometricEXPHRMConfig = {
  0x210,  // ps_select
  0x547,   // current_led
  0x7,   // tasklist
  0x421,  // led_select
  0xa4,   // measrate
  2,      // adcgain
  0x4,   // adcmisc
  0x0333, // adcmux
  0,       // ps_align
  7326,     //timestampClockFreq
};

//static Si114xhrmConfiguration_t static_biometricEXPHRMConfig = {
//  0x210,  // ps_select
//  0x442,   // current_led
//  0x7,   // tasklist
//  0x421,  // led_select
//  0xa4,   // measrate
//  1,      // adcgain
//  0x4,   // adcmisc
//  0x0333, // adcmux
//  0,       // ps_align
//  7326,     //timestampClockFreq
//};

// Biometric EXP Si1146 config
static Si114xhrmConfiguration_t static_biometricConfig = {
  0x120,  // ps_select
  0x442,   // current_led
  0x7,   // tasklist
  0x421,  // led_select
  0xa4,   // measrate
  1,      // adcgain
  0x4,   // adcmisc
  0x0333, // adcmux
  0,       // ps_align
  7326,     //timestampClockFreq
};

/**************************************************************************//**
 * Local prototypes
 *****************************************************************************/
static void HeartRateMonitor_StartTimestampCounter (void);

/**************************************************************************//**
 * @brief Stop HRM Processing and interrupts
 *****************************************************************************/
static void stopHRM (bool dynamicLibUsed)
{
	if(dynamicLibUsed)
	{
		sihrm_Pause(dynamicHrmHandle);
		//sihrm_Close(dynamicHrmHandle);
		//dynamicHeartRateValue = 0;
	}
	else
	{
		si114xhrm_Pause(staticHrmHandle);
		//si114xhrm_Close(staticHrmHandle);
		//staticHeartRateValue = 0;
		//staticSpo2Value = 0;
	}
}

static bool dynamicLoop(void)
{
	int16_t heartRate;
	int32_t skinDetect;
	HANDLE sihrmHandle;
	sihrm_GetLowLevelHandle(dynamicHrmHandle, &sihrmHandle);
	static SihrmData_t hrmData; //Make static, so the data such as DC sensing data and PS AGC don't need be sent from HRM every second for the display.
	static SihrmData_t *hrmDataPtr = &hrmData;

	switch(dynamicState)
	{
		case HRM_STATE_IDLE:
			/*
			* in the idle state we periodically check for skin contact
			* and if detected start the algorithm
			*/
			dynamicSkinContact = false;
			sihrm_DetectSkinContact(dynamicHrmHandle, &skinDetect);
			if (skinDetect)
			{
				dynamicState = HRM_STATE_ACQUIRING;
				// start the algorithm
				//Skin contact is checked every ~2 seconds in this application.  Therefore
				//idle_count > 7 is true when we have been idle for >14 seconds.
				sihrm_Run(dynamicHrmHandle, (dynamic_idle_count > 7));
				dynamic_idle_count=0;	//Reset the counter for the next idle.
				dynamicSkinContact = true;
			} //skinDetect
			else
				dynamic_idle_count++;
			break;
		case HRM_STATE_ACQUIRING:
		case HRM_STATE_ACTIVE:
			// call the main HRM algorithm processing function
			if(sihrm_Process(dynamicHrmHandle, &heartRate, 0, &dynamicHrmStatus, hrmDataPtr) == SIHRM_SUCCESS)
			{
				if (dynamicHrmStatus & SIHRM_STATUS_PS_DC_SENSE)  //a DC Sense Occurred
					dynamicHrmStatus &= ~SIHRM_STATUS_PS_DC_SENSE; // Clear the bit.

				if (dynamicHrmStatus&(SIHRM_STATUS_FRAME_PROCESSED | SIHRM_STATUS_INTERPOLATED_HR))  //We have a result either from processing a frame or an interpolated result
				{
					dynamicHrmStatus &= ~(SIHRM_STATUS_FRAME_PROCESSED | SIHRM_STATUS_INTERPOLATED_HR);  //Clear the status bit

					dynamicState = HRM_STATE_ACTIVE;

					// save the value
					dynamicHeartRateValue = (heartRate+5)/10; //+5 is the round-off
					dynamicStepRateValue = hrmData.stepRate;
					dynamicPi = hrmData.hrmPerfusionIndex;
				}

				if (dynamicHrmStatus & SIHRM_STATUS_SKIN_CONTACT_OFF)  //The algorithm thinks that skin contact has removed
				{
					if((dynamic_skinContactTimer - LETIMER_CounterGet(LETIMER0)) >= SKIN_CONTACT_TIMEOUT)		//wait SKIN_CONTACT_TIMEOUT seconds before going to idle
					{
						dynamicState = HRM_STATE_IDLE;
						stopHRM(true);
					}
					dynamicHrmStatus &= ~SIHRM_STATUS_SKIN_CONTACT_OFF; //Clear the bit
				}
				else
					dynamic_skinContactTimer = LETIMER_CounterGet(LETIMER0);
			}
			break;

		case HRM_STATE_INVALID:
		case HRM_STATE_NOSIGNAL:
			dynamicState = HRM_STATE_IDLE;
			stopHRM(true);
			break;
	}

	return ( dynamicState!=HRM_STATE_IDLE );
}

static bool staticLoop(void)
{
	int16_t heartRate;
	int16_t spo2Rate;
	int32_t skinDetect;
	HANDLE si114xHandle;
	si114xhrm_GetLowLevelHandle(staticHrmHandle, &si114xHandle);
	static Si114xhrmData_t hrmData; //Make static, so the data such as DC sensing data and PS AGC don't need be sent from HRM every second for the display.
	static Si114xhrmData_t *hrmDataPtr = &hrmData;

	switch(staticState)
	{
		case HRM_STATE_IDLE:
			/*
			* in the idle state we periodically check for skin contact
			* and if detected start the algorithm
			*/
			staticSkinContact = false;
			si114xhrm_DetectSkinContact(staticHrmHandle, &skinDetect);
			if (skinDetect)
			{
				staticState = HRM_STATE_ACQUIRING;
				// start the algorithm
				//Skin contact is checked every ~2 seconds in this application.  Therefore
				//idle_count > 7 is true when we have been idle for >14 seconds.

	//			if(static_idle_count > 7)
	//			{
					si114xhrm_Run(staticHrmHandle);
	//				static_idle_count=0;	//Reset the counter for the next idle.
	//			}
				staticSkinContact = true;
			} //skinDetect
	//		else
	//			static_idle_count++;
			break;
		case HRM_STATE_ACQUIRING:
		case HRM_STATE_ACTIVE:
			// call the main HRM algorithm processing function
			if(si114xhrm_Process(staticHrmHandle, &heartRate, &spo2Rate, &staticHrmStatus, hrmDataPtr) == SI114xHRM_SUCCESS)
			{
				if (staticHrmStatus & SI114xHRM_STATUS_FRAME_PROCESSED)  //We have a result either from processing a frame or an interpolated result
				{
					staticHrmStatus &= ~SI114xHRM_STATUS_FRAME_PROCESSED;  //Clear the status bit

					staticState = HRM_STATE_ACTIVE;
					if(!(staticHrmStatus & SI114xHRM_STATUS_SPO2_CREST_FACTOR_OFF) &&
						!(staticHrmStatus & SI114xHRM_STATUS_SPO2_TOO_LOW_AC) &&
						!(staticHrmStatus & SI114xHRM_STATUS_SPO2_TOO_HIGH_AC)
							) {
						//staticHeartRateValue = (heartRate + 5)/10; //+5 is the round-off
						//staticSpo2Value = (spo2Rate + 5)/10; //+5 is the round-off
						staticHeartRateValue = heartRate;
						staticSpo2Value = spo2Rate;
						}
				}

				if((staticHrmStatus & SI114xHRM_STATUS_FINGER_OFF) || (staticHrmStatus & SI114xHRM_STATUS_SPO2_FINGER_OFF)) //The algorithm thinks that skin contact has removed
				{
					if((static_skinContactTimer - LETIMER_CounterGet(LETIMER0)) >= SKIN_CONTACT_TIMEOUT)		//wait SKIN_CONTACT_TIMEOUT seconds before going to idle
					{
						staticState = HRM_STATE_IDLE;
						stopHRM(false);
						staticHrmStatus &= ~SI114xHRM_STATUS_FINGER_OFF & ~SI114xHRM_STATUS_SPO2_FINGER_OFF; //Clear the bit
					}
				}
				else
					static_skinContactTimer = LETIMER_CounterGet(LETIMER0);
			}
			break;

		case HRM_STATE_INVALID:
		case HRM_STATE_NOSIGNAL:
			staticState = HRM_STATE_IDLE;
			stopHRM(false);
			break;
	}

	return ( staticState!=HRM_STATE_IDLE );
}

/**************************************************************************//**
 * @brief This function returns the algorithm version string.
 *****************************************************************************/
void HeartRateMonitor_GetVersion (bool dynamicLibUsed, char *hrmVersion)
{
	if(dynamicLibUsed)
		sihrm_QuerySoftwareRevision((int8_t *)hrmVersion);
	else
		si114xhrm_QuerySoftwareRevision((int8_t *)hrmVersion);
}

/**************************************************************************//**
 * @brief This function enables the 100us timer for HRM.
 *****************************************************************************/
static void HeartRateMonitor_StartTimestampCounter (void)
{
  CMU_ClockSelectSet(cmuClock_LFA,cmuSelect_LFXO);
  CMU_ClockEnable(cmuClock_LETIMER0, true);
  CMU_ClockDivSet(cmuClock_LETIMER0, cmuClkDiv_4);			// 32,768 Hz / 4 = 8192 Hz = 122.0703 us period
  CMU_ClockEnable(cmuClock_CORELE, true);

  LETIMER_Init_TypeDef letimerInit = LETIMER_INIT_DEFAULT;
  LETIMER_Init(LETIMER0, &letimerInit);
}

/**************************************************************************//**
 * @brief Initiate the Heart Rate Monitor
 *****************************************************************************/
void HeartRateMonitor_Init( Si114xPortConfig_t* i2c, bool dynamicLibUsed, HeartRateMonitorDynamic_Config_t dynamicConfigSelect, HeartRateMonitorStatic_Config_t staticConfigSelect, int debugEnabled )
{
	// Setup Timestamp counter
	HeartRateMonitor_StartTimestampCounter ();

	if(dynamicLibUsed)
	{
		//allocate memory block for hrm
		dynamicHrmHandle = &dynamicHrmDataStorage;

		sihrm_Initialize(i2c, 0, &dynamicHrmHandle);
		switch(dynamicConfigSelect)
		{
			case DYNAMIC_BIOMETRIC_EXP:
				dynamicCurrentHRMConfig = &dynamic_biometricEXPHRMConfig;
				break;
			case DYNAMIC_FITNESS_EVB_GGG:
				dynamicCurrentHRMConfig = &dynamic_fitnessGGGHRMConfig;
				break;
			default:
				dynamicCurrentHRMConfig = &dynamic_biometricEXPHRMConfig;
				break;
		}
		dynamicCurrentHRMConfig = &dynamic_biometricEXPHRMConfig;
		//dynamicCurrentHRMConfig = &dynamic_fitnessGGGHRMConfig;
		sihrm_Configure(dynamicHrmHandle, dynamicCurrentHRMConfig); //This initializes the HRM buffers and variables.
		sihrm_SetupDebug(dynamicHrmHandle, debugEnabled, 0);
		dynamicBufferOverrunError = false;
		//dynamicHeartRateValue = 0;
		dynamicStepRateValue = 0;
	}
	else
	{
		staticDataStorage.spo2 = &staticSpo2Data;
		staticDataStorage.hrm = &staticHrmData;
		staticHrmHandle = &staticDataStorage;

		si114xhrm_Initialize(i2c, 0, &staticHrmHandle);
		switch(staticConfigSelect)
		{
			case STATIC_BIOMETRIC_EXP:
				staticCurrentHRMConfig = &static_biometricEXPHRMConfig;
				break;
			default:
				staticCurrentHRMConfig = &static_biometricEXPHRMConfig;
				break;
		}
		//staticCurrentHRMConfig = &static_biometricConfig;
		si114xhrm_Configure(staticHrmHandle, staticCurrentHRMConfig);
		//sihrm_SetupDebug(staticHrmHandle, debugEnabled, 0);

		staticBufferOverrunError = false;
		//staticHeartRateValue = 0;
		//staticSpo2Value = 0;
	}
}

/**************************************************************************//**
 * @brief Check for samples in irq queue
 *****************************************************************************/
bool HeartRateMonitor_SamplesPending (bool dynamicLibUsed)
{
  HANDLE si114xHandle;

  if(dynamicLibUsed)
  {
	  sihrm_GetLowLevelHandle(dynamicHrmHandle, &si114xHandle);
	  return (sihrmUser_SampleQueueNumentries(si114xHandle)>0);
  }
  else
  {
	  si114xhrm_GetLowLevelHandle(staticHrmHandle, &si114xHandle);
	  return (Si114xIrqQueueNumentries(si114xHandle)>0);
  }
}

/**************************************************************************//**
 * @brief The Heart Rate Monitor loop
 *        It implements the main state machine.
 *****************************************************************************/
bool HeartRateMonitor_Loop(bool dynamicLibUsed)
{
	if(dynamicLibUsed)
		return dynamicLoop();
	else
		return staticLoop();
}

void HeartRateMonitor_GetData(bool dynamicLibUsed, bool *skinContact, int16_t *pulse, int16_t *spo2)
{
	if(dynamicLibUsed)
	{
		*skinContact = dynamicSkinContact;
		*pulse = dynamicHeartRateValue;
		*spo2 = staticSpo2Value;
	}
	else
	{
		*skinContact = staticSkinContact;
		*pulse = dynamicHeartRateValue;
		*spo2 = staticSpo2Value;
	}
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (Si114x INT)
 *****************************************************************************/
void HeartRateMonitor_Interrupt (bool dynamicLibUsed)
{
  int16_t error;
  HANDLE si114xHandle;

  if(dynamicLibUsed)
	  sihrm_GetLowLevelHandle(dynamicHrmHandle, &si114xHandle);
  else
	  si114xhrm_GetLowLevelHandle(staticHrmHandle, &si114xHandle);

  error = sihrmUser_ProcessIrq(si114xHandle, ~LETIMER_CounterGet(LETIMER0));		//LETIMER is a countdown timer.  The SiHRM requires a countup timer.  Therefore we complement the count
  if (error != 0)
  {
	  if(dynamicLibUsed)
		  dynamicBufferOverrunError = true;
	  else
		  staticBufferOverrunError = true;
  }
}

/**************************************************************************//**
 * @brief Function to stop HRM completely
 *****************************************************************************/
void HeartRateMonitor_Stop(bool dynamicLibUsed)
{
	if(dynamicLibUsed)
		dynamicState = HRM_STATE_IDLE;
	else
		staticState = HRM_STATE_IDLE;

	stopHRM(dynamicLibUsed);
}
