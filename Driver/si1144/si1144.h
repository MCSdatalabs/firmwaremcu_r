/**
 * @file			si1144.h
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef SI1144_H_
#define SI1144_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "../i2c.h"
#include "rtcdriver.h"
#include "gpiointerrupt.h"
#include "../heartrate monitor/heartrate_monitor.h"

#define SI1144_INT_PIN                  (14)
#define SI1144_INT_PORT                 (gpioPortC)

#define MAX17222_EN_PIN               	(7)
#define MAX17222_EN_PORT              	(gpioPortD)

#define Si1144_ADDRESS	0xB4	//!< SI1144 I2C address

/**
 * @brief Return codes for the SI1144.
 */
typedef enum {
	SI1144_SUCCESS = 0,			//!< Transfer completed successfully.
	SI1144_FAIL = -1,          	//!< SI1144 error.
	SI1144_I2C_ERR = -2,       	//!< I2C error.
	SI1144_NO_INIT = -99       	//!< SI1144 not initialized.
}SI1144_RETURN;

/**
 * @brief Checks if a SI1144 is connected.
 *
 * @return SI1144_RETURN
 * @retval SI1144_SUCCESS			Transfer completed successfully
 * @retval SI1144_FAIL				SI1144 error
 * @retval SI1144_I2C_ERR			I2C communication error
 */
SI1144_RETURN si1144_detect(void);

/**
 * @brief Initialize the SI1144.
 *
 * This function initializes the communication port for the SI1144 and it also
 * initializes the GPIO Interrupt (PORT: gpioPortC; PIN: 14).
 *
 * @attention If the SpO2 measurement is enabled an older driver (the static si114x driver) is used and
 * no motion detection is used for the HRM measurement.
 *
 * @attention If the SpO2 measurment is disabled the new driver (the dynamic si114x driver) is used and
 * the motion detection is used for the HRM measurement. The result is a better result for the HRM
 * measurement.
 *
 * @param spo2MeasurementEnable True runs the HRM and SpO2 measurement. False runs only the HRM measurement.
 *
 * @return SI1144_RETURN
 * @retval SI1144_SUCCESS			Transfer completed successfully
 */
SI1144_RETURN si1144_init(bool spo2MeasurementEnable);

/**
 * @brief Starts the measurement.
 *
 * @return SI1144_RETURN
 * @retval SI1144_SUCCESS			Transfer completed successfully
 * @retval SI1144_NO_INIT			SI1144 not initialized
 */
SI1144_RETURN si1144_start(void);

/**
 * @brief Stops the measurement
 *
 * @return SI1144_RETURN
 * @retval SI1144_SUCCESS			Transfer completed successfully
 * @retval SI1144_NO_INIT			SI1144 not initialized
 */
SI1144_RETURN si1144_stop(void);

SI1144_RETURN si1144_disable(void);

void si1144_setAccelData (int16_t *accel);

/**
 * @brief Perform the measurement. Should be called regular in the SysTick_Eventhandler.
 *
 * @attention The functions si1144_init and si1144_start must be called before a measurement can be performed.
 *
 * @return SI1144_RETURN
 * @retval SI1144_SUCCESS			Transfer completed successfully
 * @retval SI1144_FAIL				SI1144 error
 * @retval SI1144_NO_INIT			SI1144 not initialized
 */
SI1144_RETURN si1144_measure(void);

/**
 * @brief Returns the measured values.
 *
 * @param pulse	Measured HRM value.
 * @param spo2	Measured SpO2 value.
 * @param hasSkinContact	Measured skin contact value.
 */
void si1144_getData(int16_t *pulse, int16_t *spo2, bool *hasSkinContact);

/**
 * @brief This function must be called from the GPIO interrupt handler.
 */
void si1144_interrupt(void);

#endif /* SI1144_H_ */
