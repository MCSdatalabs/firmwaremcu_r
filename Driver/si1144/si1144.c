/**
 * @file			si1144.c
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "si1144.h"

/******************** defines *****************************/
#define Si1144_GET_PART_ID			0x00
#define Si1144_PART_ID				0x44

/* Time (in ms) between skin detections */
#define Si1144_SKIN_DETECT_UPDATE_MS     2000

/******************** prototypes **************************/
static void si1144_skinDetectUpdateCallback(RTCDRV_TimerID_t id, void *user);
static void si1144_loop();

/******************** struct data type ********************/

/******************** private variables********************/
static bool initialized = false;
static bool si1144_running = false;
static int16_t si1144_pulse;
static int16_t si1144_spo2;
static bool si1144_skinContact;

/** This flag tracks if we need to perform a new measurement. */
static volatile bool si1144_updateMeasurement = true;

static HeartRateConfig_t si1144_config;
static volatile bool si1144_newSample = false;

/** HRM hardware configuration */
static HeartRateMonitorDynamic_Config_t hrmConfig = DYNAMIC_FITNESS_EVB_GGG;

/** Timer used for periodic update of the measurements. */
RTCDRV_TimerID_t     si1144SkinDetectUpdateTimerId;

/******************** private function ********************/
void si1144InterruptCallback(uint8_t pin)
{
	if(!initialized)
		return;

	//HeartRateMonitor_Interrupt(si1144_config.dynamicLibUsed);
	si1144_newSample = true;
}

void si1144_skinDetectUpdateCallback(RTCDRV_TimerID_t id, void *user)
{
	(void) id;
	(void) user;

	si1144_updateMeasurement = true;
}

void si1144_loop(void)
{
	if (si1144_newSample) {
		si1144_newSample = false;
		HeartRateMonitor_Interrupt(si1144_config.dynamicLibUsed);
	}
	if(si1144_updateMeasurement)
	{
		si1144_updateMeasurement = false;
		si1144_config.hrmActive = HeartRateMonitor_Loop(si1144_config.dynamicLibUsed);
	}
	else
		if(si1144_config.hrmCheckSkinContact)
			si1144_config.hrmActive = HeartRateMonitor_Loop(si1144_config.dynamicLibUsed);

	HeartRateMonitor_GetData(si1144_config.dynamicLibUsed, &si1144_config.hrmCheckSkinContact, &si1144_config.hrmPulse, &si1144_config.hrmSpo2);
}

/******************** global function *********************/
SI1144_RETURN si1144_detect(void)
{
	uint8_t i2c_write_data[2];
	uint8_t i2c_read_data[1];

	i2c_write_data[0] = Si1144_GET_PART_ID;
	if(i2c_writeRead(Si1144_ADDRESS, i2c_write_data, 1, i2c_read_data, 1) != 0)
		return SI1144_I2C_ERR;

	if(i2c_read_data[0] != Si1144_PART_ID)
		return SI1144_FAIL;

	return SI1144_SUCCESS;
}

SI1144_RETURN si1144_init(bool spo2MeasurementEnable)
{
	if(si1144_running)
		si1144_stop();

	initialized = false;
	si1144_pulse = 0;
	si1144_spo2 = 0;
	si1144_skinContact = false;
	si1144_running = false;
	si1144_config.i2cPort.i2cAddress = Si1144_ADDRESS >> 1;
	si1144_config.i2cPort.i2cBusSelect = 0;
	si1144_config.i2cPort.irqPin = SI1144_INT_PIN;
	si1144_config.i2cPort.irqPort = SI1144_INT_PORT;

	if(spo2MeasurementEnable)
	{
		hrmConfig = DYNAMIC_BIOMETRIC_EXP;
		si1144_config.dynamicLibUsed = false;
	}
	else
	{
		hrmConfig = DYNAMIC_FITNESS_EVB_GGG;
		si1144_config.dynamicLibUsed = true;
	}
	si1144_config.hrmCheckSkinContact = false;
	si1144_config.hrmActive = false;
	si1144_config.hrmPulse = 0;
	si1144_config.hrmSpo2 = 0;

	si1144_updateMeasurement = true;

	/* Configure MAX17222_EN_PIN as push-pull. */
	GPIO_PinModeSet(MAX17222_EN_PORT, MAX17222_EN_PIN, gpioModePushPull, 0);

	/*si114x interrupt. we want this pin high while si114x starts up*/
	GPIO_PinModeSet(SI1144_INT_PORT, SI1144_INT_PIN, gpioModePushPull, 1);
	/* Configure PD5 as input and enable interrupt - si114x interrupt. */
	/* Interrupt is active low */
	GPIO_PinModeSet(SI1144_INT_PORT, SI1144_INT_PIN, gpioModeInputPull, 1);
	GPIO_IntConfig(SI1144_INT_PORT, SI1144_INT_PIN, false, true, true);
	GPIOINT_CallbackRegister(SI1144_INT_PIN, si1144InterruptCallback);

	RTCDRV_AllocateTimer(&si1144SkinDetectUpdateTimerId);

	/*  Get the version of the HRM algorithm  */
	HeartRateMonitor_GetVersion(si1144_config.dynamicLibUsed, si1144_config.hrmVersion);

	/* perform some initialization.*/
	int32_t error = sihrmUser_Initialize(&si1144_config.i2cPort,0,0);

	if (error) {
		return SI1144_FAIL;
	}

	initialized = true;
	return SI1144_SUCCESS;
}

SI1144_RETURN si1144_start(void)
{
	if(!initialized)
		return SI1144_NO_INIT;

	/*  Configure skin detection timer  */
	RTCDRV_StartTimer(si1144SkinDetectUpdateTimerId, rtcdrvTimerTypePeriodic,
			Si1144_SKIN_DETECT_UPDATE_MS, si1144_skinDetectUpdateCallback, NULL);

	/*  Initialize the HRM state machine  */
	HeartRateMonitor_Init(&si1144_config.i2cPort, si1144_config.dynamicLibUsed, hrmConfig, 0, false);

	GPIO_PinOutSet(MAX17222_EN_PORT, MAX17222_EN_PIN);

	si1144_running = true;
	return SI1144_SUCCESS;
}

SI1144_RETURN si1144_stop(void)
{
	if(!initialized)
		return SI1144_NO_INIT;

	if(!si1144_running)
		return SI1144_FAIL;

	si1144_skinContact = false;
	si1144_running = false;
	si1144_pulse = 0;
	si1144_spo2 = 0;

	GPIO_PinOutClear(MAX17222_EN_PORT, MAX17222_EN_PIN);
	HeartRateMonitor_Stop(si1144_config.dynamicLibUsed);
	RTCDRV_StopTimer(si1144SkinDetectUpdateTimerId);

	return SI1144_SUCCESS;
}
SI1144_RETURN si1144_disable(void)
{
	initialized = false;
	return SI1144_SUCCESS;
}

void si1144_setAccelData (int16_t *accel)
{
	sihrmUser_setAccelData (accel);
}

SI1144_RETURN si1144_measure(void)
{
	si1144_skinContact = false;
	si1144_pulse = 0;
	si1144_spo2 = 0;

	if(!initialized)
		return SI1144_NO_INIT;

	if(!si1144_running)
		return SI1144_FAIL;

	si1144_loop();
	si1144_skinContact = si1144_config.hrmCheckSkinContact;
	si1144_pulse = si1144_config.hrmPulse;
	si1144_spo2 = si1144_config.hrmSpo2;

	return SI1144_SUCCESS;
}

void si1144_getData(int16_t *pulse, int16_t *spo2, bool *hasSkinContact)
{
	*pulse = si1144_pulse;
	*spo2 = si1144_spo2;
	*hasSkinContact = si1144_skinContact;
}
