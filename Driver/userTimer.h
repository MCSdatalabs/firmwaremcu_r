/**
 * @file			userTimer.h
 * @date			23.03.2017
 *	
 * @details
 */

#ifndef USER_TIMER_H_
#define USER_TIMER_H_

#include <stdint.h>
#include <stdbool.h>

#include "em_cmu.h"
#include "em_emu.h"

/**
 * @brief Starts the a timer to generate a Tick on every millisecond.
 *
 * @param userHandler A callback for the SysTick_Handler to handle globale timings
 */
void sysTick_init(void (*userHandler)(void));

/**
 * @brief Returns the elapsed time in millisecnds.
 *
 * @return Currint ticks in milliseconds
 */
uint64_t time_InMs(void);

/**
 * @brief Waits for n milliseconds.
 *
 * @param dlyTicks Delay in ms
 */
void delay(uint32_t dlyTicks);

/**
 * @brief Sets a timeout.
 *
 * @param timoutTicks Timeout in ms
 */
void timeoutSet(uint32_t timoutTicks);

/**
 * @brief Clears the timeout
 */
void timeoutClear(void);

/**
 * @brief Checks if a timeout occurred.
 *
 * @return
 * @retval true A timeout occurred.
 * @retval false No timeout occurred.
 */
bool timeoutGet(void);

#endif /* USER_TIMER_H_ */
