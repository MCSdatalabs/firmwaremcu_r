/**
 * @file			stc3115.c
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "stc3115.h"
#include "stc3115Battery.h"
#include "stc3115Driver.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/
static bool initialized = false;
static int stc3115_voltage;
static int stc3115_current;
static int stc3115_temperature;
static int stc3115_soc;
STC3115_ConfigData_TypeDef stc3115ConfigData;
STC3115_BatteryData_TypeDef stc3115BatteryData;

/******************** private function ********************/

/******************** global function *********************/
STC3115_RETURN stc3115_detect(void)
{
	uint8_t i2c_write_data[1];
	uint8_t i2c_read_data[1];

	i2c_write_data[0] = STC3115_REG_ID;
	if(i2c_writeRead(STC3115_ADDRESS, i2c_write_data, 1, i2c_read_data, 1) != 0)
		return STC3115_I2C_ERR;

	if(i2c_read_data[0] != STC3115_ID)
		return STC3115_FAIL;

	return STC3115_SUCCESS;
}

STC3115_RETURN stc3115_init(void)
{
	initialized = false;

	stc3115_voltage = -1;
	stc3115_current = -1;
	stc3115_temperature = -1;
	stc3115_soc = -1;

	if(GasGauge_Initialization(&stc3115ConfigData, &stc3115BatteryData) != STC3115_SUCCESS)
		return STC3115_NO_INIT;

	GasGauge_Task(&stc3115ConfigData, &stc3115BatteryData);

	initialized = true;
	return STC3115_SUCCESS;
}

STC3115_RETURN stc3115_disable(void)
{
	initialized = false;

	return STC3115_SUCCESS;
}

STC3115_RETURN stc3115_measure(void)
{
	stc3115_voltage = -1;
	stc3115_current = -1;
	stc3115_temperature = -1;
	stc3115_soc = -1;

	if(!initialized)
		return STC3115_NO_INIT;

	GasGauge_Task(&stc3115ConfigData, &stc3115BatteryData);
	stc3115_voltage = stc3115BatteryData.Voltage;
	stc3115_current = stc3115BatteryData.Current;
	// Temperature has only one decimal plcae, but for consistency with body
	// temperature we add another place
	stc3115_temperature = stc3115BatteryData.Temperature * 10;
	stc3115_soc = (stc3115BatteryData.SOC + 5) / 10;

	return STC3115_SUCCESS;
}

void stc3115_getValues(int16_t *voltage, int16_t *current, int16_t *temperature, int16_t *soc)
{
	*voltage = stc3115_voltage;
	*current = stc3115_current;
	*temperature = stc3115_temperature;
	*soc = stc3115_soc;

	return;
}
