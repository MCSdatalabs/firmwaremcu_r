/**
 * @file			stc3115.h
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef STC3115_H_
#define STC3115_H_

#include <stdint.h>
#include <stdbool.h>

#include "../i2c.h"

#define STC3115_ADDRESS		0xE0	//!< STC3115 I2C address

/**
 * @brief Return codes for the STC3115.
 */
typedef enum {
    STC3115_SUCCESS = 0, 		//!< Transfer completed successfully.
    STC3115_FAIL = -1,   		//!< STC3115 error
    STC3115_I2C_ERR = -2,		//!< I2C error.
    STC3115_NO_INIT = -99		//!< STC3115 not initialized.
}STC3115_RETURN;

/**
 * @brief Detect if a STC3115 is present.
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS 	STC3115 detected
 * @retval STC3115_FAIL 	STC3115 not detected
 * @retval STC3115_I2C_ERR 	I2C communication error
 */
STC3115_RETURN stc3115_detect(void);

/**
 * @brief Initialize the STC3115. The battery values are defined in the stc3115.h file.
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS 	STC3115 detected
 * @retval STC3115_NO_INT 	STC3115 not initialized
 */
STC3115_RETURN stc3115_init(void);

STC3115_RETURN stc3115_disable(void);

/**
 * @brief Perform a measurement.
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS 	STC3115 detected
 * @retval STC3115_NO_INT 	STC3115 not initialized
 */
STC3115_RETURN stc3115_measure(void);

/**
 * @brief Gets all data from the STC3115.
 *
 * @param voltage		Measured battery voltage
 * @param current		Measured battery current
 * @param temperature	Measured temperature on the STC3115
 * @param soc			Calculated battery capacity in %
 */
void stc3115_getValues(int16_t *voltage, int16_t *current, int16_t *temperature, int16_t *soc);

#endif /* STC3115_H_ */
