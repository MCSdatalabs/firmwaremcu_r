/**
 * @file			si7051.h
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef SI7013_H_
#define SI7013_H_

#include <stdint.h>
#include <stdbool.h>

#include "../i2c.h"

#define SI7013_ADDRESS      0x82	//!< SI7013 I2C address

#define SI7013_PROGRAM_NTC	0		//!< 1 to write the correction table to the sensor, 0 to not write. The table does not need to be programmed into the sensor. The Software is making the calculation. So keep this Value as 0.

/**
 * @brief Return codes for the SI7013.
 */
typedef enum {
	SI7013_SUCCESS = 0,				//!< Transfer completed successfully.
    SI7013_FAIL = -1,          		//!< SI7013 error.
    SI7013_I2C_ERR = -2,       		//!< I2C error.
    SI7013_WRONG_FIRMWARE = -10,	//!< Wrong firmware version received.
    SI7013_WRONG_ID = -11,      	//!< Wrong ID received.
    SI7013_NO_INIT = -99       		//!< SI7013 not initialized.
}SI7013_RETURN;

/**
 * @brief Checks if a SI7013 is connected.
 *
 * @return SI7013_RETURN
 * @retval SI7013_SUCCESS			Transfer completed successfully
 * @retval SI7013_I2C_ERR			I2C communication error
 * @retval SI7013_WRONG_ID			A different temperature sensor was detected but not initialized
 * @retval SI7013_WRONG_FIRMWARE	A different firmware version than 2.0 detected
 */
SI7013_RETURN si7013_detect(void);

/**
 * @brief Initialize the SI7013.
 *
 * Depending on the value for #SI7013_PROGRAM_NTC the correction table is written to the sensor or not. In the current state
 * the #SI7013_PROGRAM_NTC should be 0 to not write the correction table into the sensor.
 *
 * @return SI7013_RETURN
 * @retval SI7013_SUCCESS			Transfer completed successfully
 * @retval SI7013_I2C_ERR			I2C communication error
 * @retval SI7013_NO_INIT			No SI7013 was detected
 */
SI7013_RETURN si7013_init(void);

/**
 * @brief Disables the SI7013.
 *
 * @return SI7013_RETURN
 * @retval SI7013_SUCCESS			Transfer completed successfully
 */
SI7013_RETURN si7013_disable(void);

/**
 * @brief Get the measured temperature.
 *
 * Measures the temperature between -40&deg;C and 125&deg;C.
 *
 * @param[out] temperature Measured temperature value as integer in 0.01&deg;C
 *
 * @return SI7013_RETURN
 * @retval SI7013_SUCCESS	Transfer completed successfully
 * @retval SI7013_I2C_ERR	I2C communication error
 * @retval SI7013_NO_INIT	No SI7013 was detected
 */
SI7013_RETURN si7013_getTemperature(int16_t *temperature);

/**
 * @brief Get the measured NTC temperature.
 *
 * Measures the temperature between -40&deg;C and 125&deg;C.
 *
 * @param[out] temperature Measured NTC temperature value as integer in 0.01&deg;C
 *
 * @return SI7013_RETURN
 * @retval SI7013_SUCCESS	Transfer completed successfully
 * @retval SI7013_I2C_ERR	I2C communication error
 * @retval SI7013_NO_INIT	No SI7013 was detected
 */
SI7013_RETURN si7013_getNTCTemperature(int16_t *temperature);

/**
 * @brief Get the measured humidity.
 *
 * @param[out] humidity Measured humidity value as integer
 *
 * Measures the humidity between 0% and 100%.
 *
 * @return SI7013_RETURN
 * @retval SI7013_SUCCESS	Transfer completed successfully
 * @retval SI7013_I2C_ERR	I2C communication error
 * @retval SI7013_NO_INIT	No SI7013 was detected
 */
SI7013_RETURN si7013_getHumidity(int16_t *humidity);

#endif /* SI7013_H_ */
