/**
 * @file			si7021.c
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "si7013.h"
#include "ntcDefines.h"

#include "debug/debug.h"

/******************** defines *****************************/
/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */

/** Si7013 Read Temperature Command */
#define SI7013_READ_TEMP       				0xE0  /* Read previous T data from RH measurement
                                      	  	  	  * command*/
/** Si7013 Read RH Command */
#define SI7013_READ_RH         				0xE5  /* Perform RH (and T) measurement. */
/** Si7013 Read RH (no hold) Command */
#define SI7013_READ_RH_NH      				0xF5  /* Perform RH (and T) measurement in no hold mode. */
/** Si7013 Read Thermistor Command */
#define SI7013_READ_VIN        				0xEE  /* Perform thermistor measurement. */

#define SI7013_READ_USER_2					0x10
#define SI7013_WRITE_USER_2					0x50

#define SI7013_READ_USER_1					0xE7
#define SI7013_WRITE_USER_1					0xE6

#define SI7013_READ_USER_3					0x11
#define SI7013_WRITE_USER_3					0x51


/** Si7013 Read ID */
#define SI7013_READ_ID_FIRST_BYTE_1      	0xFA
#define SI7013_READ_ID_FIRST_BYTE_2      	0x0F
#define SI7013_READ_ID_SECOND_BYTE_1      	0xFC
#define SI7013_READ_ID_SECOND_BYTE_2		0xC9
/** Si7013 Read Firmware Revision */
#define SI7013_READ_FIRMWARE_1				0x84
#define SI7013_READ_FIRMWARE_2				0xB8

#define SI7013_FIRMWARE_REV_10				0xFF
#define SI7013_FIRMWARE_REV_20 				0x20
/** Device ID value for Si7013 */
#define SI7013_DEVICE_ID 					0x0D
/** @endcond */

/******************** prototypes **************************/
#if (SI7013_PROGRAM_NTC)
static uint8_t checkArrays(uint16_t *orgString, const uint16_t *testString, uint8_t size);
static SI7013_RETURN read_memory(uint16_t *input, uint16_t *output, uint16_t *slope);
static SI7013_RETURN write_memory(uint16_t *input, uint16_t *output, uint16_t *slope);
#endif

/******************** struct data type ********************/

/******************** private variables********************/
static bool initialized = false;

/******************** private function ********************/
#if (SI7013_PROGRAM_NTC)
uint8_t checkArrays(uint16_t *orgString, const uint16_t *testString, uint8_t size)
{
	uint8_t i;
	uint8_t ret = 0;
	for(i = 0; i < size; i++) {
		if(orgString[i] != testString[i]) {
			ret++;
		}
	}
	return ret;
}

SI7013_RETURN read_memory(uint16_t *input, uint16_t *output, uint16_t *slope)
{
	uint8_t i;

	uint8_t i2c_write_data[2];
	uint8_t i2c_read_data[1];

	i2c_write_data[0] = 0x84;

	for(i = 0; i < 9; i++) {
		i2c_write_data[1] = 0x82 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		input[i] = (i2c_read_data[0] << 8);

		i2c_write_data[1] = 0x83 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		input[i] |= i2c_read_data[0];
	}

	for(i = 0; i < 9; i++) {
		i2c_write_data[1] = 0x94 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		output[i] = (i2c_read_data[0] << 8);

		i2c_write_data[1] = 0x95 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		output[i] |= i2c_read_data[0];
	}

	for(i = 0; i < 9; i++) {
		i2c_write_data[1] = 0xA6 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		slope[i] = (i2c_read_data[0] << 8);

		i2c_write_data[1] = 0xA7 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		slope[i] |= i2c_read_data[0];
	}

	return SI7013_SUCCESS;
}

SI7013_RETURN write_memory(uint16_t *input, uint16_t *output, uint16_t *slope)
{
	uint8_t i = 0;
	uint16_t const test[9] = {0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF};

	uint8_t i2c_write_data[3];
	uint8_t i2c_read_data[1];

	i2c_write_data[0] = 0xC5;

	if(checkArrays(input, test, 9) == 0) {
		for(i = 0; i < 9; i++) {
			i2c_write_data[1] = 0x82 + i * 2;
			i2c_write_data[2] = ((ntcInput[i] & 0xFF00) >> 8);
			if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 3, i2c_read_data, 1) != 0)
				return SI7013_I2C_ERR;

			i2c_write_data[1] = 0x83 + i * 2;
			i2c_write_data[2] = ntcInput[i] & 0x00FF;
			if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 3, i2c_read_data, 1) != 0)
				return SI7013_I2C_ERR;
		}
	}

	if(checkArrays(output, test, 9) == 0) {
		for(i = 0; i < 9; i++) {
			i2c_write_data[1] = 0x94 + i * 2;
			i2c_write_data[2] = ((ntcOutput[i] & 0xFF00) >> 8);
			if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 3, i2c_read_data, 1) != 0)
				return SI7013_I2C_ERR;

			i2c_write_data[1] = 0x95 + i * 2;
			i2c_write_data[2] = ntcOutput[i] & 0x00FF;
			if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 3, i2c_read_data, 1) != 0)
				return SI7013_I2C_ERR;
		}
	}

	if(checkArrays(slope, test, 9) == 0) {
		for(i = 0; i < 9; i++) {
			i2c_write_data[1] = 0xA6 + i * 2;
			i2c_write_data[2] = ((ntcSlope[i] & 0xFF00) >> 8);
			if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 3, i2c_read_data, 1) != 0)
				return SI7013_I2C_ERR;

			i2c_write_data[1] = 0xA7 + i * 2;
			i2c_write_data[2] = ntcSlope[i] & 0x00FF;
			if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 3, i2c_read_data, 1) != 0)
				return SI7013_I2C_ERR;
		}
	}

	return SI7013_SUCCESS;
}
#endif

SI7013_RETURN read_memory(uint16_t *input, uint16_t *output, uint16_t *slope)
{
	uint8_t i;

	uint8_t i2c_write_data[2];
	uint8_t i2c_read_data[1];

	i2c_write_data[0] = 0x84;

	for(i = 0; i < 9; i++) {
		i2c_write_data[1] = 0x82 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		input[i] = (i2c_read_data[0] << 8);

		i2c_write_data[1] = 0x83 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		input[i] |= i2c_read_data[0];
	}

	for(i = 0; i < 9; i++) {
		i2c_write_data[1] = 0x94 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		output[i] = (i2c_read_data[0] << 8);

		i2c_write_data[1] = 0x95 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		output[i] |= i2c_read_data[0];
	}

	for(i = 0; i < 9; i++) {
		i2c_write_data[1] = 0xA6 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		slope[i] = (i2c_read_data[0] << 8);

		i2c_write_data[1] = 0xA7 + i * 2;
		if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
			return SI7013_I2C_ERR;

		slope[i] |= i2c_read_data[0];
	}

	return SI7013_SUCCESS;
}


/******************** global function *********************/
SI7013_RETURN si7013_detect(void)
{
	uint8_t i2c_write_data[2];
	uint8_t i2c_read_data[8];

	i2c_write_data[0] = SI7013_READ_USER_2;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 1) != 0)
		return SI7013_I2C_ERR;

	i2c_write_data[0] = SI7013_WRITE_USER_2;
	i2c_write_data[1] = i2c_read_data[0] & ~(1 << 5) & ~(1 << 1);
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
		return SI7013_I2C_ERR;

	i2c_write_data[0] = SI7013_READ_ID_FIRST_BYTE_1;
	i2c_write_data[1] = SI7013_READ_ID_FIRST_BYTE_2;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 8) != 0)
		return SI7013_I2C_ERR;

	i2c_write_data[0] = SI7013_READ_ID_SECOND_BYTE_1;
	i2c_write_data[1] = SI7013_READ_ID_SECOND_BYTE_2;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 8) != 0)
		return SI7013_I2C_ERR;

	if(i2c_read_data[0] != SI7013_DEVICE_ID)
		return SI7013_WRONG_ID;

	i2c_write_data[0] = SI7013_READ_FIRMWARE_1;
	i2c_write_data[1] = SI7013_READ_FIRMWARE_2;

	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
		return SI7013_I2C_ERR;

	if(i2c_read_data[0] != SI7013_FIRMWARE_REV_20)
		return SI7013_WRONG_FIRMWARE;

	return SI7013_SUCCESS;
}

SI7013_RETURN si7013_init(void)
{
	initialized = false;

	if(si7013_detect() != SI7013_SUCCESS)
		return SI7013_NO_INIT;

	uint16_t input[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	uint16_t output[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	uint16_t slope[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

	if(read_memory(input, output, slope) != SI7013_SUCCESS) {
		return SI7013_NO_INIT;
	}
	for(int i = 0; i< 9;i++){
			log_info("SI7013 READ INPUT %d: %x\n",i, input[i]);
		}
	for(int i = 0; i< 9;i++){
		log_info("SI7013 READ OUTPUT %d: %x\n",i, output[i]);
	}
	for(int i = 0; i< 9;i++){
		log_info("SI7013 READ SLOPE %d: %x\n",i, slope[i]);
	}

#if (SI7013_PROGRAM_NTC)
	uint16_t input[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	uint16_t output[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	uint16_t slope[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};


	if(read_memory(input, output, slope) != SI7013_SUCCESS) {
		return SI7013_NO_INIT;
	}

	if(write_memory(input, output, slope) != SI7013_SUCCESS) {
		return SI7013_NO_INIT;
	}

	if(read_memory(input, output, slope) != SI7013_SUCCESS) {
		return SI7013_NO_INIT;
	}

	uint8_t ret = checkArrays(input, ntcInput, 9);
	ret += checkArrays(output, ntcOutput, 9);
	ret += checkArrays(slope, ntcSlope, 9);

	if(ret != 0) {
		return SI7013_NO_INIT;
	}
#endif

	initialized = true;

	return SI7013_SUCCESS;
}

SI7013_RETURN si7013_disable(void)
{
	initialized = false;

	return SI7013_SUCCESS;
}

SI7013_RETURN si7013_getNTCTemperature(int16_t *temperature)
{
	*temperature = -27315;

	if(!initialized)
		return SI7013_NO_INIT;

	uint8_t i2c_write_data[2];
	uint8_t i2c_read_data[2];


	//dbg

	i2c_write_data[0] = SI7013_READ_USER_1;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 1) != 0)
		return SI7013_I2C_ERR;
	log_info("USER1 READ: %x , heater on %x",i2c_read_data[0],i2c_read_data[0] | (1 << 2));


	/*
	i2c_write_data[0] = SI7013_WRITE_USER_1;
	i2c_write_data[1] = 0x3A;
	int code = i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 2);
	if(code != 0){
		log_info("us1 write failed %d",code);
		return SI7013_I2C_ERR;
	}



	i2c_write_data[0] = SI7013_READ_USER_1;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 1) != 0)
		return SI7013_I2C_ERR;
	log_info("USER1 READ after write: %x ",i2c_read_data[0]);
	 	*/

	//dbg
	i2c_write_data[0] = SI7013_READ_USER_2;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 2) != 0)
		return SI7013_I2C_ERR;
	log_info("USER2 READ: %02x \n",i2c_read_data[0]&0xff);

	i2c_write_data[0] = SI7013_WRITE_USER_2;
	i2c_write_data[1] = 0x0E;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
		return SI7013_I2C_ERR;

	//dbg
	i2c_write_data[0] = SI7013_READ_USER_2;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 2) != 0)
		return SI7013_I2C_ERR;
	log_info("USER2 READ: %02x \n",i2c_read_data[0]&0xff);




	i2c_write_data[0] = SI7013_READ_USER_3;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 2) != 0)
		return SI7013_I2C_ERR;
	log_info("USER3 READ: %02x \n",i2c_read_data[0]&0xff);

	i2c_write_data[0] = SI7013_READ_VIN;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 2) != 0){
		log_info("READ_VIN write failed ");
		return SI7013_I2C_ERR;
	}


	//dbg
	//i2c_read_data[0] = 0x13;
	//i2c_read_data[1] = 0x7a;

	log_info("TEMP READ RAW: %x %x \n",i2c_read_data[0],i2c_read_data[1]);


	//&0xfc
	int32_t temp = (int32_t)( i2c_read_data[0] << 8) + (i2c_read_data[1]  );//(uint32_t)((i2c_read_data[0] << 8) | (i2c_read_data[1] & 0xfc));
	log_info("TEMP READ INT: %d ",temp);

	i2c_write_data[0] = SI7013_WRITE_USER_2;
	i2c_write_data[1] = 0x01;//0x09
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0){
		return SI7013_I2C_ERR;
	}



	int32_t div = 256;// uint16_t
	int16_t slope, output, input; //16
	if(temp > ntcInput[1]) {
		log_info("TAKING 0\n");
		input = ntcInput[0];
		output = ntcOutput[0];
		slope = ntcSlope[0];
	} else if (temp > ntcInput[2]) {
		log_info("TAKING 1\n");
		input = ntcInput[1];
		output = ntcOutput[1];
		slope = ntcSlope[1];
	} else if (temp > ntcInput[3]) {
		log_info("TAKING 2\n");
		input = ntcInput[2];
		output = ntcOutput[2];
		slope = ntcSlope[2];
	} else if (temp > ntcInput[4]) {
		log_info("TAKING 3\n");
		input = ntcInput[3];
		output = ntcOutput[3];
		slope = ntcSlope[3];
	} else if (temp > ntcInput[5]) {
		log_info("TAKING 4 %x \n",ntcInput[4]);
		input = (int32_t)ntcInput[4];
		output = (int32_t)ntcOutput[4];
		slope = (int32_t)ntcSlope[4];
	} else if (temp > ntcInput[6]) {
		log_info("TAKING 5\n");
		input = ntcInput[5];
		output = ntcOutput[5];
		slope = ntcSlope[5];
	} else if (temp > ntcInput[7]) {
		log_info("TAKING 6\n");
		input = ntcInput[6];
		output = ntcOutput[6];
		slope = ntcSlope[6];
	} else if (temp > ntcInput[8]) {
		log_info("TAKING 7\n");
		input = ntcInput[7];
		output = ntcOutput[7];
		slope = ntcSlope[7];
	} else {
		log_info("TAKING 8\n");
		input = ntcInput[8];
		output = ntcOutput[8];
		slope = ntcSlope[8];
		div = 1;
	}
	// 137A = 4984 = 26.7GradC
	//temp = 4984;
	//input = 5650;//5592;
	//output = 26797;
	//slope = -586;//-591;


	int32_t dt = ((temp - input) / div);
	//TempTest temp = output + slope * dt;

	//temp = output + slope * (temp - input) / 256;


	log_info("TeMp: %d \n", temp);
	// TempTest *temperature = temp * 17572/65536-4685;
	*temperature = temp ;
	/*
	i2c_write_data[0] = SI7013_WRITE_USER_2;
	i2c_write_data[1] = 0x09;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 2, i2c_read_data, 1) != 0)
		return SI7013_I2C_ERR;
	*/
	return SI7013_SUCCESS;
}

SI7013_RETURN si7013_getTemperature(int16_t *temperature)
{
	*temperature = -27315;

	if(!initialized)
		return SI7013_NO_INIT;

	uint8_t i2c_write_data[1];
	uint8_t i2c_read_data[2];

	i2c_write_data[0] = SI7013_READ_TEMP;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 2) != 0)
		return SI7013_I2C_ERR;

	int32_t temp = (int32_t)(i2c_read_data[0] << 8 | i2c_read_data[1]) * 17572/65536-4685;
	*temperature = (int16_t)temp;

	return SI7013_SUCCESS;
}

SI7013_RETURN si7013_getHumidity(int16_t *humidity)
{
	*humidity = -1;

	if(!initialized)
		return SI7013_NO_INIT;

	uint8_t i2c_write_data[1];
	uint8_t i2c_read_data[2];

	i2c_write_data[0] = SI7013_READ_RH;
	if(i2c_writeRead(SI7013_ADDRESS, i2c_write_data, 1, i2c_read_data, 2) != 0)
		return SI7013_I2C_ERR;

	int32_t temp = (int32_t)(i2c_read_data[0] << 8 | i2c_read_data[1]) * 125/65536-6;
	*humidity = (int16_t)temp*100;

	return SI7013_SUCCESS;
}
