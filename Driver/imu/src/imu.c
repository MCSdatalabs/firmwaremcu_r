/**
 * @file			mpu9250.c
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include <imu/inc/imu.h>
#include "inc/ble.h"

/******************** defines *****************************/
#if (MPU9250)
#include "imu/driver/mpu9250Driver.h"

#define imuDriver_init				mpu9250Driver_init
#define imuDriver_reset				mpu9250Driver_reset
#define imuDriver_getID				mpu9250Driver_getID
#define imuDriver_selfTest			mpu9250Driver_selfTest
#define imuDriver_calibrate			mpu9250Driver_calibrate
#define imuDriver_getTempData		mpu9250Driver_getTempData
#define imuDriver_interruptHandler	mpu9250Driver_interruptHandler

#elif (ICM20649)
#include "imu/driver/icm20649Driver.h"

#define imuDriver_init				icm20649Driver_init
#define imuDriver_reset				icm20649Driver_reset
#define imuDriver_getID				icm20649Driver_getID
#define imuDriver_selfTest			icm20649Driver_selfTest
#define imuDriver_calibrate			icm20649Driver_calibrate
#define imuDriver_getTempData		icm20649Driver_getTempData
#define imuDriver_interruptHandler	icm20649Driver_interruptHandler

#else
#error "Choose a IMU"
#endif


/******************** prototypes **************************/
static int8_t imu_i2cWrite(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
static int8_t imu_i2cRead(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
static void imuInterruptOneCallback(uint8_t pin);
#if (ICM20649)
static void imuInterruptTwoCallback(uint8_t pin);
#endif

callback_setAccelData_t localSetAccelData = NULL;

/******************** struct data type ********************/
static imuDriver_t imu = {
	.read = imu_i2cRead,
	.write = imu_i2cWrite
};

/******************** private variables********************/
static bool initialized = false;
static bool gyroInterrupt = false;
static bool accelInterrupt = false;

static bool interruptOneData = false;
static bool interruptTwoData = false;

static uint8_t writeData[I2C_BUFFER_SIZE];
static uint8_t readData[I2C_BUFFER_SIZE];

static float localGyro[3];
static float gyroLimit[3];
static float localAccel[3];
static float accelLimit[3];

/******************** private function ********************/
int8_t imu_i2cWrite(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t i;

	writeData[0] = reg_addr;
	for(i = 0; i < cnt; i++)
		writeData[i + 1] = reg_data[i];

	if(i2c_writeRead(IMU_ADDRESS, writeData, cnt + 1, readData, 0) != I2C_SUCCESS)
		return -1;

	return 0;
}

int8_t imu_i2cRead(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t i;

	writeData[0] = reg_addr;
	if(i2c_writeRead(IMU_ADDRESS, writeData, 1, readData, cnt) != I2C_SUCCESS)
		return -1;

	for(i = 0; i < cnt; i++)
		reg_data[i] = readData[i];

	return 0;
}

void imuInterruptOneCallback(uint8_t pin)
{
	if(!initialized)
		return;

	interruptOneData = true;
	GPIO_IntClear(1 << IMU_INT_1_PIN);
}

#if (NUMBER_OF_INT == 2)
void imuInterruptTwoCallback(uint8_t pin)
{
	if(!initialized)
		return;

	interruptTwoData = true;
	GPIO_IntClear(1 << IMU_INT_2_PIN);
}
#endif

/******************** global function *********************/
IMU_RETURN imu_detect(void)
{
	uint8_t id = 0;

	if(imuDriver_getID(&imu, &id) != IMU_SUCCESS)
		return IMU_I2C_ERR;

	if(id != IMU_DEVICE_ID)
		return IMU_FAIL;

	return IMU_SUCCESS;
}

IMU_RETURN imu_init(callback_setAccelData_t setAccelData)
{
	float selfTest[6];

	initialized = false;
	localSetAccelData = NULL;

	if(imu_detect() != IMU_SUCCESS)
		return IMU_FAIL;

	imuDriver_selfTest(&imu, selfTest);
	for(int i = 0; i < 6; i++) {
		if((selfTest[0] < -14) || (selfTest[0] > 14))
			return IMU_FAIL;
	}

	gyroInterrupt = false;
	accelInterrupt = false;
	gyroLimit[0] = 0;
	gyroLimit[1] = 0;
	gyroLimit[2] = 0;
	accelLimit[0] = 0;
	accelLimit[1] = 0;
	accelLimit[2] = 0;

	if(setAccelData == NULL)
		return IMU_NO_INIT;
	localSetAccelData = setAccelData;

	/* Configure PB11 as input and enable interrupt - mpu9250 interrupt. */
	/* Interrupt is active low */
	GPIO_PinModeSet(IMU_INT_1_PORT, IMU_INT_1_PIN, gpioModeInputPull, 1);
	GPIO_IntConfig(IMU_INT_1_PORT, IMU_INT_1_PIN, false, true, true);
	GPIOINT_CallbackRegister(IMU_INT_1_PIN, imuInterruptOneCallback);

#if (NUMBER_OF_INT == 2)
	GPIO_PinModeSet(IMU_INT_2_PORT, IMU_INT_2_PIN, gpioModeInputPull, 1);
	GPIO_IntConfig(IMU_INT_2_PORT, IMU_INT_2_PIN, false, true, true);
	GPIOINT_CallbackRegister(IMU_INT_2_PIN, imuInterruptTwoCallback);
#endif

	imuDriver_calibrate(&imu);
	imuDriver_init(&imu, ACCEL_FS_SEL_8g, GYRO_FS_SEL_500dps, MFS_16BITS);

	initialized = true;

	imuInterruptOneCallback(IMU_INT_1_PIN);
#if (NUMBER_OF_INT == 2)
	imuInterruptTwoCallback(IMU_INT_2_PIN);
#endif

	return IMU_SUCCESS;
}

IMU_RETURN imu_disable(void)
{
//	imuDriver_reset(&imu);		// -> f�hrt zu einer Endlosschleife und damit zu einem Watchdog reset.
	initialized = false;

	return IMU_SUCCESS;
}

IMU_RETURN imu_measure(void)
{
	if(!initialized)
		return IMU_NO_INIT;

	if(interruptOneData) {
		int16_t interrupt = 0;

		imuDriver_interruptHandler(&imu, localGyro, localAccel);

		if(localSetAccelData != NULL)
		{
			int16_t acc[3];
			acc[0] = localAccel[0] * 256;
			acc[1] = localAccel[1] * 256;
			acc[2] = localAccel[2] * 256;
			localSetAccelData(acc);
		}

		if(gyroInterrupt) {
			uint8_t i;
			for(i = 0; i < 3; i++) {
				if((localGyro[i] >= gyroLimit[i]) && (gyroLimit[i] > 0)){
					interrupt |= (1 << i);
				}
			}
		}

		if(accelInterrupt) {
			uint8_t i;
			for(i = 0; i < 3; i++) {
				if((localAccel[i] >= accelLimit[i]) && (accelLimit[i] > 0)){
					interrupt |= (1 << (i + 3));
				}
			}
		}

		if(interrupt > 0) {
			int16_t gyro[3];
			int16_t accel[3];

			imu_getGyroData(gyro);
			imu_getAccelData(accel);
			ble_sendCmd(SET_ACCEL, accel, 3);
			ble_sendCmd(SET_GYRO, gyro, 3);
			ble_sendCmd(SET_MPU9250_INT, &interrupt, 1);
			interrupt = 0;
		}

		interruptOneData = false;
	}
	if(interruptTwoData) {
		interruptTwoData = false;
	}

	return IMU_SUCCESS;
}

IMU_RETURN imu_getData(int16_t *gyro, int16_t *accel)
{
	gyro[0] = 0;
	gyro[1] = 0;
	gyro[2] = 0;
	accel[0] = 0;
	accel[1] = 0;
	accel[2] = 0;

	if(imu_getAccelData(accel) != IMU_SUCCESS)
		return IMU_NO_INIT;

	if(imu_getGyroData(gyro) != IMU_SUCCESS)
		return IMU_NO_INIT;

	return IMU_SUCCESS;
}

IMU_RETURN imu_getAccelData(int16_t *accel)
{
	accel[0] = 0;
	accel[1] = 0;
	accel[2] = 0;

	if(!initialized)
		return IMU_NO_INIT;

	accel[0] = localAccel[0] * 1000;
	accel[1] = localAccel[1] * 1000;
	accel[2] = localAccel[2] * 1000;

	return IMU_SUCCESS;
}

IMU_RETURN imu_getGyroData(int16_t *gyro)
{
	gyro[0] = 0;
	gyro[1] = 0;
	gyro[2] = 0;

	if(!initialized)
		return IMU_NO_INIT;

	gyro[0] = localGyro[0] * 1000;
	gyro[1] = localGyro[1] * 1000;
	gyro[2] = localGyro[2] * 1000;

	return IMU_SUCCESS;
}

IMU_RETURN imu_getTempData(int16_t *temperature)
{
	*temperature = -27315;

	if(!initialized)
		return IMU_NO_INIT;

	float temp;
	imuDriver_getTempData(&imu, &temp);
	*temperature = temp + 0.5;

	return IMU_SUCCESS;
}

IMU_RETURN imu_setGyroLimit(uint16_t *data)
{
	uint8_t i;

	gyroInterrupt = false;
	for(i = 0; i < 3; i++) {
		gyroLimit[i] = data[i] / 1000;
		if(gyroLimit[i] > 0) {
			gyroInterrupt = true;
		}
	}

	return IMU_SUCCESS;
}

IMU_RETURN imu_setAccelLimit(uint16_t *data)
{
	uint8_t i;

	accelInterrupt = false;
	for(i = 0; i < 3; i++) {
		accelLimit[i] = data[i] / 1000;
		if(accelLimit[i] > 0) {
			accelInterrupt = true;
		}
	}

	return IMU_SUCCESS;
}
