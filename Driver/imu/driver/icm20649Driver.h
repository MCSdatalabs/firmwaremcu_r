/**
 * @file 	icm20649Driver.h
 * @author 	GrLi
 * @date 	14.03.2019
 *
 * @brief
 *
**/

#ifndef _ICM20649DRIVER_H_
#define _ICM20649DRIVER_H_

#include "float.h"
#include "stdint.h"
#include "stdbool.h"

#include "../inc/imu_config.h"

#define ICM20649_DEVICE_ID	0xE1


int8_t icm20649Driver_getID(imuDriver_t *icm20649, uint8_t *id);
void icm20649Driver_reset(imuDriver_t *icm20649);
void icm20649Driver_calibrate(imuDriver_t *icm20649);
void icm20649Driver_init(imuDriver_t *icm20649, ACCEL_FS_SEL Ascale, GYRO_FS_SEL Gscale, MSCALE_SEL Mscale);
void icm20649Driver_selfTest(imuDriver_t *icm20649, float *destination);
void icm20649Driver_getTempData(imuDriver_t *icm20649, float *temperature);
void icm20649Driver_interruptHandler(imuDriver_t *icm20649, float *gyro, float *accel);


#endif /* _ICM20649DRIVER_H_ */
