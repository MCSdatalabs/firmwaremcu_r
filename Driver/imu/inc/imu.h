/**
 * @file			mpu9250.h
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef IMU_H_
#define IMU_H_

#include <stdbool.h>

#include "i2c.h"
#include "imu_config.h"
#include "gpiointerrupt.h"

typedef void (*callback_setAccelData_t)(int16_t *accel);

/**
 * @brief Return codes for the MP9250.
 */
typedef enum {
	IMU_SUCCESS = 0,			//!< Transfer completed successfully.
	IMU_FAIL = -1,          	//!< IMU error.
	IMU_I2C_ERR = -2,       	//!< I2C error.
	IMU_NO_INIT = -99       	//!< IMU not initialized.
}IMU_RETURN;

/**
 * @brief Checks if a IMU is connected.
 *
 * @return IMU_RETURN
 * @retval IMU_SUCCESS			Transfer completed successfully
 * @retval IMU_FAIL				IMU error
 * @retval IMU_I2C_ERR			I2C communication error
 */
IMU_RETURN imu_detect(void);

/**
 * @brief Initialize the IMU.
 *
 * Initialize the the gyro and the acceleration. The magnetic measurement
 * is not initialized.
 *
 * @return IMU_RETURN
 * @retval IMU_SUCCESS			Transfer completed successfully
 * @retval IMU_NO_INIT			IMU not initialized
 */
IMU_RETURN imu_init(callback_setAccelData_t setAccelData);

IMU_RETURN imu_disable(void);

IMU_RETURN imu_measure(void);

IMU_RETURN imu_getData(int16_t *gyro, int16_t *accel);
IMU_RETURN imu_getAccelData(int16_t *accel);
IMU_RETURN imu_getGyroData(int16_t *gyro);
IMU_RETURN imu_getTempData(int16_t *temperature);

/**
 * @brief Get the IMU measured temperature.
 *
 * @param temperature Measured temperature value as integer.
 *
 * @return IMU_RETURN
 * @retval IMU_SUCCESS			Transfer completed successfully
 * @retval IMU_I2C_ERR			I2C communication error
 * @retval IMU_NO_INIT			IMU not initialized
 */
//IMU_RETURN imu_getTempData(int16_t *temperature);

IMU_RETURN imu_setGyroLimit(uint16_t *data);
IMU_RETURN imu_setAccelLimit(uint16_t *data);


#endif /* IMU_H_ */
