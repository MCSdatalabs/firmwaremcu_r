/**
 * @file			i2c.c
 * @date			15.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "i2c.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/
static I2C_RETURN ret;
static bool initialized = false;
static bool busy = false;

/******************** private function ********************/

/******************** global function *********************/
I2C_RETURN i2c_init()
{
	if(initialized)
		return I2C_SUCCESS;

	/* Enabling clock to the I2C0 */
	CMU_ClockEnable(cmuClock_I2C0, true);

	/* Configure SDA (PA0) and SCL (PD1) as Open-drain with pull-up and filter. */
	GPIO_PinModeSet(I2C0_SCL_PORT, I2C0_SCL_PIN, gpioModeWiredAndPullUpFilter, 1);
	GPIO_PinModeSet(I2C0_SDA_PORT, I2C0_SDA_PIN, gpioModeWiredAndPullUpFilter, 1);

	// Using default settings
	I2C_Init_TypeDef init = I2C_INIT_DEFAULT;

	init.enable                    = 1;
	init.master                    = 1;
	init.freq                      = 400000;//I2C_FREQ_STANDARD_MAX;
	init.clhr                      = i2cClockHLRStandard;

	/* Initializing the I2C */
	I2C_Init(I2C0, &init);

	/* Enable signals SCL, SDA */
	I2C0->ROUTE |= I2C_ROUTE_SCLPEN | I2C_ROUTE_SDAPEN;

	initialized = true;
	busy = false;

	return I2C_SUCCESS;
}

I2C_RETURN i2c_getInitialized(void)
{
	if(initialized)
		return I2C_SUCCESS;

	return I2C_NO_INIT;
}

I2C_RETURN i2c_disable(void)
{
	initialized = false;
	
	return I2C_SUCCESS;
}

I2C_RETURN i2c_write(uint8_t addr, uint8_t* txBuf, uint32_t txSize)
{
	ret = I2C_SUCCESS;

	if(!initialized)
		return I2C_NO_INIT;

	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	i2cTransfer.addr  = addr;
	i2cTransfer.flags = I2C_FLAG_WRITE;
	i2cTransfer.buf[0].data   = txBuf;
	i2cTransfer.buf[0].len    = txSize;
	I2C_TransferInit(I2C0, &i2cTransfer);

	/* Sending data */
	I2C_TransferReturn_TypeDef ret;
	do{
		ret = I2C_Transfer(I2C0);
	}while(ret == i2cTransferInProgress);

	return ret;
}

I2C_RETURN i2c_writeRead(uint8_t addr, uint8_t* txBuf, uint32_t txSize, uint8_t* rxBuf, uint32_t rxSize)
{
	if(busy)
		return I2C_TRANSFER_IN_PROGRESS;

	if(!initialized)
		return I2C_NO_INIT;

	ret = I2C_SUCCESS;
	busy = true;

	/* Transfer structure */
	I2C_TransferSeq_TypeDef i2cTransfer;

	i2cTransfer.addr  = addr;
	i2cTransfer.flags = I2C_FLAG_WRITE_READ;
	i2cTransfer.buf[0].data   = txBuf;
	i2cTransfer.buf[0].len    = txSize;

	/* Select location/length of data to be read */
	i2cTransfer.buf[1].data = rxBuf;
	if(rxSize == 0)
		i2cTransfer.buf[1].len  = 1;
	else
		i2cTransfer.buf[1].len  = rxSize;
	I2C_TransferInit(I2C0, &i2cTransfer);

	/* Sending data */
	I2C_TransferReturn_TypeDef ret;
	do{
		ret = I2C_Transfer(I2C0);
	}while(ret == i2cTransferInProgress);

	busy = false;
	return ret;
}
