/**
 * @file			main.cpp
 * @date			16.03.2017
 *
 * @details
 */
/******************** include files ***********************/
#include "em_cmu.h"
#include "em_rmu.h"
#include "em_chip.h"
#include "em_device.h"

extern "C"
{
#include "i2c.h"
#include "watchdog.h"
#include "userTimer.h"
#include "rtcdriver.h"

#include "inc/ble.h"
#include "inc/gpio.h"

#include "memory/memory.h"
#include "si7013/si7013.h"
#include "si7051/si7051.h"
#include "si1144/si1144.h"
#include "stc3115/stc3115.h"
#include <imu/inc/imu.h>

#include "debug/debug.h"
}

/******************** defines *****************************/
#define MEMORY			0

#undef VERSION_TEXT
#undef VERSION_MAJ
#undef VERSION_MIN
#define VERSION_MAJ 0
#define VERSION_MIN 1
#define VERSION_TEXT "SmarKo Firmware v%d.%2d\nMCU: EFM32GG230F1024\n\n"

#define LED_COUNTER		5000
#define SI1144_COUNTER 	1000  	// HRM && SPO2 counter
#define STC3115_COUNTER 30000   // Battery monitor counter
#define SI7051_COUNTER 	15000  	// Body temperature counter
#define SDMODE_COUNTER 	60
#define IMU_COUNTER 	500
#define SI7013_COUNTER 	15000  	// Environment temperature counter

#define SI7013_BYTE		0
#define SI7051_BYTE		1
#define SI1144_BYTE		2
#define STC3115_BYTE	3
#define IMU_BYTE		4
#define MEMORY_BYTE		5
#define IMAGE_0_BYTE	6
#define IMAGE_1_BYTE	7

/******************** struct data type ********************/
typedef enum {
	TEST_SUCCESS = 0, TEST_FAIL = -1
} TEST_RETURN;

/******************** prototypes **************************/
TEST_RETURN autoTests();
TEST_RETURN i2cAutoTest();
TEST_RETURN spiAutoTest();
TEST_RETURN powerAutoTest();

void diasbleAllSensors(void);
void startAllSensors(void);

void changeGyroTime(uint16_t time);
void changeAccelTime(uint16_t time);

void SysTick_userHandler(void);

/******************** private variables********************/
static unsigned long resetCause;
static bool debug = false;
static bool connected = false;
static bool update = false;
static bool overTemperature = false;

static bool panic;
static bool charging;
static bool HRM_SPO2_mode = true;

static bool startUp = true;
static bool si1144Started = false;
static bool si1144Enabled = false;
static bool si7013Enabled = false;
static bool si7051Enabled = false;
static bool memoryEnabled = false;
static bool imuEnabled = false;
static bool stc3115Enabled = false;
static bool si1144_hasSkinContact = false;
static bool si1144_hasSkinContactOld = false;

static uint8_t errorByte;
static uint8_t statusByte;
static int16_t errorStatus;

static int16_t gyro[3];
static int16_t accel[3];
static int16_t skinTemperature;
static int16_t environmentTemperature;
static int16_t environmentHumidity;
static int16_t stc3115Voltage;
static int16_t stc3115Current;
static int16_t stc3115Temperature;
static int16_t stc3115Soc;
static int16_t ledCounter;
static int16_t si1144_hrm;
static int16_t si1144_spo2;

static int32_t si1144_counter;
static int32_t si7051_counter;
static int32_t stc3115_counter;
static int32_t sdmode_counter;
static int32_t measurement_counter;
static int32_t imu_counter;
static int32_t imu_accelCounter;
static int32_t imu_accelTime;
static int32_t imu_gyroCounter;
static int32_t imu_gyroTime;
static int32_t si7013_counter;

/******************** test function ***********************/
TEST_RETURN autoTests()
{
	TEST_RETURN ret = TEST_SUCCESS; // green

	/* I2C */
	if (i2cAutoTest() == TEST_FAIL)
		ret = TEST_FAIL;

#if (MEMORY)
	delay(100);
	/* SPI */
	if (spiAutoTest() == TEST_FAIL)
		ret = TEST_FAIL;
#else
	errorByte |= (1 << MEMORY_BYTE);
#endif

	delay(100);
	/* Power */
	if (powerAutoTest() == TEST_FAIL)
		ret = TEST_FAIL;

	return ret;
}

TEST_RETURN i2cAutoTest()
{
	TEST_RETURN ret = TEST_SUCCESS;

	/* I2C device scan */
	log_info_swo("I2C auto test:\n\n");
	log_info_swo("Si7013: ");

	if (si7013_detect() != SI7013_SUCCESS) {
		ret = TEST_FAIL;
		errorByte |= (1 << SI7013_BYTE);
		log_msg_swo("FAIL\n");
	} else {
		log_msg_swo("OK\n");
	}

	log_info_swo("Si7051: ");
	if (si7051_detect() != SI7051_SUCCESS) {
		ret = TEST_FAIL;
		errorByte |= (1 << SI7051_BYTE);
		log_msg_swo("FAIL\n");
	} else {
		log_msg_swo("OK\n");
	}

	log_info_swo("Si1144: ");
	if (si1144_detect() != SI1144_SUCCESS) {
		ret = TEST_FAIL;
		errorByte |= (1 << SI1144_BYTE);
		log_msg_swo("FAIL\n");
	} else {
		log_msg_swo("OK\n");
	}

	log_info_swo("STC3115: ");

	if (stc3115_detect() != STC3115_SUCCESS) {
		ret = TEST_FAIL;
		errorByte |= (1 << STC3115_BYTE);
		log_msg_swo("FAIL\n");
	} else {
		log_msg_swo("OK\n");
	}

	log_info_swo("IMU: ");
	if (imu_detect() != IMU_SUCCESS) {
		ret = TEST_FAIL;
		errorByte |= (1 << IMU_BYTE);
		log_msg_swo("FAIL\n");
	} else {
		log_msg_swo("OK\n");
	}
	log_msg_swo("\n");
	return ret;
}

TEST_RETURN spiAutoTest() {
	TEST_RETURN ret = TEST_SUCCESS;

	/* SPI device scan */
	log_info_swo("SPI auto test:\n\n");


	static uint8_t transmitBuffer[12] = {"Test String"};
	static uint8_t receiveBuffer[12];

	log_info_swo("Memory: ");
	if (memory_init() == MEMORY_SUCCESS) {
		watchdog_feed();

		log_msg_swo("OK\n");
		memory_chipErase();


		log_info_swo("Write test string to memory.\n");
		log_info_swo("String write: ");
		log_msg_swo((char *)transmitBuffer);
		log_msg_swo("\n");

		memory_writeMemory(0, transmitBuffer, 11);
		watchdog_feed();

		log_info_swo("Read test string from memory.\n");
		log_info_swo("String read: ");

		memory_readMemory(0, receiveBuffer, 11);

		log_msg_swo((char *)receiveBuffer);
		log_msg_swo("\n");

		watchdog_feed();

		log_info_swo("Compare strings: ");
		if(memcmp(transmitBuffer,receiveBuffer,11) != 0) {
			errorByte |= (1 << MEMORY_BYTE);
			log_msg_swo("FAIL\n");
		} else {
			log_msg_swo("OK\n");
		}

		watchdog_feed();
	} else {
		ret = TEST_FAIL;
		errorByte |= (1 << MEMORY_BYTE);
		log_msg_swo("FAIL\n");
	}
	log_msg_swo("\n");

	return ret;
}

TEST_RETURN powerAutoTest() {
	int16_t vol = 0;
	int16_t cur = 0;
	int16_t tmp = 0;
	int16_t soc = 0;
	TEST_RETURN ret = TEST_SUCCESS;

	log_info_swo("Power auto test\n\n");

	if (stc3115_init() != STC3115_SUCCESS)
		ret = TEST_FAIL;

	delay(500);
	if (stc3115_measure() == STC3115_SUCCESS)
	{
		stc3115_getValues(&vol, &cur, &tmp, &soc);
		if (vol < 2900 || vol > 4300)
			ret = TEST_FAIL;

		if (cur < -50 || cur > 300)
			ret = TEST_FAIL;
	} else
		ret = TEST_FAIL;
	log_info_swo("Battery voltage/current: %d mV/%d mA\n", vol, cur);

	if (ret == TEST_FAIL) {
		errorByte |= (1 << STC3115_BYTE);
		log_error_swo("Power auto test: FAIL\n");
	} else {
		log_info_swo("Power auto test: OK\n");
	}

	log_msg("\n");
	return ret;
}

/********************Validation Functions *****************/
bool validate_HRM (){
	if (si1144_hrm >= 30 && si1144_hrm <= 200 ) {
		return true;
	}
	return false;
}

bool validate_SPO2 (){
	if (si1144_spo2 >= 50 && si1144_spo2 <= 220 ) {
		return true;
	}
	return false;
}

/******************** private function ********************/
void diasbleAllSensors(void)
{
	stc3115_disable();
	si7013_disable();
	si7051_disable();
	imu_disable();
	si1144Started = false;
	si1144_stop();
	sdmode_counter = 0;
	si1144_hrm = 0;
	si1144_spo2 = 0;
	si1144_disable();
}

void startAllSensors(void)
{
/*	The memory is currently not used!
	if (memory_init() == MEMORY_SUCCESS) {
		memoryEnabled = true;
		statusByte |= (1 << MEMORY_BYTE);
#if (DEBUG)
		log_info("Memory init success.\n");
	} else {
		log_error("Memory init failed.\n");
#endif
	}
*/

	watchdog_feed();
	if (si7013_init() == SI7013_SUCCESS) {
		si7013Enabled = true;
		statusByte |= (1 << SI7013_BYTE);
		log_info_swo("Si7013 init success.\n");
	} else {
		log_error_swo("Si7013 init failed.\n");
	}

	watchdog_feed();
	if (si7051_detect() == SI7051_SUCCESS) {
		si7051Enabled = true;
		statusByte |= (1 << SI7051_BYTE);
		log_info_swo("Si7051 init success.\n");
	} else {
		log_error_swo("Si7051 init failed.\n");
	}

	watchdog_feed();
	if (si1144_init(HRM_SPO2_mode) == SI1144_SUCCESS) {
		si1144Enabled = true;
		statusByte |= (1 << SI1144_BYTE);
		log_info_swo("Si1144 init success.\n");
	} else {
		log_error_swo("Si1144 init failed.\n");
	}

	watchdog_feed();
	if (imu_init(si1144_setAccelData) == IMU_SUCCESS) {
		imuEnabled = true;
		statusByte |= (1 << IMU_BYTE);
		log_info_swo("IMU init success.\n");
	} else {
		log_error_swo("IMU init failed.\n");
	}

	watchdog_feed();
	if (stc3115_init() == STC3115_SUCCESS) {
		stc3115Enabled = true;
		statusByte |= (1 << STC3115_BYTE);
		stc3115_getValues(&stc3115Voltage, &stc3115Current, &stc3115Temperature, &stc3115Soc);
		log_info_swo("STC3115 init success.\n");
	} else {
		log_error_swo("STC3115 init failed.\n");
	}

	watchdog_feed();

	errorStatus = (errorByte << 8 | statusByte);
	ble_sendCmd(GET_ERROR_STATUS_BYTE, &errorStatus, 1);

	if ((si1144Enabled) && (si1144_start() == SI1144_SUCCESS)) { //&& (!charging)
		si1144Started = true;
	}
}

void changeGyroTime(uint16_t time)
{
	imu_gyroTime = time;
	log_info_swo("MPU9250 Accel Time: %d\n", imu_gyroTime);
}

void changeAccelTime(uint16_t time)
{
	imu_accelTime = time;
	log_info_swo("MPU9250 Gyro Time: %d\n", imu_accelTime);
}

/******************** interrupt function ******************/
void SysTick_userHandler(void) {



	if(startUp)
		return;

	if(update) {
		if(ledCounter <= 250) {
			//gpio_setLED(LED_YELLOW);
			ledCounter--;
			if(ledCounter <= 0) {
				ledCounter = 500;
				//gpio_setLED(LED_OFF);
			}
		}
		else {
			ledCounter--;
		}
		return;
	}
	watchdog_feed();

	// Gyro test
	if (imuEnabled){

		imu_measure();
		if (imu_accelCounter <= 0) {
			imu_accelCounter = imu_accelTime;
			imu_getAccelData(accel);
			ble_sendCmd(SET_ACCEL, accel, 3);
		} else {
			imu_accelCounter--;
		}

		if (imu_gyroCounter <= 0) {
			imu_gyroCounter = imu_gyroTime;
			imu_getGyroData(gyro);
			ble_sendCmd(SET_GYRO, gyro, 3);
		} else {
			imu_gyroCounter--;
		}

#if (DEBUG && DEBUG_SWO)
		if (imu_counter <= 0) {
			imu_counter = IMU_COUNTER * 2;
			imu_getData(gyro, accel);
			//log_info("IMU Gyro: %d / %d / %d\n", gyro[0], gyro[1], gyro[2]);
			//log_info("IMU Accel: %d / %d / %d\n", accel[0], accel[1], accel[2]);

		} else {
			imu_counter--;
		}
#endif
	}

	// HRM/SPO2, get measured data and send it to BLE chip
	if (si1144Started) {
		// HRM/SPO2 should be measured constantly as often as possible
		si1144_measure();

		if(si1144_counter <= 0) {
			si1144_counter = SI1144_COUNTER;

			si1144_getData(&si1144_hrm, &si1144_spo2, &si1144_hasSkinContact);
			if(si1144_hasSkinContactOld != si1144_hasSkinContact) {
				ble_sendCmd(SET_HAS_SKIN_CONTACT, (int16_t *)si1144_hasSkinContact, 1);
				log_info_swo("Si1144 SkinContact: %d\n", si1144_hasSkinContact);
			}
			si1144_hasSkinContactOld = si1144_hasSkinContact;

			if (si1144_hasSkinContact) {
				// swaping between the two modes (static and dynamic)
				if(sdmode_counter == 50) {
					HRM_SPO2_mode = false;
					si1144_init(HRM_SPO2_mode);
					si1144_start();
					measurement_counter = 10;
					sdmode_counter--;
				} else if (sdmode_counter <= 0) {
					HRM_SPO2_mode = true;
					sdmode_counter = SDMODE_COUNTER;
					measurement_counter = 10;
					si1144_init(HRM_SPO2_mode);
					si1144_start();
				} else {
					sdmode_counter--;
				}

				if (validate_HRM()) {
					if(measurement_counter == 0) {
						ble_sendCmd(SET_PULSE_DATA, &si1144_hrm, 1);
						log_info_swo("Si1144 HRM:  %d\n", si1144_hrm);
					} else {
						measurement_counter--;
					}
				}

				if (validate_SPO2()) {
					ble_sendCmd(SET_SPO2_DATA, &si1144_spo2, 1);
					log_info_swo("Si1144 SpO2: %d\n", si1144_spo2);
				}
			} else {
				if(measurement_counter != -1) {
					HRM_SPO2_mode = true;
					sdmode_counter = SDMODE_COUNTER;
					si1144_init(HRM_SPO2_mode);
					si1144_start();
					measurement_counter = -1;
				}
			}
		} else {
			si1144_counter--;
		}
	}

	if(si7013Enabled) {
		if (si7013_counter <= 0) {
			si7013_counter = SI7013_COUNTER;
			//si7013_getTemperature(&environmentTemperature);
			si7013_getNTCTemperature(&environmentTemperature);
			si7013_getHumidity(&environmentHumidity);
			ble_sendCmd(SET_ENV_TEMP, &environmentTemperature, 1);
			ble_sendCmd(SET_ENV_HUMIDITY, &environmentHumidity, 1);
#if (DEBUG && DEBUG_SWO)
			log_info("Si7013 Env_Temp: %d\n", environmentTemperature);
			log_info("Si7013 Hum:  %d\n", environmentHumidity);
#endif
		} else {
			si7013_counter--;
		}
	}

	// Body temperature, get measured data and send it to BLE chip
	if(si7051Enabled) {
		if (si7051_counter <= 0) {
			si7051_counter = SI7051_COUNTER;
			si7051_getTemperature(&skinTemperature);
			//t_core = t_skin + t_factor * (t_skin - t_tambient);
			ble_sendCmd(SET_BODY_TEMP, &skinTemperature, 1);
			log_info_swo("Si7051 Skin_raw_Temp: %d\n", skinTemperature);
		} else {
			si7051_counter--;
		}
	}

	// this is a test Battery Monitor, get measured data and send it to BLE chip
	if(stc3115Enabled) {
		if (stc3115_counter <= 0) {
			stc3115_counter = STC3115_COUNTER;
			stc3115_measure();
			stc3115_getValues(&stc3115Voltage, &stc3115Current, &stc3115Temperature, &stc3115Soc);
			ble_sendCmd(SET_BATTERY_SOC, &stc3115Soc, 1);
			ble_sendCmd(SET_BATTERY_VOLTAGE, &stc3115Voltage, 1);
			ble_sendCmd(SET_BATTERY_CURRENT, &stc3115Current, 1);
			ble_sendCmd(SET_BATTERY_TEMP, &stc3115Temperature, 1);
			log_info_swo("Battery SOC: %d%%\n", stc3115Soc);
			log_info_swo("Battery Voltage:  %dmV\n", stc3115Voltage);
			log_info_swo("Battery Current: %dmA\n", stc3115Current);

		} else {
			stc3115_counter--;
		}
	}

	if(ledCounter <= 50) {
		gpio_setLED(LED_GREEN);
		ledCounter--;
		if(ledCounter <= 0) {
			ledCounter = LED_COUNTER;
			gpio_setLED(LED_OFF);
		}
	} else {
		ledCounter--;
	}
}

/******************** global function *********************/
int main(void) {
	ledCounter = LED_COUNTER;
	si1144_counter = SI1144_COUNTER;
	stc3115_counter = STC3115_COUNTER;
	si7051_counter = SI7051_COUNTER;
	si7013_counter = SI7013_COUNTER;
	sdmode_counter = SDMODE_COUNTER;
	imu_counter = IMU_COUNTER;
	imu_accelTime = IMU_COUNTER;
	imu_accelCounter = imu_accelTime;
	imu_gyroTime = IMU_COUNTER;
	imu_gyroCounter = imu_gyroTime;

	errorByte = 0;
	statusByte = 0;
	errorStatus = 0;
	accel[0] = 0;
	accel[1] = 0;
	accel[2] = 0;
	gyro[0] = 0;
	gyro[1] = 0;
	gyro[2]= 0;
	skinTemperature = 0;
	environmentTemperature = 0;
	environmentHumidity = 0;

	si1144_hrm = 0;
	si1144_spo2 = 0;;
	stc3115Voltage = 0;
	stc3115Current = 0;
	stc3115Temperature = 0;
	stc3115Soc = 0;

	startUp = true;
	si1144Enabled = false;
	si7013Enabled = false;
	si7051Enabled = false;
	memoryEnabled = false;
	imuEnabled = false;
	stc3115Enabled = false;
	si1144_hasSkinContact = false;
	si1144_hasSkinContactOld = false;

	HRM_SPO2_mode = true;

	update = false;
	debug = false;
	connected = false;
	overTemperature = false;

	/* Chip errata */
	CHIP_Init();

	/* Store the cause of the last reset, and clear the reset cause register */
	resetCause = RMU_ResetCauseGet();
	RMU_ResetCauseClear();

	CMU ->CTRL = (CMU ->CTRL & ~_CMU_CTRL_HFXOMODE_MASK) | CMU_CTRL_HFXOMODE_XTAL;
	CMU ->CTRL = (CMU ->CTRL & ~_CMU_CTRL_HFXOBOOST_MASK) | CMU_CTRL_HFXOBOOST_50PCENT;
	SystemHFXOClockSet(48000000);

	CMU_OscillatorEnable(cmuOsc_HFXO, true, true);

	/* Enable peripheral clock */
	CMU_ClockEnable(cmuClock_HFPER, true);

	// call the Handler of interrupt function
	sysTick_init(SysTick_userHandler);

	RTCDRV_Init();

	gpio_init(&charging, &panic);
	gpio_setLED(LED_YELLOW);

	NVMHAL_Init();

	uint8_t *address;
	NVMHAL_Read(CURRENT_IMAGE_ADDR, &address, sizeof(address));

	if(address == IMAGE_0_BINARY_ADDR){
		errorByte |= (1 << IMAGE_0_BYTE);
		statusByte |= (1 << IMAGE_0_BYTE);
		gpio_setLED(LED_GREEN);
	}
	else if(address == IMAGE_1_BINARY_ADDR){
		errorByte |= (1 << IMAGE_1_BYTE);
		statusByte |= (1 << IMAGE_1_BYTE);
		gpio_setLED(LED_RED);
	}

	NVMHAL_DeInit();

	i2c_init();

//	NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
//	NVIC_EnableIRQ(GPIO_EVEN_IRQn);
//
//	NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
//	NVIC_EnableIRQ(GPIO_ODD_IRQn);

	NVIC_ClearPendingIRQ(I2C1_IRQn);
	NVIC_EnableIRQ(I2C1_IRQn);

	// RZ
	if (resetCause & RMU_RSTCAUSE_WDOGRST) {
		ble_reset(false);
	} else {
		ble_reset(true);
	}



	//ble_reset(true);

	// SWO debugging
#if (DEBUG && DEBUG_SWO)
	swo_init();				// enable SWO-pin for debug data -> needed for energy profiling
#endif

	ble_init(&connected, &debug, &update);

	log_info(VERSION_TEXT, VERSION_MAJ, VERSION_MIN);

	int8_t i = 0;
	TEST_RETURN testRet = autoTests();
	errorStatus = (errorByte << 8 | statusByte);
	ble_sendCmd(GET_ERROR_STATUS_BYTE, &errorStatus, 1);
	if(testRet == TEST_FAIL) {
		for(i = 0; i < 3 ; i++) {
			gpio_setLED(LED_RED);
			delay(500);
			gpio_setLED(LED_OFF);
			delay(500);
		}
	} else {
		for(i = 0; i < 3 ; i++)	{
			gpio_setLED(LED_GREEN);
			delay(500);
			gpio_setLED(LED_OFF);
			delay(500);
		}
	}

	gpio_setLED(LED_YELLOW);

	watchdog_init();
	watchdog_start();

	watchdog_feed();
	delay(100);

	if (resetCause & RMU_RSTCAUSE_WDOGRST){
		log_info_swo("A watchdog reset occurred!\n\n");
	}

	startAllSensors();
	gpio_setLED(LED_OFF);
	startUp = false;
	watchdog_feed();
	while (true) {


		if(update){
			//log_info("update: %d \n",update);
			gpio_setLED(LED_OFF);
			delay(500);
			if(address == IMAGE_0_BINARY_ADDR){
				gpio_setLED(LED_GREEN);
			}
			else if(address == IMAGE_1_BINARY_ADDR){
				gpio_setLED(LED_YELLOW);
			}
			delay(500);
			continue;
		}
		watchdog_feed();


		if(gpio_getCharging()) {
			charging = true;
		} else {
			charging = false;
		}

		if(charging) {
			if(si1144Started) {
				si1144Started = false;
				si1144_stop();
				sdmode_counter = 0;
				si1144_hrm = 0;
				si1144_spo2 = 0;
				ble_sendCmd(SET_PULSE_DATA, &si1144_hrm, 1);
				ble_sendCmd(SET_SPO2_DATA, &si1144_spo2, 1);
			}
		} else {
			if((!si1144Started) && (!overTemperature)) {
				delay(100);
				if(!charging) {
					si1144_start();
					si1144Started = true;
				}
			}
		}

		if(update && si1144Started)	{
			si1144Started = false;
			si1144_stop();
			sdmode_counter = 0;
			si1144_hrm = 0;
			si1144_spo2 = 0;
			ble_sendCmd(SET_PULSE_DATA, &si1144_hrm, 1);
			ble_sendCmd(SET_SPO2_DATA, &si1144_spo2, 1);
		}

		if(!overTemperature && environmentTemperature >= 6000) {
			overTemperature = true;
			gpio_clearBQ51050B_TS();
			delay(500);
			gpio_setBQ51050B_TS();
		} else if(overTemperature && environmentTemperature < 4500) {
			overTemperature = false;
		}
	}
}
