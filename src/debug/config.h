#ifndef CONFIG_H_
#define CONFIG_H_

#define DEBUG						1 // enable debug messages (at least one of DEBUG_SWO and DEBUG_BLE should be activated too)
#define DEBUG_SWO                   1 // if enabled debug messages are send through SWO (DEBUG_SWO and DEBUG_BLE can be used at the same time)
#define DEBUG_UPDATE                1 // if enabled update messages are send through SWO
#define DEBUG_IMAGE					0

#endif /* CONFIG_H_ */
