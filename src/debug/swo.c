#include "swo.h"
#include "config.h"

void swo_init(void) {
	/* Enable GPIO Clock. */
	CMU ->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
	/* Enable Serial wire output pin */GPIO ->ROUTE |= GPIO_ROUTE_SWOPEN;

	/* Set location 0 */
	GPIO ->ROUTE = (GPIO ->ROUTE & ~(_GPIO_ROUTE_SWLOCATION_MASK)) | GPIO_ROUTE_SWLOCATION_LOC0;

	/* Enable output on pin - GPIO Port F, Pin 2 */
	GPIO ->P[5].MODEL &= ~(_GPIO_P_MODEL_MODE2_MASK);
	GPIO ->P[5].MODEL |= GPIO_P_MODEL_MODE2_PUSHPULL;

	/* Enable debug clock AUXHFRCO */
	CMU ->OSCENCMD = CMU_OSCENCMD_AUXHFRCOEN;

	while (!(CMU ->STATUS & CMU_STATUS_AUXHFRCORDY))
		;

#if (DEBUG_SWO)
	/* Enable trace in core debug */
	CoreDebug ->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	ITM ->LAR = 0xC5ACCE55;
	ITM ->TER = 0x0;
	ITM ->TCR = 0x0;
	TPI ->SPPR = 2;
	TPI ->ACPR = 0xf;
	ITM ->TPR = 0x0;
	DWT ->CTRL = 0x400003FF;
	ITM ->TCR = 0x0001000D;
	TPI ->FFCR = 0x00000100;
	ITM ->TER = 0x1;
#else
	/* Enable trace in core debug */CoreDebug ->DHCSR |= 1;
	CoreDebug ->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

	/* Enable PC and IRQ sampling output */DWT ->CTRL = 0x400113FF;
	/* Set TPIU prescaler to 16. */TPI ->ACPR = 0xf;
	/* Set protocol to NRZ */TPI ->SPPR = 2;
	/* Disable continuous formatting */TPI ->FFCR = 0x100;
	/* Unlock ITM and output data */ITM ->LAR = 0xC5ACCE55;
	ITM ->TCR = 0x10009;
#endif
}

void swo_print(const char * msg) {
	uint16_t len = strlen(msg);
	for (uint16_t i = 0; i < len; i++) {
		ITM_SendChar(msg[i]);
	}
}

void swo_printf(const char * msg, ...) {
	char formattedStr[128];
	va_list args;
	va_start(args, msg);
	vsprintf(formattedStr, msg, args);
	va_end(args);
	swo_print(formattedStr);
}
