#ifndef DEBUG_H_
#define DEBUG_H_
/*
 * debug.h
 *
 *  Created on: Nov 9, 2017
 *      Author: Simon
 */

#include "stdarg.h"
#include "string.h"
#include "config.h"
#include "swo.h"
#include "inc/ble.h"

typedef enum {
	LOG_DEBUG,
	LOG_INFO,
	LOG_ERROR,
	LOG_MSG
} LOG_TYPE;

void debug_log(LOG_TYPE type, const char * msg);

void debug_logf(LOG_TYPE type, const char * msg, ...);

void log_debug(const char * msg, ...);

void log_info(const char * msg, ...);

void log_info_update(const char * msg, ...);

void log_info_swo(const char * msg, ...);

void log_error(const char * msg, ...);

void log_error_swo(const char * msg, ...);

void log_msg(const char * msg, ...);

void log_msg_update(const char * msg, ...);

void log_msg_swo(const char * msg, ...);

#endif /* DEBUG_H_ */
