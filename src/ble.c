/**
 * @file			ble.c
 * @date			21.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include <imu/inc/imu.h>
#include "i2c.h"
#include "inc/ble.h"
#include "inc/gpio.h"
#include "watchdog.h"

/******************** defines *****************************/
#define RX_BUFFERSIZE 			255
#define TX_BUFFERSIZE 			16

/******************** prototypes **************************/
static void ble_transmit(uint8_t *data, uint8_t dataLen);
static void ble_returnUpdateCmd(char *cmd, int16_t ret);
static void ble_rxHandler(uint16_t offset);

static void ble_txCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount);
static void ble_rxCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount);

static uint32_t numOfChunks = 0;

/******************** struct data type ********************/
/**
 * struct for buffer
 */
typedef struct
{
	/*@{*/
	uint8_t write;		/**< write */
	uint8_t read;		/**< read */
	uint8_t size;		/**< size */
	bool overflow;		/**< overflow */
	struct{
		uint8_t bufLen;
		uint8_t buffer[TX_BUFFERSIZE];	/**< buffer */
	}data[EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS];
	/*@}*/
}Buffer_t;

/******************** private variables********************/
UARTDRV_HandleData_t bleHandleData;
UARTDRV_Handle_t bleHandle = &bleHandleData;
uint8_t updateBuffer[RX_BUFFERSIZE];

static Buffer_t txBuffer = {
		.write		= 0,
		.read		= 0,
		.overflow 	= false,
		.size		= EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS,
};

static bool initialized = false;
static bool *localConnected;
static bool *localDebug;
static bool *localUpdate;
static uint8_t rxByte;
static uint8_t rxBuf[RX_BUFFERSIZE*2];
static uint16_t rxBufCounter = 0;

/******************** private function ********************/
void ble_transmit(uint8_t *data, uint8_t dataLen)
{
	if((txBuffer.overflow) && (txBuffer.write == txBuffer.read))
		return;

	txBuffer.data[txBuffer.write].bufLen = dataLen;
	memcpy(txBuffer.data[txBuffer.write].buffer, data, dataLen*sizeof(uint8_t) + 1);

	UARTDRV_Transmit(bleHandle, txBuffer.data[txBuffer.write].buffer, txBuffer.data[txBuffer.write].bufLen, ble_txCallback);

	txBuffer.write++;
	if(txBuffer.write >= txBuffer.size)
	{
		txBuffer.write = 0;
		txBuffer.overflow = true;
	}
}

void ble_returnUpdateCmd(char *cmd, int16_t ret)
{
	static uint8_t tmp[9];
	strcpy((char *)tmp, "AT+U");
	tmp[4] = 4;
	tmp[5] = *cmd;
	tmp[6] = (ret &0xFF00) >> 8;
	tmp[7] = (ret &0x00FF);
	tmp[8] = '\n';
	ble_transmit(tmp, 9);
}

void ble_rxHandler(uint16_t offset)
{
	int8_t ret;
	uint16_t buf[3];
	uint16_t bufLen;

	switch(rxBuf[offset + 3])
	{
		case 'C':
			switch(rxBuf[offset + 5])
			{
				case SET_GYRO_TIME:
					changeGyroTime((rxBuf[offset + 6] << 8) | rxBuf[offset + 7]);
					break;
				case SET_GYRO_LIMIT:
					buf[0] = (rxBuf[offset + 6] << 8) | rxBuf[offset + 7];
					buf[1] = (rxBuf[offset + 8] << 8) | rxBuf[offset + 9];
					buf[2] = (rxBuf[offset + 10] << 8) | rxBuf[offset + 11];
					imu_setGyroLimit(buf);
					break;
				case SET_ACCEL_TIME:
					changeAccelTime((rxBuf[offset + 6] << 8) | rxBuf[offset + 7]);
					break;
				case SET_ACCEL_LIMIT:
					buf[0] = (rxBuf[offset + 6] << 8) | rxBuf[offset + 7];
					buf[1] = (rxBuf[offset + 8] << 8) | rxBuf[offset + 9];
					buf[2] = (rxBuf[offset + 10] << 8) | rxBuf[offset + 11];
					imu_setAccelLimit(buf);
					break;
				case GET_RESET:
					*localDebug = false;
					*localConnected = false;
					watchdog_reset();
					break;
				case GET_CONNECTION:
					if(rxBuf[offset + 7] == 0)
					{
						*localConnected = false;
						log_info_swo("Connection closed!\n");
					}
					else
					{
						*localConnected = true;
						if(*localUpdate == true){
							log_info_swo("UOTA was aborted!\n");
							*localUpdate = false;
							//watchdog_init(); // watchdog_reset();???
							watchdog_start();
							startAllSensors();
						}
						log_info_swo("Connection open!\n");

					}

					break;
				case GET_DEBUG_MODE:
					if(rxBuf[offset + 7]  == 0)
						*localDebug = false;
					else
						*localDebug = true;
					break;
			}
			break;

		case 'U':
			switch(rxBuf[offset + 5])
			{
				case 'F':
					for(bufLen = 0; bufLen < rxBuf[offset + 4]; bufLen++)
						updateBuffer[bufLen] = rxBuf[offset + 6 + bufLen];

					//convert array of numbers to one number!
					uint16_t i, checksum_org = 0;
					for (i = 0; i < (rxBuf[offset + 4] -1); i++)
						checksum_org = 10 * checksum_org + (updateBuffer[i]-48);

					ret = image_Finish(checksum_org);
					ble_returnUpdateCmd("F", ret);
					log_info_update("Done with OUTA %d!\n",ret);
					//*localUpdate = false;

					//watchdog_init();
					log_info_update("feed1!\n");
					watchdog_feed();
					log_info_update("start!\n");
					watchdog_start();//
					log_info_update("feed2!\n");
					watchdog_feed();

					log_info("Waiting for watchdog reset");
					while(1){
						log_info("Waiting for Reset!");
					}
					log_info("You should never ever see this!");
					startAllSensors();
					/*
					watchdog_init();
					watchdog_start();
					startAllSensors();
					*/
					break;
				case 'U':
					log_msg_update("\n");
					log_info_update("receive c no.: %d, data length: %d\n", numOfChunks, rxBuf[4]);
					for(bufLen = 0; bufLen < rxBuf[offset + 4]-1; bufLen++) {
						updateBuffer[bufLen] = 0;
					}
					for(bufLen = 0; bufLen < rxBuf[offset + 4]-1; bufLen++) {
						updateBuffer[bufLen] = rxBuf[offset + 6 + bufLen];
#if (DEBUG && DEBUG_IMAGE)
						log_msg("%02x", updateBuffer[bufLen]);
						if(((bufLen % 16) == 0) && (bufLen > 0)) {
							log_msg("\n");
						}
#endif
						//log_msg("%02x", updateBuffer[bufLen]);
						//if(((bufLen % 16) == 0) && (bufLen > 0)) {
						//	log_msg("\n");
						//}
					}




					if(numOfChunks < 480)
						ret = image_Write(updateBuffer, bufLen);
					else {
						numOfChunks++;
						ret = image_Write(updateBuffer, bufLen);
						numOfChunks--;
					}
					uint16_t temp = rxBuf[offset + 4]+6;
					for(bufLen = 0; bufLen < temp; bufLen++) {
						rxBuf[bufLen] = 0;
					}

					if(ret < 0){
						//log_info("ret < 0: %d",ret);
						ble_returnUpdateCmd("U", ret);
					}else{
						//log_info("U %d",numOfChunks);
						ble_returnUpdateCmd("U", numOfChunks++);
					}

					//numOfChunks++;
					break;
				case 'S':
					watchdog_feed();
#if (DEBUG && DEBUG_UPDATE)
					log_info("OUTA started!\n");
#endif
					diasbleAllSensors();
					watchdog_stop();
					*localUpdate = true;
					numOfChunks = 0;
					NVMHAL_Init();
					ret = image_Init();
					if(ret == 0)
						ret = image_Current();

					log_info_update("Image init finished!\n");
					ble_returnUpdateCmd("S", ret);
					break;
			}
			break;
	}
	return;
}

/******************** callback function *******************/
void ble_txCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount)
{
	(void)handle;
	(void)transferStatus;
	(void)data;
	(void)transferCount;

	if(transferStatus == ECODE_EMDRV_UARTDRV_OK)
	{
		if((!txBuffer.overflow) && (txBuffer.read == txBuffer.write))
			return;

		txBuffer.read++;
		if(txBuffer.read >= txBuffer.size)
		{
			txBuffer.read = 0;
			txBuffer.overflow = false;
		}
	}
}

void ble_rxCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount)
{
	(void)handle;
	(void)transferStatus;
	(void)data;
	(void)transferCount;

	watchdog_feed();
	if(transferStatus == ECODE_EMDRV_UARTDRV_OK)
	{
		rxBuf[rxBufCounter++] = *data;
		if((*data == '\n') && (rxBufCounter > rxBuf[4]))
		{
			char* buf = strstr((char *)rxBuf, "AT+");
			volatile int8_t temp = (int8_t)((uint8_t *)buf - (uint8_t *)rxBuf);
			if(rxBufCounter >= rxBuf[temp + 4] + 5)
			{
				//if(temp != 0)
				//	log_msg("Transfer Counter: %d\n", transferCount);
				//memccpy(buf, &rxBuf[temp], (rxBuf[temp + 4] + 5)*sizeof(uint8_t));
			UARTDRV_FlowControlSet(bleHandle, uartdrvFlowControlOff);
			//log_msg("Transfer Counter: %d\n", transferCount);
			ble_rxHandler(temp);
//			for(uint16_t i = 0; i < rxBufCounter; i++) {
//				rxBuf[i] = 0;
//			}
			rxBufCounter = 0;
			UARTDRV_FlowControlSet(bleHandle, uartdrvFlowControlOn);
			}
		}
	}

	/* RX the next byte */
	UARTDRV_Receive(bleHandle, &rxByte, 1, ble_rxCallback);
}

/******************** global function *********************/
BLE_RETURN ble_reset(bool reset)
{
	if(reset)
	{
		GPIO_PinModeSet(BLE_RESET_PORT, BLE_RESET_PIN, gpioModePushPull, 0);
		delay(500);
	}

	GPIO_PinModeSet(BLE_RESET_PORT, BLE_RESET_PIN, gpioModePushPull, 1);
	delay(500);

	return BLE_SUCCESS;
}

BLE_RETURN ble_init(bool *connected, bool *debug, bool *update)
{
	initialized = false;
	localConnected = connected;
	localDebug = debug;
	localUpdate = update;
	*localUpdate = false;

	//txBuffer.data = malloc(EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS * sizeof(txBuffer.data));
	
	UARTDRV_InitUart_t initData;

	DEFINE_BUF_QUEUE(EMDRV_UARTDRV_MAX_CONCURRENT_RX_BUFS, rxBleBufferQueue);
	DEFINE_BUF_QUEUE(EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS, txBleBufferQueue);

	initData.port = USART0;
	initData.baudRate = 9600;
	initData.portLocation = USART_ROUTE_LOCATION_LOC0;
	initData.stopBits = usartStopbits1;
	initData.parity = usartNoParity;
	initData.oversampling = usartOVS16;
	initData.mvdis = false;
	initData.fcType = uartdrvFlowControlHw;
	initData.ctsPort = BLE_USART_RTS_PORT;
	initData.ctsPin = BLE_USART_RTS_PIN;
	initData.rtsPort = BLE_USART_CTS_PORT;
	initData.rtsPin = BLE_USART_CTS_PIN;
	initData.rxQueue = (UARTDRV_Buffer_FifoQueue_t *)&rxBleBufferQueue;
	initData.txQueue = (UARTDRV_Buffer_FifoQueue_t *)&txBleBufferQueue;

	UARTDRV_InitUart(bleHandle, &initData);
	UARTDRV_Receive(bleHandle, &rxByte, 1, ble_rxCallback);

	initialized = true;
	delay(500);

	int16_t data = 0;
	ble_sendCmd(GET_CONNECTION, &data, 1);
	ble_sendCmd(GET_DEBUG_MODE, &data, 1);

	delay(500);

	return BLE_SUCCESS;
}

BLE_RETURN ble_sendCmd(BLE_CMD cmd, int16_t *data, uint8_t len)
{
	if(!initialized)
		return BLE_NO_INIT;

	static uint8_t tmp[32];
	strcpy((char *)tmp, "AT+C");
	tmp[4] = 2 + 2 * len;
	tmp[5] = cmd;
	uint8_t j = 6;
	for(uint8_t i = 0; i < len; i++) {
		tmp[j++] = (data[i] & 0xFF00) >> 8;
		tmp[j++] = (data[i] & 0x00FF);
	}
	tmp[4 + tmp[4]] = '\n';
	ble_transmit(tmp, tmp[4] + 5);

	return BLE_SUCCESS;
}
